#pragma once
#include <queue>
#include "vec.h"
#include "mat.h"
#include "RasterizerData.h"

namespace Flat_Raster {
    struct Edge : public ::RasterizerEdge {
    };
    
    struct Triangle_Edges {
        Edge e0, e1, e2;
        
        vec4 color;
    };
    
    static auto compare_triangles_min_y = [](Triangle_Edges p1, Triangle_Edges p2) { return p1.e0.min_y > p2.e0.min_y; };
    
    class Rasterizer {
        public:
        RasterizerData *rdata;
        
        std::priority_queue<Triangle_Edges, std::vector<Triangle_Edges>,
        decltype(compare_triangles_min_y)> edge_queue;
        
        Rasterizer(RasterizerData *data);
        
        void load_triangles(const vec3 *vertices, const vec3 *face_normals, size_t count);
        void draw_triangles();
        
        GLfloat diffuse_summation(vec3 face_noraml);
        
        void push_triangle_edges            (vec3 v0, vec3 v1, vec3 v2, vec3 n, vec4 c);
        void push_flat_bottom_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 n, vec4 c);
        void push_flat_top_triangle_edges   (vec3 v0, vec3 v1, vec3 v2, vec3 n, vec4 c);
    };
}
