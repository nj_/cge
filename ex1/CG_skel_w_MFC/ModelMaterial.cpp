#include "stdafx.h"
#include "ModelMaterial.h"

inline vec4 apply_coefficient(const vec4& color, GLfloat coefficient) {
	return vec4(color.r * coefficient, color.g * coefficient, color.b * coefficient, color.a);
}

void ModelMaterial::change_emissive(const vec4& color) {
	this->base_emissive = color;
}

void ModelMaterial::change_color_ambient(const vec4& color) {
	this->base_ambient = color;
}

void ModelMaterial::change_color_diffuse(const vec4& color) {
	this->base_diffuse = color;
}

void ModelMaterial::change_color_specular(const vec4& color) {
	this->base_specular = color;
}

void ModelMaterial::change_color(const vec4& color) {
	this->change_emissive(color);
	this->change_color_ambient(color);
	this->change_color_diffuse(color);
	this->change_color_specular(color);
}

void ModelMaterial::lerp_to_color(const vec4& color, GLfloat t) {
    base_emissive = LERP(base_emissive, t, color*emissive_coefficient);
    base_ambient  = LERP(base_ambient,  t, color*ambient_coefficient);
    base_diffuse  = LERP(base_diffuse,  t, color*diffuse_coefficient);
    base_specular = LERP(base_specular, t, color*specular_coefficient);
}

void ModelMaterial::lerp_to_color(const vec4& color) {
    lerp_to_color(color, lerp_to_color_factor);
}

ModelMaterialStruct::ModelMaterialStruct(vec4 base_emissive, vec4 base_ambient,
                                         vec4 base_diffuse, vec4 base_specular,
                                         GLfloat shininess_coefficient, GLfloat emissive_coefficient, 
                                         GLfloat ambient_coefficient, GLfloat diffuse_coefficient, 
                                         GLfloat specular_coefficient, GLint texture_mode,
                                         bool use_normal_map, bool use_environment_mapping,
                                         GLfloat reflectiveness, GLfloat toon_constant,
                                         GLfloat silhouette_threshold, vec4 silhouette_color,
                                         GLfloat animate_over_x_coef,
                                         GLfloat animate_over_x_range,
                                         vec4 animate_over_x_color,
                                         GLfloat animate_over_y_coef,
                                         GLfloat animate_over_y_range,
                                         vec4 animate_over_y_color,
                                         GLfloat animate_over_z_coef,
                                         GLfloat animate_over_z_range,
                                         vec4 animate_over_z_color) {
    this->base_emissive = base_emissive;
    this->base_ambient  = base_ambient;
    this->base_diffuse  = base_diffuse;
    this->base_specular = base_specular;
    this->shininess_coefficient = shininess_coefficient;
    this->ambient_coefficient = ambient_coefficient;
    this->diffuse_coefficient = diffuse_coefficient;
    this->emissive_coefficient = emissive_coefficient;
    this->specular_coefficient = specular_coefficient;
    
    this->toon_constant = toon_constant;
    this->silhouette_threshold = silhouette_threshold;
    this->silhouette_color = silhouette_color;
    
    this->texture_mode = texture_mode;
    this->use_normal_map = use_normal_map;
    this->use_environment_mapping = use_environment_mapping;
    
    this->reflectiveness = reflectiveness;
    
    this->animate_over_x_coef = animate_over_x_coef;
    this->animate_over_x_range = animate_over_x_range;
    this->animate_over_x_color = animate_over_x_color;
    this->animate_over_y_coef = animate_over_y_coef;
    this->animate_over_y_range = animate_over_y_range;
    this->animate_over_y_color = animate_over_y_color;
    this->animate_over_z_coef = animate_over_z_coef;
    this->animate_over_z_range = animate_over_z_range;
    this->animate_over_z_color = animate_over_z_color;
}

ModelMaterialStruct ModelMaterial::getModelMaterialStruct() {
    return ModelMaterialStruct(base_emissive, base_ambient,
                               base_diffuse, base_specular,
                               shininess_coefficient, emissive_coefficient,
                               ambient_coefficient, diffuse_coefficient,
                               specular_coefficient, texture_mode,
                               use_normal_map, use_environment_mapping,
                               reflectiveness, toon_constant, 
                               silhouette_threshold, silhouette_color,
                               animate_over_x_coef,
                               animate_over_x_range,
                               animate_over_x_color,
                               animate_over_y_coef,
                               animate_over_y_range,
                               animate_over_y_color,
                               animate_over_z_coef,
                               animate_over_z_range,
                               animate_over_z_color);
}

void ModelMaterial::change_shininess(const GLfloat alpha) {
    
}

void ModelMaterial::turn_red() {
    
}

void ModelMaterial::turn_green() {
    
}

void ModelMaterial::turn_blue() {
    
}

void ModelMaterial::increment_shininess(GLfloat inc) {
    
}

void ModelMaterial::increment_red(GLfloat inc) {
    
}

void ModelMaterial::increment_green(GLfloat inc) {
    
}

void ModelMaterial::increment_blue(GLfloat inc) {
    
}