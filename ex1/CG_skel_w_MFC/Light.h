#pragma once
#include "vec.h"
#include <vector>
#include "ModelMaterial.h"
#include <vector>

namespace Light_Settings {
    enum LightSettings {
        Enabled = 1UL << 0,
        
        //Spotlight = 1UL << 4,
        Distance_Attenuation = 1UL << 5, //marks whether attentuation shading is on or off
        
        Ambient_Off = 1UL << 6,
        Diffuse_Off = 1UL << 7,
        Specular_Off = 1UL << 8,
    };
}
namespace Light_Type {
    enum LightType {
        Point    = 1,
        Parallel = 2,
        Ambient  = 3,
    };
}

vec4 calculate_lighting_summation(void* light, ModelMaterial* material, vec3* normal, vec3* point_in_world, vec3* point_of_view);
vec4 empty_vec_function_light_off(void* light, ModelMaterial* material, vec3* normal, vec3* point_in_world, vec3* point_of_view);

GLfloat remote_distance_attentuation(void* light, vec3* point_in_world);
GLfloat no_distance_attentuation(void* light, vec3* point_in_world);

void point_to_direction_parallel_source(void* light, vec3* point_in_world);
void point_to_direction_point_source(void* light, vec3* point_in_world);
vec4 calculate_ambient(void* light, ModelMaterial* material);
vec4 calculate_diffuse(void* light, ModelMaterial* material, vec3* normal);
vec4 calculate_specular(void* light, ModelMaterial* material, vec3* normal, vec3* viewing_angle);

vec4 empty_vec_function_ambient(void* light, ModelMaterial* material);
vec4 empty_vec_function_diffuse(void* light, ModelMaterial* material, vec3* normal);
vec4 empty_vec_function_specular(void* light, ModelMaterial* material, vec3* normal, vec3* viewing_angle);

#define LIGHT_ON 0
#define AMBIENT_ON 1
#define DIFFUSIVE_ON 2
#define SPECULAR_ON 3
#define DISTANCE_ON 4

#define TYPE_POINT 0
#define TYPE_PARALLEL 1
#define TYPE_AMBIENT 2

struct LightData {
    uint32_t type = 0;
    
    //TODO(OZ): Figure out these constants. 
    GLfloat attentuation_constant  = .3f;
    GLfloat attentuation_linear    = .5f;
    GLfloat attentuation_quadratic = .2f;
    
    vec3 position  = vec3(.0f, 4.f, .0f);
    vec3 direction = vec3(.0f, -1.f, .0f);
    vec4 color = vec4(1.f);
    
    int light_data_settings[5] = { 1, 1, 1, 1, 1 };
    // 0 - Light on/off
    // 1 - Ambient on/off
    // 2 - Diffusive on/off
    // 3 - Specular on/off
    // 4 - Distance attentuation on/off
};

class Light {
    public:
    uint32_t settings = Light_Settings::Enabled; // | Light_Settings::Distance_Attenuation;
    uint32_t type = Light_Type::Point;
    
    //TODO(OZ): Figure out these constants. 
    GLfloat attentuation_constant = .3f; //a
    GLfloat attentuation_linear = .5f; //b
    GLfloat attentuation_quadratic = .2f; //c
    
    vec3 camera_position;
    vec3 direction = vec3(0.f, -1.f, 0.f); //kept normalized
    
    vec3 position = vec3(0.f, 4.f, 0.f);
    vec4 color = vec4(1.f);
    vec4 hue = vec4(1.f);
    GLfloat intensity = 2.f;
    
    Light();
    Light(vec3 direction, vec3 position);
    
    GLfloat calculate_distance   (vec3* point_in_world);
    GLfloat distance_attentuation(GLfloat distance);
    
    vec3 calculate_reflection_angle(vec3* normal);
    
    //Functions pertaining to light color
    void calculate_color();
    void change_intensity(GLfloat intensity);
    
    //Calculation function pointers:
    vec4   (*lighting_function) (void*, ModelMaterial*, vec3*, vec3*, vec3*) = &calculate_lighting_summation;
    
    GLfloat(*distance_parameter)(void*, vec3*)                               = &remote_distance_attentuation;
    void   (*update_direction)  (void*, vec3*)                               = &point_to_direction_point_source;
    vec4   (*ambient_function)  (void*, ModelMaterial*)                      = &calculate_ambient;
    vec4   (*diffuse_function)  (void*, ModelMaterial*, vec3*)               = &calculate_diffuse;
    vec4   (*specular_function) (void*, ModelMaterial*, vec3*, vec3*)        = &calculate_specular;
    
    //Switching functions, use ONLY these to change light parameters:
    void switch_on_off();
    void switch_ambient_on_off();
    void switch_diffuse_on_off();
    void switch_specular_on_off();
    
    void restore_from_ambient();
    void set_type(Light_Type::LightType new_type);
    
    void set_distance_attentuation();
    
    void set_calculation_parameters(int* light_data_settings);
};

vec4 calculate_lighting(std::vector<Light*>* lights, ModelMaterial* material, vec3* normal, vec3* point_in_world, vec3* point_of_view);

static inline vec4 normalize_hue(vec4 hue) {
    vec3 c = vec3(hue.r + .00001f, hue.g + .00001f, hue.b + .00001f);
    c = 3.f*normalize(c);
    hue.r = c.x; hue.g = c.y; hue.b = c.z;
    
    return hue;
}