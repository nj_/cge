#include "stdafx.h"
#include "ExperimentalRasterizer.h"
#include "Renderer.h"

using namespace Experimental_Raster;

Rasterizer::Rasterizer(RasterizerData *rdata) :
rdata(rdata), edge_queue(compare_triangles_min_y) {}

inline void Rasterizer::fill_pixel(int x, int y, GLfloat z, vec4 color) {
    rdata->rgba_buffer[INDEX_RGBA(rdata->width, x, y, 0)] = color[0];
    rdata->rgba_buffer[INDEX_RGBA(rdata->width, x, y, 1)] = color[1];
    rdata->rgba_buffer[INDEX_RGBA(rdata->width, x, y, 2)] = color[2];
    rdata->rgba_buffer[INDEX_RGBA(rdata->width, x, y, 3)] = color[3];
    rdata->z_buffer[INDEX_VALUE(rdata->width, x, y)] = z;
}

inline Edge Rasterizer::make_edge(vec3 v0, vec3 v1, int dy) {
    Edge e;
    e.xi = (v0.x < v1.x) - (v0.x >= v1.x);
    
    e.x  = SCREEN_COORD(v0.x, rdata->width);
    e.dx = (SCREEN_COORD(v1.x, rdata->width) - e.x);
    
    e.error = ((e.xi * dy) - e.dx)*e.xi;
    
    e.min_z = v0.z;
    e.max_z = v1.z;
    
    return e;
}

// Reference: https://mcejp.github.io/2020/11/06/bresenham.html
inline void Rasterizer::clip_and_push_triangle_edges_to_queue(Triangle_Edges te) {
    // Clip the edges to the screen:
    if(te.dy > 0) {
        if(te.e0.xi > 0 || te.e1.xi > 0) return;
        if(te.max_y >= rdata->height) {
            if(te.min_y >= rdata->height) return;
            te.max_y = rdata->height-1;
        }
        if(te.min_y < 0) {
            if(te.max_y < 0) return;
            
            for(; te.min_y < 0; ++te.min_y) {
                
            }
        }
    }
    
    edge_queue.push(te);
}

inline void Rasterizer::push_flat_bottom_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 normal, vec4 color) {
    Triangle_Edges te;
    
    te.min_y = SCREEN_COORD(v0.y, rdata->height);
    te.max_y = SCREEN_COORD(v2.y, rdata->height);
    
    te.dy = te.max_y - te.min_y;
    te.normal = normal;
    te.color = color;
    
    if(v0.x < v1.x) {
        te.e0 = make_edge(v0, v2, te.dy);
        te.e1 = make_edge(v1, v2, te.dy);
    } else {
        te.e0 = make_edge(v1, v2, te.dy);
        te.e1 = make_edge(v0, v2, te.dy);
    }
    
    clip_and_push_triangle_edges_to_queue(te);
}

inline void Rasterizer::push_flat_top_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 normal, vec4 color)  {
    Triangle_Edges te;
    
    te.min_y = SCREEN_COORD(v0.y, rdata->height);
    te.max_y = SCREEN_COORD(v2.y, rdata->height);
    
    te.dy = te.max_y - te.min_y;
    te.normal = normal;
    te.color = color;
    
    if(v1.x < v2.x) {
        te.e0 = make_edge(v0, v1, te.dy);
        te.e1 = make_edge(v0, v2, te.dy);
    } else {
        te.e0 = make_edge(v0, v2, te.dy);
        te.e1 = make_edge(v0, v1, te.dy);
    }
    
    clip_and_push_triangle_edges_to_queue(te);
}

inline void Rasterizer::push_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 normal, vec4 color) {
    if(around_zero_epsilon(v0.y - v1.y)) {
        push_flat_bottom_triangle_edges(v0, v1, v2, normal, color);
    } else if(around_zero_epsilon(v1.y - v2.y)) {
        push_flat_top_triangle_edges(v0, v1, v2, normal, color);
    } else {
        GLfloat t = (v1.y - v0.y) / (v2.y - v0.y);
        
        vec3 v3 = LERP(v0, t, v2);
        
        push_flat_top_triangle_edges(v0, v1, v3, normal, color);
        push_flat_bottom_triangle_edges(v1, v3, v2, normal, color);
    }
}

void Rasterizer::load_triangles(const vec3 *vertices, size_t count, vec4 color) {
    mat4 transform = rdata->projection * rdata->camera_transform * rdata->world_transform * rdata->model_transform;
    
    for (int i = 0; i < count; ++i) {
        vec4 tv0 = transform * vertices[i*3 + 0];
        vec4 tv1 = transform * vertices[i*3 + 1];
        vec4 tv2 = transform * vertices[i*3 + 2];
        
        if(!z_in_bounds(tv0) && !z_in_bounds(tv1) && !z_in_bounds(tv2)) {
            continue;
        }
        vec3 v0 = vec3FromAffine(tv0);
        vec3 v1 = vec3FromAffine(tv1);
        vec3 v2 = vec3FromAffine(tv2);
        
        // Backface culling {
        vec3 normal = cross(v1-v0, v2-v0);
        if(normal.z <= 0) continue;
        normal = normalize(normal);
        // }
        
        vec3 n_v0, n_v1, n_v2;
        
        // organize triangle vertices by y value:
        if(v0.y < v2.y) {
            if(v0.y < v1.y) {
                n_v0 = v0;
                
                if(v1.y < v2.y) {
                    n_v1 = v1;
                    n_v2 = v2;
                } else { // v2.y < v1.y
                    n_v1 = v2;
                    n_v2 = v1;
                }
            } else { // v1.y < v0.y
                n_v0 = v1;
                n_v1 = v0;
                n_v2 = v2;
            }
        } else { // v2.y < v0.y
            if(v1.y < v2.y) {
                n_v0 = v1;
                n_v1 = v2;
                n_v2 = v0;
            } else { // v2.y < v1.y
                n_v0 = v2;
                
                if(v0.y < v1.y) {
                    n_v1 = v0;
                    n_v2 = v1;
                } else { // v1.y < v0.y
                    n_v1 = v1;
                    n_v2 = v0;
                }
            }
        }
        
        // color = debug_face_colors[i % debug_color_count];
        push_triangle_edges(n_v0, n_v1, n_v2, normal, color);
    }
}

void Rasterizer::draw_triangles() {
    std::vector<Triangle_Edges> active_edges;
    
    vec3 light_direction = normalize(vec3FromAffine(rdata->projection * rdata->camera_transform * vec3( 2, -2,  0)));
    vec3 light_location  = vec3FromAffine(rdata->projection * rdata->camera_transform * vec3(-2,  2,  0));
    
    for(int y = 0; y < rdata->height; ++y) {
        while(!edge_queue.empty()) {
            auto it = &edge_queue.top();
            if(it->min_y > y) break;
            active_edges.push_back(*it);
            edge_queue.pop();
        }
        
        auto it = active_edges.begin();
        while(it != active_edges.end()) {
            auto e0 = &(*it).e0;
            auto e1 = &(*it).e1;
            
            GLfloat t;
            if(it->max_y == it->min_y) {
                t = 1;
            } else {
                t = (GLfloat)(y - it->min_y)/(GLfloat)(it->max_y - it->min_y);
            }
            
            GLfloat z0 = LERP(e0->min_z, t, e0->max_z);
            
            GLfloat diffuse = 2.f*dot(it->normal, light_direction);
            vec4 color = it->color * diffuse;
            color.x += .25f;
            color.y += .15f;
            color.z += .2f;
            
            if(it->dy > 0) {         
                while(e0->error < 0) {
                    e0->x += e0->xi;
                    e0->error += 2*it->dy;
                }
                e0->error -= (2*e0->dx)*e0->xi;
                while(e1->error < 0) {
                    e1->x += e1->xi;
                    e1->error += 2*it->dy;
                }
                e1->error -= (2*e1->dx);
            }
            
            int x0 = clamp_min(e0->x, 0);
            int x1 = clamp_max(e1->x, rdata->width - 1);
            
            if(x0 == x1) {
                if((z0 >= -1.f && z0 <= 1.f) && (z0 < rdata->z_buffer[INDEX_VALUE(rdata->width, x0, y)])) {
                    fill_pixel(x0, y, z0, color);
                }
            } else {
                GLfloat z1 = LERP(e1->min_z, t, e1->max_z);
                for(int x = x0; x < x1; ++x) {
                    GLfloat t = (GLfloat)(x - e0->x)/(GLfloat)(e1->x - e0->x);
                    GLfloat z = LERP(z0, t, z1);
                    if(z < -1.f || z > 1.f) continue;
                    if(z >= rdata->z_buffer[INDEX_VALUE(rdata->width, x, y)]) continue;
                    
                    fill_pixel(x, y, z, color);
                }
            }
            
            
            // NOTE(NJ): Remove edges from the active set
            if(y >= it->max_y) {
                it = active_edges.erase(it);
                continue;
            } else {
                ++it;
            }
        }
    }
}
