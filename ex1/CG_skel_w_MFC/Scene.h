#pragma once
#include "gl/glew.h"
#include <vector>
#include <string>
#include "Renderer.h"
#include "Camera.h"
#include "Light.h"
#include "MeshModel.h"

namespace Scene_Settings {
    enum SceneSettings {
        CameraMode = 1UL << 0,
        ModelMode = 1UL << 1,
        
        WorldFrame = 1UL << 2,
        ViewFrame = 1UL << 3,
        
        MouseMoveable = 1UL << 9,
        MousePassivelyMoveable = 1UL << 10,
        
        CameraMovesInWorldSpace = 1UL << 11,
        CameraRotatesAroundAt = 1UL << 12,
        
        Scaling_X = 1UL << 13,
        Scaling_Y = 1UL << 14,
        Scaling_Z = 1UL << 15,
        Scaling_All = Scaling_X | Scaling_Y | Scaling_Z,
        
        Fog_Effect = 1UL << 16,
        Supersampling_Anti_Aliasing = 1UL << 17,
        
        DrawUiElements = 1UL << 20,
        AlwaysDraw = 1UL << 21,
        
        Animate = 1UL << 22,
        SkyboxEnabled = 1UL << 23,
    };
}

struct SceneActionDelta {
    GLfloat translation = 0.1f;
    GLfloat rotation    = 1.0f;
    GLfloat scaling     = 1.25f;
};

struct RendererData {
    GLuint program_flat;
    GLuint program_gouraud;
    GLuint program_phong;
    GLuint program_toon;
    
    GLuint program_skybox;
    GLuint program_normal;
    GLuint program_ui;
    
    GLuint light_buffer;
    GLuint ssaa_factor = 2;
    
    GLuint skybox_texture;
    GLuint skybox_vertex_array;
    GLuint skybox_vertex_buffer;
    
    int width, height;
};

class Scene {
    public:
    // ---- Data ----
    std::vector<MeshModel*> models;
    
    std::vector<Light*> lights;
    std::vector<Camera*> cameras;
    
    GLdouble time = 0;
    
    Renderer *renderer = nullptr;
    
    Camera* active_camera = nullptr;
    MeshModel* active_model = nullptr; //active model is identified through its pointer
    Light* active_light = nullptr;
    
    SceneActionDelta model_delta;
    SceneActionDelta camera_delta;
    
    uint32_t settings = (Scene_Settings::ViewFrame   | Scene_Settings::CameraMode  |
                         Scene_Settings::Scaling_All | Scene_Settings::MouseMoveable |
                         Scene_Settings::Fog_Effect  | Scene_Settings::DrawUiElements |
                         Scene_Settings::AlwaysDraw  | Scene_Settings::SkyboxEnabled);
    uint32_t shading_mode = Shading_Mode::Flat;
    
    RendererData gl_renderer;
    
    std::string skybox_path;
    bool use_skybox = true;
    
    // ---- Methods ----
    MeshModel* get_next_meshmodel_address(MeshModel* activeModel);
    Camera* get_next_camera_address(Camera* camera);
    
    Scene();
    Scene(Renderer *renderer);
    ~Scene();
    
    void modify_model_translation_delta(GLfloat modifier);
    void modify_model_rotation_delta(GLfloat modifier);
    void modify_model_scaling_delta(GLfloat modifier);
    void reset_model_delta();
    
    void modify_camera_translation_delta(GLfloat modifier);
    void modify_camera_rotation_delta(GLfloat modifier);
    void modify_camera_scaling_delta(GLfloat modifier);
    void reset_camera_delta();
    
    void load_obj_model(std::string fileName);
    
    void add_model(MeshModel *model);
    
    void add_camera(Camera *camera);
    void add_camera();
    
    void draw();
    void opengl_render();
    void load_light_data(LightData* lights_data);
    
    void load_skybox();
    
    void update_aspect_ratio(int width, int height);
    void update_aspect_ratio();
    
    // Lighting operations:
    Light* get_next_light_address(Light* active_light);
    void iterate_active_light();
    void rotate_active_light_x(GLfloat theta);
    void rotate_active_light_y(GLfloat theta);
    void rotate_active_light_z(GLfloat theta);
    void move_active_light    (vec3& v);
    void move_active_light_x  (GLfloat theta);
    void move_active_light_y  (GLfloat theta);
    void move_active_light_z  (GLfloat theta);
    void add_light(Light *light);
    void add_light();
    void unload_active_light();
    
    // Active Model Transformations:
    bool is_active_model(MeshModel* model);
    void iterate_models();
    void unload_active_model();
    
    void look_at_active_model();
    
    void rotate_active_model_z(GLfloat theta);
    void rotate_active_model_x(GLfloat theta);
    void rotate_active_model_y(GLfloat theta);
    void scale_active_model(vec3 v);
    void scale_active_model(GLfloat delta);
    void scale_active_model_up();
    void scale_active_model_down();
    
    void translate_active_model(vec3 v);
    void model_scaling_switch();
    void reset_active_model_mtransform();
    void reset_active_model_wtransform();
    
    // Active Camera Transformations:
    bool is_active_camera(Camera* camera);
    void iterate_cameras();
    void unload_active_camera();
    
    void rotate_active_camera_z(GLfloat theta);
    void rotate_active_camera_x(GLfloat theta);
    void rotate_active_camera_y(GLfloat theta);
    void translate_active_camera(vec3 v);
    void zoom_in_active_camera();
    void zoom_out_active_camera();
    void reset_active_camera();
    
    void print_active_model();
    
    int activeLight;
    
    GLfloat normalize_mouse_movement_x(int dy);
    GLfloat normalize_mouse_movement_y(int dx);
};

vec4 empty_vec4(RasterizerData* rdata, GLfloat z, vec4& color);
vec4 fog_effect_calculation(RasterizerData* rdata, GLfloat z, vec4& color);