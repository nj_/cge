#pragma once
#include "vec.h"
#include "mat.h"

class Camera {
    public:
    vec3 at  = vec3(0.0f, 0.0f, 0.0f);
    vec3 eye = vec3(0.0f, 0.0f, 11.0f);
    vec3 up  = vec3(0.0f, 1.0f, 0.0f);
    
    float left   = -1.0f, right =  1.0f;
    float bottom = -1.0f, top   =  1.0f;
    float z_near = -8.0f, z_far = -110.0f;
    
    float pixels_per_unit = 300.0f;
    int width = 512, height = 512;
    
    bool is_orthographic = false;
    
    Camera() = default;
    
    Camera(int width, int height);
    
    void translate(vec3 delta);
    void global_translate(vec3 delta);
    
    void zoom(float factor);
    
    void rotate(GLfloat theta, vec3 axis);
    void rotate_around(GLfloat theta, vec3 axis);
    void global_rotate(GLfloat theta, vec3 axis);
    
    void look_at(const vec3& eye, const vec3& at, const vec3& up);
    void look_at(const vec3& at);
    
    mat4 get_transform();
    
    mat4 ortho();
    mat4 frustum();
    mat4 get_projection();
    void reset_projection();
    
    void set_projection(float left, float right, float bottom, float top, float z_near, float z_far);
    
    void set_projection(float fovy, float aspect, float z_near, float z_far);
    
    void update_aspect_ratio(int width, int height);
    
    vec3 get_x_axis();
    vec3 get_y_axis();
    vec3 get_z_axis();
};
