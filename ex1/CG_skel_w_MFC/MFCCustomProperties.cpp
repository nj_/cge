#include "stdafx.h"
#include "MFCCustomProperties.h"
#include "vec.h"

MFCPropertyData::MFCPropertyData(MFCProperty_Type::MFCPropertyType type, void *data, uint32_t enum_value) :
type(type), data(data), enum_value(enum_value) {};

// Group constructor
MFCCustomProperty::MFCCustomProperty(const CString& strGroupName, DWORD_PTR dwData, BOOL bIsValueList) :
CMFCPropertyGridProperty(strGroupName, dwData, bIsValueList) {};

// Simple property
MFCCustomProperty::MFCCustomProperty(const CString& strName, const COleVariant& varValue, LPCTSTR lpszDescr, MFCPropertyData *data,
                                     LPCTSTR lpszEditMask, LPCTSTR lpszEditTemplate, LPCTSTR lpszValidChars) :
CMFCPropertyGridProperty(strName, varValue, lpszDescr, (DWORD_PTR)data,
                         lpszEditMask, lpszEditTemplate, lpszValidChars) {};

CString MFCCustomProperty::FormatProperty() {
    CString str;
    if (GetValue().vt == VT_R4) {
        str.Format("%.2f", GetValue().fltVal);
    } else {
        str = CMFCPropertyGridProperty::FormatProperty();
    }
    return str;
}


void MFCCustomProperties::Init() {
    CMFCPropertyGridCtrl::Init();
    CRect rect;
    GetClientRect(rect);
    
    //add all attribute
    m_nLeftColumnWidth = rect.Width() / 3;
}

MFCCustomProperties::MFCCustomProperties(Scene *scene) : scene(scene) {};

inline void ApplyProperty(CMFCPropertyGridProperty *property) {
    using namespace MFCProperty_Type;
    MFCPropertyData *data = (MFCPropertyData *)property->GetData();
    if(data) {
        switch(data->type) {
            case Float: {
                *(float *)data->data = V_R4(&property->GetValue());
                auto parent = property->GetParent();
                auto parent_data = (MFCPropertyData *)parent->GetData();
                if(parent_data && parent_data->type == MFCProperty_Type::Direction) {
                    vec3 *direction = static_cast<vec3 *>(parent_data->data);
                    *direction = normalize(*direction);
                    parent->GetSubItem(0)->SetValue(direction->x);
                    parent->GetSubItem(1)->SetValue(direction->y);
                    parent->GetSubItem(2)->SetValue(direction->z);
                }
            } break;
            
            case Angle: {
                float theta = V_R4(&property->GetValue());
                while(theta < 0.0f) {
                    theta += 360.0f;
                }
                while(theta >= 360.0f) {
                    theta -= 360.0f;
                }
                property->SetValue(theta);
                *(float *)data->data = theta;
            } break;
            
            case Bool: {
                *(bool *)data->data = V_BOOL(&property->GetValue());
                if(strcmp(property->GetName(), _T("Crazy")) == 0) {
                    auto parent = property->GetParent();
                    parent->GetSubItem(2)->Show(*(bool *)(data->data), TRUE);
                }
            } break;
            
            case SceneSettings: {
                uint32_t *settings = (uint32_t *)data->data;
                bool value = V_BOOL(&property->GetValue());
                if(value) {
                    *settings |= data->enum_value;
                } else {
                    *settings &= (~data->enum_value);
                }
            } break;
            
            case Int: {
                *(int *)data->data = V_I4(&property->GetValue());
            } break;
            
            case Color: {
                auto color_property = static_cast<CMFCPropertyGridColorProperty *>(property);
                *(vec4 *)data->data = RGB_TO_VEC4(color_property->GetColor());
            } break;
            
            case Path: {
                auto path_property = static_cast<CMFCPropertyGridFileProperty *>(property);
                
                switch(data->enum_value) {
                    case -1: { 
                        auto scene = static_cast<Scene *>(data->data);
                        scene->skybox_path = (const char *)_bstr_t(V_BSTR(&path_property->GetValue()));
                        scene->load_skybox();
                        scene->use_skybox = true;
                    } break;
                    case 0: { 
                        auto model = static_cast<MeshModel *>(data->data);
                        model->texture_map_path = (const char *)_bstr_t(V_BSTR(&path_property->GetValue()));
                        model->load_texture_map();
                    } break;
                    case 1: {
                        auto model = static_cast<MeshModel *>(data->data);
                        model->normal_map_path = (const char *)_bstr_t(V_BSTR(&path_property->GetValue()));
                        model->load_normal_map();
                    } break;
                }
            } break;
            
            case MaterialColor: {
                auto color_property = static_cast<CMFCPropertyGridColorProperty *>(property);
                ModelMaterial *material = (ModelMaterial *)data->data;
                vec4 color = RGB_TO_VEC4(color_property->GetColor());
                
                switch(data->enum_value) {
                    case 0: { 
                        auto parent = property->GetParent();
                        material->change_color(color);
                        
                        ((CMFCPropertyGridColorProperty *)parent->GetSubItem(1))->SetColor(color_property->GetColor());
                        ((CMFCPropertyGridColorProperty *)parent->GetSubItem(3))->SetColor(color_property->GetColor());
                        ((CMFCPropertyGridColorProperty *)parent->GetSubItem(5))->SetColor(color_property->GetColor());
                        ((CMFCPropertyGridColorProperty *)parent->GetSubItem(7))->SetColor(color_property->GetColor());
                    } break;
                    case 1: { material->change_emissive(color); } break;
                    case 2: { material->change_color_ambient(color); } break;
                    case 3: { material->change_color_diffuse(color); } break;
                    case 4: { material->change_color_specular(color); } break;
                }
            } break;
            
            case ProjectionType: {
                auto value = V_BSTR(&property->GetValue());
                bool is_orthographic = (wcscmp(value, L"Orthographic") == 0);
                *(bool *)data->data = is_orthographic;
            } break;
            
            case ShadingMode: {
                auto value = V_BSTR(&property->GetValue());
                ModelMaterial *m = (ModelMaterial *)data->data;
                if(wcscmp(value, L"Flat") == 0) {
                    m->shading_mode = Shading_Mode::Flat;
                } else if(wcscmp(value, L"Gouraud") == 0) {
                    m->shading_mode = Shading_Mode::Gouraud;
                } else if(wcscmp(value, L"Phong") == 0) {
                    m->shading_mode = Shading_Mode::Phong;
                } else if(wcscmp(value, L"Toon") == 0) {
                    m->shading_mode = Shading_Mode::Toon;
                }
            } break;
            
            case LightType: {
                Light *light = (Light *)data->data;
                auto parent = property->GetParent();
                
                parent->GetSubItem(2)->Show(FALSE, TRUE);
                parent->GetSubItem(3)->Show(FALSE, TRUE);
                parent->GetSubItem(4)->Show(FALSE, TRUE);
                
                auto value = V_BSTR(&property->GetValue());
                if(wcscmp(value, L"Ambient") == 0) {
                    light->set_type(Light_Type::Ambient);
                } else if(wcscmp(value, L"Point") == 0) {
                    light->set_type(Light_Type::Point);
                    parent->GetSubItem(2)->Show(TRUE, TRUE);
                    parent->GetSubItem(4)->Show(TRUE, TRUE);
                } else if(wcscmp(value, L"Parallel") == 0) {
                    light->set_type(Light_Type::Parallel);
                    parent->GetSubItem(2)->Show(TRUE, TRUE);
                    parent->GetSubItem(3)->Show(TRUE, TRUE);
                }
                parent->Redraw();
            } break;
            
            case LightSettings: {
                Light *light = (Light *)data->data;
                
                bool value = V_BOOL(&property->GetValue());
                if(data->enum_value == Light_Settings::Enabled) {
                    light->switch_on_off();
                    return;
                }
                if(value) {
                    light->settings |= data->enum_value;
                } else {
                    light->settings &= (~data->enum_value);
                }
                if(data->enum_value == Light_Settings::Distance_Attenuation) {
                    light->set_distance_attentuation();
                }
            } break;
            
            case LightHue: {
                Light *light = (Light *)data->data;
                
                auto hue_property = static_cast<CMFCPropertyGridColorProperty *>(property);
                vec4 hue = normalize_hue(RGB_TO_VEC4(hue_property->GetColor()));
                
                light->hue = hue;
                hue_property->SetColor(VEC4_TO_RGB(hue));
                
                light->calculate_color();
            } break;
            
            case LightIntensity: {
                Light *light = (Light *)data->data;
                light->intensity = clamp_min(V_R4(&property->GetValue()));
                property->SetValue(light->intensity);
                light->calculate_color();
            } break;
        }
    }
    
    for(int i = 0; i < property->GetSubItemsCount(); ++i) {
        auto it = property->GetSubItem(i);
        ApplyProperty(it);
    }
}

void MFCCustomProperties::OnPropertyChanged(CMFCPropertyGridProperty* property) const {
    ApplyProperty(property);
    if(strcmp(property->GetName(), _T("SSAA factor")) == 0) {
        scene->update_aspect_ratio();
    }
    
    auto parent = property->GetParent();
    if(property->GetSubItemsCount() > 0) {
        parent = property;
    }
    if(parent) {
        Camera *camera = scene->active_camera;
        if(strcmp(parent->GetName(), _T("Frustum")) == 0) {
            auto perspective = parent->GetParent()->GetSubItem(2);
            float fovy = -std::atan((camera->top - camera->bottom)/(2.f*camera->z_near))*360.f/F_PI;
            float aspect = (camera->top - camera->bottom)/(camera->right - camera->left);
            perspective->GetSubItem(0)->SetValue(fovy);
            perspective->GetSubItem(1)->SetValue(aspect);
            perspective->GetSubItem(2)->SetValue(camera->z_near);
            perspective->GetSubItem(3)->SetValue(camera->z_far);
        } else if(strcmp(parent->GetName(), _T("Perspective")) == 0) {
            camera->set_projection(-V_R4(&parent->GetSubItem(0)->GetValue())*F_PI/360.f,
                                   V_R4(&parent->GetSubItem(1)->GetValue()),
                                   V_R4(&parent->GetSubItem(2)->GetValue()),
                                   V_R4(&parent->GetSubItem(3)->GetValue()));
            auto projection = parent->GetParent()->GetSubItem(1);
            projection->GetSubItem(0)->SetValue(camera->left);
            projection->GetSubItem(1)->SetValue(camera->right);
            projection->GetSubItem(2)->SetValue(camera->top);
            projection->GetSubItem(3)->SetValue(camera->bottom);
            projection->GetSubItem(4)->SetValue(camera->z_near);
            projection->GetSubItem(5)->SetValue(camera->z_far);
        }
    }
    scene->draw();
}

void MFCCustomProperties::Reset() {
    ResetOriginalValues();
    
    for(int i = 0; i < GetPropertyCount(); ++i) {
        ApplyProperty(static_cast<MFCCustomProperty*>(GetProperty(i)));
    }
}
