#pragma once
#include <queue>
#include "vec.h"
#include "mat.h"
#include "RasterizerData.h"

namespace Experimental_Raster {
    struct Edge {
        int x, dx, xi, error;
        GLfloat min_z, max_z;
    };
    
    struct Triangle_Edges {
        int min_y, max_y, dy;
        Edge e0, e1;
        
        vec4 color;
        vec3 normal;
    };
    
    static auto compare_triangles_min_y = [](Triangle_Edges p1, Triangle_Edges p2) { return p1.min_y > p2.min_y; };
    
    class Rasterizer {
        public:
        RasterizerData *rdata;
        
        std::priority_queue<Triangle_Edges, std::vector<Triangle_Edges>,
        decltype(compare_triangles_min_y)> edge_queue;
        
        Rasterizer(RasterizerData *data);
        Edge make_edge(vec3 v0, vec3 v1, int dy);
        
        void load_triangles(const vec3 *vertices, size_t count, vec4 color);
        void draw_triangles();
        
        void fill_pixel(int x, int y, GLfloat z, vec4 color);
        void push_triangle_edges            (vec3 v0, vec3 v1, vec3 v2, vec3 n, vec4 c);
        void push_flat_bottom_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 n, vec4 c);
        void push_flat_top_triangle_edges   (vec3 v0, vec3 v1, vec3 v2, vec3 n, vec4 c);
        void clip_and_push_triangle_edges_to_queue(Triangle_Edges te);
    };
}
