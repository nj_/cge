#include "StdAfx.h"
#include "Light.h"
#include <cassert>

vec4 calculate_lighting_summation(void* light, ModelMaterial* material, vec3* normal, vec3* point_in_world, vec3* point_of_view) {
    Light* cast_light = (Light*)light;
    
    cast_light->update_direction(cast_light, point_in_world);
    vec4    ambient = cast_light->ambient_function(cast_light, material);
    vec4    diffuse = cast_light->diffuse_function(cast_light, material, normal);
    vec3    viewing_angle = normalize(*point_of_view - *point_in_world);
    vec4    specular = cast_light->specular_function(cast_light, material, normal, &viewing_angle);
    vec4    emissive = material->base_emissive;
    GLfloat distance_term = cast_light->distance_parameter(cast_light, point_in_world);
    
    return distance_term * (ambient + diffuse + specular) + emissive;
}

vec4 empty_vec_function_light_off(void* light, ModelMaterial* material, vec3* normal, vec3* point_in_world, vec3* point_of_view) { return vec4(0.f); }

GLfloat remote_distance_attentuation(void* light, vec3* point_in_world) {
    Light* cast_light = (Light*)light;
    return cast_light->distance_attentuation(cast_light->calculate_distance(point_in_world));
}

GLfloat no_distance_attentuation(void* light, vec3* point_in_world) { return 1.f; }

void point_to_direction_parallel_source(void* light, vec3* point_in_world) {
    return;
}

void point_to_direction_point_source(void* light, vec3* point_in_world) {
    Light* cast_light = (Light*)light;
    cast_light->direction = normalize(cast_light->position - *point_in_world);
}

vec4 calculate_ambient(void* light, ModelMaterial* material) {
    Light* cast_light = (Light*)light;
    return cast_light->color * material->base_ambient;
}

vec4 calculate_diffuse(void* light, ModelMaterial* material, vec3* normal) {
    Light* cast_light = (Light*)light;
    return material->base_diffuse * clamp_min(dot(cast_light->direction, *normal)) * cast_light->color;
}

vec4 calculate_specular(void* light, ModelMaterial* material, vec3* normal, vec3* viewing_angle) {
    Light* cast_light = (Light*)light;
    return material->base_specular * cast_light->color * clamp_min(std::pow(dot(cast_light->calculate_reflection_angle(normal), *viewing_angle), material->shininess_coefficient));
}

vec4 empty_vec_function_ambient(void* light, ModelMaterial* material) { return vec4(0.f); }
vec4 empty_vec_function_diffuse(void* light, ModelMaterial* material, vec3* normal) { return vec4(0.f); }
vec4 empty_vec_function_specular(void* light, ModelMaterial* material, vec3* normal, vec3* viewing_angle) { return vec4(0.f); }

Light::Light() {
    hue = normalize_hue(hue);
    calculate_color();
}

Light::Light(vec3 direction, vec3 position) : direction(direction), position(position) {
    hue = normalize_hue(hue);
    calculate_color();
}

inline GLfloat Light::calculate_distance(vec3* point_in_world) {
    return distance(position, *point_in_world);
}

inline GLfloat Light::distance_attentuation(GLfloat distance) {
    GLfloat denominator = distance * (distance * attentuation_quadratic + attentuation_linear) + attentuation_constant;
    if (denominator == 0.f) {
        return 1.f;
    }
    return 1 / (denominator);
}

inline vec3 Light::calculate_reflection_angle(vec3* normal) {
    return 2 * dot(direction, *normal) * (*normal) - direction;
}

vec4 calculate_lighting(std::vector<Light*>* lights, ModelMaterial* material, vec3* normal, vec3* point_in_world, vec3* point_of_view) {
    vec4 result = vec4(0.f);
    for (auto it : *lights) {
        result += it->lighting_function(it, material, normal, point_in_world, point_of_view);
    }
    
    return vec4(clamp(result.r), clamp(result.g), clamp(result.b), clamp(result.a));
}


void Light::set_calculation_parameters(int* light_data_settings) {
    light_data_settings[LIGHT_ON] = (settings & Light_Settings::Enabled) ? 1 : 0;
    light_data_settings[AMBIENT_ON] = (settings & Light_Settings::Ambient_Off) ? 0 : 1;
    light_data_settings[DIFFUSIVE_ON] = (settings & Light_Settings::Diffuse_Off) ? 0 : 1;
    light_data_settings[SPECULAR_ON] = (settings & Light_Settings::Specular_Off) ? 0 : 1;
    light_data_settings[DISTANCE_ON] = (settings & Light_Settings::Distance_Attenuation) ? 1 : 0;
}


/////////                             /////////  
/////////                             ///////// 
///////// ---- Switch functions: ---- /////////
/////////                             ///////// 
/////////                             ///////// 

void Light::switch_on_off() { ////////////////////TODO/////////////////
    if (settings & Light_Settings::Enabled) {
        settings &= ~(Light_Settings::Enabled);
        this->lighting_function = &empty_vec_function_light_off;
    }
    else {
        settings |= (Light_Settings::Enabled);
        this->lighting_function = &calculate_lighting_summation;
    }
}

void Light::switch_ambient_on_off() {
    if (type == Light_Type::Ambient) return;
    
    if (settings & Light_Settings::Ambient_Off) {
        settings &= ~(Light_Settings::Ambient_Off);
        this->ambient_function = &calculate_ambient;
    }
    else {
        settings |= Light_Settings::Ambient_Off;
        this->ambient_function = &empty_vec_function_ambient;
    }
}

void Light::switch_diffuse_on_off() {
    if (type == Light_Type::Ambient) return;
    
    if (settings & Light_Settings::Diffuse_Off) {
        settings &= ~(Light_Settings::Diffuse_Off);
        this->diffuse_function = &calculate_diffuse;
    }   
    else {
        settings |= (Light_Settings::Diffuse_Off);
        this->diffuse_function = &empty_vec_function_diffuse;
    }
}

void Light::switch_specular_on_off() {
    if (type == Light_Type::Ambient) return;
    
    if (settings & Light_Settings::Specular_Off) {
        settings &= ~(Light_Settings::Specular_Off);
        this->specular_function = &calculate_specular;
    }
    else {
        settings |= (Light_Settings::Specular_Off);
        this->specular_function = &empty_vec_function_specular;
    }
}

void Light::restore_from_ambient() {
    if (settings & Light_Settings::Ambient_Off)
        ambient_function = &empty_vec_function_ambient;
    else
        ambient_function = &calculate_ambient;
    if (settings & Light_Settings::Diffuse_Off)
        diffuse_function = &empty_vec_function_diffuse;
    else
        diffuse_function = &calculate_diffuse;
    if (settings & Light_Settings::Specular_Off)
        specular_function = &empty_vec_function_specular;
    else
        specular_function = &calculate_specular;
}

void Light::set_type(Light_Type::LightType new_type) {
    if(type == Light_Type::Ambient) {
        restore_from_ambient();
    }
    type = new_type;
    
    distance_parameter = &no_distance_attentuation;
    
    switch(type) {
        case Light_Type::Ambient: {
            update_direction = &point_to_direction_parallel_source;
            ambient_function = &calculate_ambient;
            diffuse_function = &empty_vec_function_diffuse;
            specular_function = &empty_vec_function_specular;
        } break;
        case Light_Type::Point: {
            update_direction = &point_to_direction_point_source;
            if (settings & Light_Settings::Distance_Attenuation)
                distance_parameter = &remote_distance_attentuation;
        } break;
        case Light_Type::Parallel: {
            update_direction = &point_to_direction_parallel_source;
        } break;
        default: {
            assert(!"Unsupported light type");
        }
    }
}

void Light::set_distance_attentuation() {
    if ((type == Light_Type::Parallel) || (type == Light_Type::Ambient)) return;
    
    if (settings & Light_Settings::Distance_Attenuation) {
        this->distance_parameter = &remote_distance_attentuation;
    } else {
        this->distance_parameter = &no_distance_attentuation;
    }
}

inline bool outside_legal_range_single_val(GLfloat color_val) {
    return color_val > 1.f || color_val < 0.f;
}

inline bool outside_legal_range(vec4& color_vec) {
    return outside_legal_range_single_val(color_vec.r) ||
        outside_legal_range_single_val(color_vec.g) ||
        outside_legal_range_single_val(color_vec.b);
}

void Light::calculate_color() {
    color = hue * intensity;
}

void Light::change_intensity(GLfloat new_intensity) {
    if (new_intensity <= 1.f && new_intensity >= 0.f)
        this->intensity = new_intensity;
}