#include "stdafx.h"
#include "Scene.h"
#include "MeshModel.h"
#include "PrimeMeshModel.h"
#include <string>
#include <cassert>
#include "GL\freeglut.h"
#include "InitShader.h"
#include "stb_image.h"

Scene::Scene(Renderer *renderer) : renderer(renderer) {
    Camera *camera = new Camera();
    cameras.push_back(camera);
    active_camera = camera;
    Light* light = new Light();
    lights.push_back(light);
    active_light = light;
    reset_active_camera();
    renderer->rdata.lights = &lights;
    
    gl_renderer.program_flat    = InitShader("shaders/flat_vshader.glsl",    "shaders/flat_fshader.glsl");
    gl_renderer.program_gouraud = InitShader("shaders/gouraud_vshader.glsl", "shaders/gouraud_fshader.glsl");
    gl_renderer.program_phong   = InitShader("shaders/phong_vshader.glsl",   "shaders/phong_fshader.glsl");
    gl_renderer.program_toon    = InitShader("shaders/toon_vshader.glsl", "shaders/toon_fshader.glsl");
    
    gl_renderer.program_skybox  = InitShader("shaders/skybox_vshader.glsl", "shaders/skybox_fshader.glsl");
    GLfloat left = active_camera->left;
    GLfloat right = active_camera->right;
    GLfloat top = active_camera->top;
    GLfloat bottom = active_camera->bottom;
    GLfloat z_near = active_camera->z_near;
    GLfloat z_far = active_camera->z_far;
    
    vec3 cube_pos[8] = { vec3(-50.f, -50.f, -50.f), // 0
        vec3(50.f, -50.f, -50.f), // 1
        vec3(50.f,  50.f, -50.f), // 2
        vec3(-50.f,  50.f, -50.f), // 3
        vec3(-50.f, -50.f,  50.f), // 4
        vec3(50.f, -50.f,  50.f), // 5
        vec3(50.f,  50.f,  50.f), // 6
        vec3(-50.f,  50.f,  50.f) // 7
    };
    
    short cube_indices[] = {
        0, 2, 1,
        0, 3, 2,
        
        1, 2, 6, 
        6, 5, 1, 
        
        4, 5, 6, 
        6, 7, 4, 
        
        2, 3, 6, 
        6, 3, 7, 
        
        0, 7, 3, 
        0, 4, 7, 
        
        0, 1, 5, 
        0, 5, 4
    };
    
    vec3 skybox_positions[36] = {};
    
    for (int i = 0; i < 36; i++) {
        skybox_positions[i] = cube_pos[cube_indices[i]];
    }
    
    glGenVertexArrays(1, &gl_renderer.skybox_vertex_array);
    glBindVertexArray(gl_renderer.skybox_vertex_array);
    
    GLsizei skybox_buffer_size = 36 * sizeof(vec3);
    glGenBuffers(1, &gl_renderer.skybox_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, gl_renderer.skybox_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, skybox_buffer_size, (GLfloat*)(skybox_positions), GL_STATIC_DRAW);
    
    skybox_path = "skyboxes/sky_0.jpg";
    load_skybox();
    
    // TODO(NJ): Add geometry shader for drawing the normals!
    gl_renderer.program_normal = InitShader("shaders/normal_vshader.glsl", "shaders/normal_fshader.glsl");
    gl_renderer.program_ui = InitShader("shaders/ui_vshader.glsl", "shaders/ui_fshader.glsl");
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
}

Scene::Scene() : Scene(new Renderer(512, 512)) {
    renderer = new Renderer(512, 512);
}

Scene::~Scene() {
    for (auto it : models) {
        delete it;
    }
    for (auto it : cameras) {
        delete it;
    }
    for (auto it : lights) {
        delete it;
    }
    
    delete renderer;
}

void Scene::update_aspect_ratio(int width, int height) {
    gl_renderer.width = width;
    gl_renderer.height = height;
    glViewport(0, 0, gl_renderer.width, gl_renderer.height);
    
    // renderer->allocate_buffers(width, height);
    for (auto it : cameras) {
        it->update_aspect_ratio(width, height);
    }
}

void Scene::update_aspect_ratio() {
    renderer->reallocate_buffers();
    for (auto it : cameras) {
        it->update_aspect_ratio(renderer->width, renderer->height);
    }
}

void Scene::load_obj_model(std::string file_name) {
    MeshModel *model = new MeshModel(file_name);
    add_model(model);
}

void Scene::add_model(MeshModel *model) {
    if(model) {
        models.push_back(model);
        active_model = model;
        
        glGenVertexArrays(1, &model->gl_data.vertex_array);
        glBindVertexArray(model->gl_data.vertex_array);
        
        GLsizei vertex_buffer_size = model->triangle_count*3*sizeof(VertexData);
        glGenBuffers(1, &model->gl_data.vertex_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, model->gl_data.vertex_buffer);
        glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, (GLfloat *)(model->vertices), GL_STATIC_DRAW);
        
        GLsizei face_buffer_size = model->triangle_count*3*sizeof(FaceData);
        glGenBuffers(1, &model->gl_data.face_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, model->gl_data.face_buffer);
        glBufferData(GL_ARRAY_BUFFER, face_buffer_size, (GLfloat *)(model->faces), GL_STATIC_DRAW);
        
        if(model->uv_coords) {
            GLsizei uv_buffer_size = model->triangle_count*3*sizeof(vec2);
            glGenBuffers(1, &model->gl_data.uv_buffer);
            glBindBuffer(GL_ARRAY_BUFFER, model->gl_data.uv_buffer);
            glBufferData(GL_ARRAY_BUFFER, uv_buffer_size, (GLfloat *)(model->uv_coords), GL_STATIC_DRAW);
        }
        
        GLsizei index_buffer_size = model->triangle_count*3*sizeof(GLint);
        glGenBuffers(1, &model->gl_data.index_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, model->gl_data.index_buffer);
        glBufferData(GL_ARRAY_BUFFER, index_buffer_size, (GLint *)(model->indices), GL_STATIC_DRAW);
        
        glGenBuffers(1, &model->gl_data.bounding_box_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, model->gl_data.bounding_box_buffer);
        glBufferData(GL_ARRAY_BUFFER, MeshModel::bounding_box_corner_count*sizeof(vec3),
                     model->bounding_box, GL_STATIC_DRAW);
    }
}

void Scene::add_camera(Camera *camera) {
    if(camera) {
        cameras.push_back(camera);
        active_camera = camera;
    }
}

void Scene::add_camera() {
    add_camera(new Camera(renderer->rdata.width, renderer->rdata.height));
}


void Scene::draw() {
    opengl_render();
    return;
    if((renderer->rdata.width <= 0) || (renderer->rdata.height <= 0)) return;
    
    renderer->clear_buffers();
    renderer->set_camera_transform(active_camera->get_transform());
    renderer->set_projection(active_camera->get_projection());
    renderer->rdata.active_camera = active_camera;
    
    if (settings & Scene_Settings::Fog_Effect)
        renderer->rdata.fog_calculation = &fog_effect_calculation;
    else
        renderer->rdata.fog_calculation = &empty_vec4;
    
    for(auto m : models) {
        vec4 color(1.0);
        
        renderer->rdata.world_transform = m->world_transform;
        renderer->rdata.model_transform = m->model_transform;
        renderer->rdata.normal_transform = m->normal_transform;
        renderer->rdata.material = &m->material;
        
        mat4 normalized_device_transform = renderer->get_normalized_device_transform();
        
        if(!m->is_in_clip_space(normalized_device_transform) ||
           !(m->enabled)) {
            continue;
        }
        
        if(m->settings & Model_Settings::DrawBoundingBox) {
            vec4 bounding_color = color;
            bounding_color[0] = is_active_model(m) ? 0.5f : 0.0f;
            renderer->draw_bounding_box(m->bounding_box, bounding_color);
        }
        
        switch(m->material.shading_mode) {
            case Shading_Mode::Flat: {
                // renderer->raster_flat.load_triangles(m->vertex_positions, m->face_normals, m->triangle_count);
            } break;
            
            case Shading_Mode::Gouraud: {
                // renderer->raster_gouraud.load_triangles(m->vertex_positions, m->vertex_normals, m->vertex_indices, m->triangle_count);
            } break;
            
            case Shading_Mode::Phong: {
                // renderer->raster_phong.load_triangles(m->vertex_positions, m->vertex_normals, m->vertex_indices, m->triangle_count);
            } break;
            
            case Shading_Mode::Experimental: {
                // renderer->raster_experimental.load_triangles(m->vertex_positions, m->triangle_count, color);
            } break;
            
            default: {
                assert(!"Unsupported shading mode");
            } break;
        }
        
        if(m->settings & Model_Settings::Draw_Wireframe) {
            vec4 wireframe_color = color;
            wireframe_color[3] = .15f;
            // renderer->draw_wireframe(m->vertex_positions, m->triangle_count, wireframe_color);
        }
        
        if (m->settings & Model_Settings::DrawFaceNormals) {
            // renderer->draw_face_normals(m->face_normals, m->triangle_count, vec4(.75f, .25f, 1.f, 1.f));
        }
        
        if(m->settings & Model_Settings::DrawVertexNormals)  {
            // renderer->draw_vertex_normals(m->vertex_positions, m->vertex_normals, m->triangle_count);
        }
    }
    
    renderer->raster_experimental.draw_triangles();
    renderer->raster_flat.draw_triangles();
    renderer->raster_gouraud.draw_triangles();
    renderer->raster_phong.draw_triangles();
    
    if(settings & Scene_Settings::DrawUiElements) {
        if(settings & Scene_Settings::CameraMode) {
            float min_x = -1.0f, max_x = (float)(renderer->rdata.width-1)/(float)(renderer->rdata.width);
            float min_y = -1.0f, max_y = (float)(renderer->rdata.height-1)/(float)(renderer->rdata.height);
            
            vec4 color = vec4(.5f, .0f, .5f, 1.0f);
            if(settings & Scene_Settings::CameraRotatesAroundAt) {
                color = vec4(.5f, .5f, .0f, 1.0f);
            }
            int w = renderer->rdata.width-1;
            int h = renderer->rdata.height-1;
            for(int i = 0; i < renderer->ssaa_factor*2; ++i) {
                renderer->plot_line(0, i, w, i, color);
            }
        }
        
        for(auto c : cameras) {
            if(is_active_camera(c)) continue;
            renderer->draw_camera_ui(c);
        }
        
        for(auto l : lights) {
            renderer->draw_light_ui(l);
        }
        
        if(active_model && (settings & Scene_Settings::ModelMode)) {
            auto m = active_model;
            
            renderer->rdata.world_transform = m->world_transform;
            renderer->rdata.model_transform = m->model_transform;
            renderer->rdata.normal_transform = m->normal_transform;
            
            // renderer->draw_selected_wireframe(m->vertex_positions, m->triangle_count, vec4(.75f, 1.f, 1.f, .25f));
        }
    }
    
    renderer->swap_buffers();
}

MeshModel* Scene::get_next_meshmodel_address(MeshModel* active_model) {
    if (active_model == nullptr) {
        if (models.empty()) return nullptr;
        else return models.front();
    }
    auto it = std::find(models.begin(), models.end(), active_model);
    ++it;
    if (it != models.end())
        return *it;
    return models.front();
}

void Scene::iterate_models() {
    active_model = get_next_meshmodel_address(active_model);
}

bool Scene::is_active_model(MeshModel* model) {
    return model == active_model;
}

void Scene::unload_active_model() {
    if (active_model == nullptr) return;
    auto it = std::find(models.begin(), models.end(), active_model);
    if (it != models.end()) {
        delete* it;
        models.erase(it);
    }
    
    if (models.empty()) active_model = nullptr;
    else active_model = models.front();
    
}

void Scene::look_at_active_model() {
    if (active_camera == nullptr || active_camera == nullptr) return;
    
    vec3 o = vec3FromAffine(active_model->world_transform * active_model->boundary.center);
    active_camera->look_at(o);
}

Camera* Scene::get_next_camera_address(Camera* active_camera) {
    if (active_camera == nullptr) {
        if (cameras.empty()) return nullptr;
        else return cameras.front();
    }
    auto it = std::find(cameras.begin(), cameras.end(), active_camera);
    ++it;
    if (it != cameras.end())
        return *it;
    return cameras.front();
    
}

void Scene::iterate_cameras() {
    active_camera = get_next_camera_address(active_camera);
}

bool Scene::is_active_camera(Camera* camera) {
    return camera == active_camera;
}

void Scene::unload_active_camera() {
    if (active_camera == nullptr ||
        cameras.size() <= 1) return;
    auto it = std::find(cameras.begin(), cameras.end(), active_camera);
    if (it != cameras.end()) {
        delete *it;
        cameras.erase(it);
    }
    
    if (cameras.empty()) active_camera = nullptr;
    else active_camera = cameras.front();
}

void Scene::rotate_active_model_z(GLfloat theta) {
    if(!active_model) return;
    theta *= model_delta.rotation;
    vec3 axis = (settings & Scene_Settings::ViewFrame) ? active_camera->get_z_axis() : vec3(0, 0, 1);
    active_model->model_transform  = mat3::Rotate(theta, axis)*active_model->model_transform;
    active_model->normal_transform = mat3::Rotate(theta, axis)*active_model->normal_transform;
}

void Scene::rotate_active_model_x(GLfloat theta) {
    if(!active_model) return;
    theta *= model_delta.rotation;
    vec3 axis = (settings & Scene_Settings::ViewFrame) ? active_camera->get_x_axis() : vec3(1, 0, 0);
    active_model->model_transform  = mat3::Rotate(theta, axis)*active_model->model_transform;
    active_model->normal_transform = mat3::Rotate(theta, axis)*active_model->normal_transform;
}

void Scene::rotate_active_model_y(GLfloat theta) {
    if(!active_model) return;
    theta *= model_delta.rotation;
    vec3 axis = (settings & Scene_Settings::ViewFrame) ? active_camera->get_y_axis() : vec3(0, 1, 0);
    active_model->model_transform  = mat3::Rotate(theta, axis)*active_model->model_transform;
    active_model->normal_transform = mat3::Rotate(theta, axis)*active_model->normal_transform;
}

void Scene::scale_active_model(vec3 v) {
    if(!active_model) return;
    active_model->model_transform  *= mat3::Scale(v);
    active_model->normal_transform *= mat3::Scale(1.f/v.x, 1.f/v.y, 1.f/v.z);
}

void Scene::scale_active_model(GLfloat delta) {
    vec3 v = vec3(1.f);
    if(settings & Scene_Settings::Scaling_X) v.x = delta;
    if(settings & Scene_Settings::Scaling_Y) v.y = delta;
    if(settings & Scene_Settings::Scaling_Z) v.z = delta;
    
    scale_active_model(v);
}

void Scene::scale_active_model_up() {
    scale_active_model(model_delta.scaling);
}

void Scene::scale_active_model_down() {
    scale_active_model(1.0f/model_delta.scaling);
}

void Scene::translate_active_model(vec3 v) {
    if(!active_model) return;
    v *= model_delta.translation;
    
    if(settings & Scene_Settings::ViewFrame) {
        v = (v.x*active_camera->get_x_axis() +
             v.y*active_camera->get_y_axis() +
             v.z*active_camera->get_z_axis());
    }
    active_model->world_transform *= mat4::Translate(v);
}

void Scene::reset_active_model_mtransform() {
    if(!active_model) return;
    active_model->model_transform = mat3(1.0);
    active_model->normal_transform = mat3(1.0);
}

void Scene::reset_active_model_wtransform() {
    if(!active_model) return;
    active_model->world_transform = mat4(1.0);
}

void Scene::print_active_model() {
    if(!active_model) return;
    active_model->PrintModelInCFormat();
}


////////////////////////////////////
//CAMERA OPERATIONS:
////////////////////////////////////
void Scene::rotate_active_camera_z(GLfloat theta) {
    if(!active_camera) return;
    
    theta *= camera_delta.rotation;
    vec3 rotation_axis = vec3(0.0f, 0.0f, 1.0f);
    if (settings & Scene_Settings::CameraMovesInWorldSpace) {
        active_camera->global_rotate(theta, rotation_axis);
    }
    else if(settings & Scene_Settings::CameraRotatesAroundAt) {
        active_camera->rotate_around(theta, rotation_axis);
    } else {
        active_camera->rotate(theta, rotation_axis);
    }
}

void Scene::rotate_active_camera_x(GLfloat theta) {
    if(!active_camera) return;
    
    theta *= camera_delta.rotation;
    vec3 rotation_axis = vec3(1.0f, 0.0f, 0.0f);
    if (settings & Scene_Settings::CameraMovesInWorldSpace) {
        active_camera->global_rotate(theta, rotation_axis);
    }
    else if(settings & Scene_Settings::CameraRotatesAroundAt) {
        active_camera->rotate_around(theta, rotation_axis);
    } else {
        active_camera->rotate(theta, rotation_axis);
    }
}

void Scene::rotate_active_camera_y(GLfloat theta) {
    if(!active_camera) return;
    
    theta *= camera_delta.rotation;
    vec3 rotation_axis = vec3(0.0f, 1.0f, 0.0f);
    if (settings & Scene_Settings::CameraMovesInWorldSpace) {
        active_camera->global_rotate(theta, rotation_axis);
    }
    else if(settings & Scene_Settings::CameraRotatesAroundAt) {
        active_camera->rotate_around(theta, rotation_axis);
    } else {
        active_camera->rotate(theta, rotation_axis);
    }
}

void Scene::translate_active_camera(vec3 v) {
    if(!active_camera) return;
    v *= camera_delta.translation;
    
    if (settings & Scene_Settings::CameraMovesInWorldSpace) {
        active_camera->global_translate(v);
    }
    else {
        active_camera->translate(v);
    }
}

void Scene::reset_active_camera() {
    if(!active_camera) return;
    *active_camera = Camera();
    active_camera->update_aspect_ratio(renderer->rdata.width, renderer->rdata.height);
}

void Scene::zoom_in_active_camera() {
    if(!active_camera) return;
    active_camera->zoom(camera_delta.scaling);
}

void Scene::zoom_out_active_camera() {
    active_camera->zoom(1.0f/camera_delta.scaling);
}

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

GLfloat Scene::normalize_mouse_movement_x(int dy) {
    return (float)dy/(float)renderer->rdata.width;
}

GLfloat Scene::normalize_mouse_movement_y(int dx) {
    return (float)dx/(float)renderer->rdata.width;
}

void Scene::model_scaling_switch() {
    using namespace Scene_Settings;
    
    if ((settings & Scaling_X) && (settings & Scaling_Y) && (settings & Scaling_Z)) {
        settings &= ~(Scaling_Y | Scaling_Z);
        settings |=  Scaling_X;
    }
    else if (settings & Scaling_X) {
        settings &= ~(Scaling_X | Scaling_Z);
        settings |=  Scaling_Y;
    }
    else if (settings & Scaling_Y) {
        settings &= ~(Scaling_X | Scaling_Y);
        settings |=  Scaling_Z;
    }
    else {
        settings |= Scaling_All;
    }
}

void Scene::modify_model_translation_delta(GLfloat factor) {
    model_delta.translation += factor;
    model_delta.translation = clamp_min(model_delta.translation, 0.01f); //min val
}

void Scene::modify_model_scaling_delta(GLfloat factor) {
    model_delta.scaling += factor;
    model_delta.scaling = clamp_min(model_delta.scaling, 0.01f); //min val
}

void Scene::modify_model_rotation_delta(GLfloat factor) {
    model_delta.rotation += factor;
    while(model_delta.rotation < 0.0f) {
        model_delta.rotation += 360.0f;
    }
    while(model_delta.rotation >= 360.0f) {
        model_delta.rotation -= 360.0f;
    }
}

void Scene::reset_model_delta() {
    model_delta = SceneActionDelta();
}

void Scene::modify_camera_translation_delta(GLfloat factor) {
    camera_delta.translation += factor;
    camera_delta.translation = clamp_min(camera_delta.translation, 0.01f); //min val
}

void Scene::modify_camera_scaling_delta(GLfloat factor) {
    camera_delta.scaling += factor;
    camera_delta.scaling = clamp_min(camera_delta.scaling, 0.01f); //min val
}

void Scene::modify_camera_rotation_delta(GLfloat factor) {
    camera_delta.rotation += factor;
    while(camera_delta.rotation < 0.0f) {
        camera_delta.rotation += 360.0f;
    }
    while(camera_delta.rotation >= 360.0f) {
        camera_delta.rotation -= 360.0f;
    }
}

void Scene::reset_camera_delta() {
    camera_delta = SceneActionDelta();
}

////////////////////////////////////
//LIGHT SOURCE OPERATIONS:
////////////////////////////////////

Light* Scene::get_next_light_address(Light* active_light) {
    if (active_light == nullptr) {
        if (lights.empty()) return nullptr;
        else return lights.front();
    }
    auto it = std::find(lights.begin(), lights.end(), active_light);
    ++it;
    if (it != lights.end())
        return *it;
    return lights.front();
}

void Scene::iterate_active_light() {
    active_light = get_next_light_address(active_light);
}

void Scene::rotate_active_light_x(GLfloat theta) {
    if (active_light) {
        active_light->direction = mat3::RotateX(theta) * active_light->direction;
    }
}

void Scene::rotate_active_light_y(GLfloat theta) {
    if (active_light) {
        active_light->direction = mat3::RotateY(theta) * active_light->direction;
    }
}

void Scene::rotate_active_light_z(GLfloat theta) {
    if (active_light) {
        active_light->direction = mat3::RotateZ(theta) * active_light->direction;
    }
}

void Scene::move_active_light(vec3& v) {
    if (active_light) {
        active_light->position += v;
    }
}

void Scene::move_active_light_x(GLfloat theta) {
    move_active_light(vec3(theta, 0.f, 0.f));
}

void Scene::move_active_light_y(GLfloat theta) {
    move_active_light(vec3(0.f, theta, 0.f));
}

void Scene::move_active_light_z(GLfloat theta) {
    move_active_light(vec3(0.f, 0.f, theta));
}

void Scene::add_light(Light* light) {
    if (light) {
        lights.push_back(light);
        active_light = light;
    }
}

void Scene::add_light() {
    add_light(new Light(active_camera->at - active_camera->eye, active_camera->eye));
}

void Scene::unload_active_light() {
    if (active_light == nullptr ||
        lights.size() <= 1) return; //will not enable the user to remove all light sources
    auto it = std::find(lights.begin(), lights.end(), active_light);
    if (it != lights.end()) {
        delete* it;
        lights.erase(it);
    }
    
    if (lights.empty()) active_light = nullptr;
    else active_light = lights.front();
}

vec4 empty_vec4(RasterizerData* rdata, GLfloat z, vec4& color) {
    return color;
}

vec4 fog_effect_calculation(RasterizerData* rdata, GLfloat z, vec4& color) {
    GLfloat max_dist = 0.8f;
    GLfloat min_dist = 0.2f;
    
    if (z < 0.2f)
        return color;
    
    vec4& fog_color = rdata->background_color;
    
    GLfloat dist = std::abs(min_dist - z);
    GLfloat fog_factor = (max_dist - dist) / (max_dist - min_dist);
    
    fog_factor = clamp(fog_factor, 0.f, 1.f);
    
    return fog_color * (1 - fog_factor) + color * fog_factor;
}


static const int max_gl_light_count = 16;


inline void Scene::load_light_data(LightData* lights_data) {
    int i = 0;
    for (auto l : lights) {
        if (i == max_gl_light_count)
            break;
        if (!(l->settings & Light_Settings::Enabled)) continue;
        
        if (i >= max_gl_light_count) break;
        lights_data[i].type = l->type;
        
        lights_data[i].attentuation_constant = l->attentuation_constant;
        lights_data[i].attentuation_linear = l->attentuation_linear;
        lights_data[i].attentuation_quadratic = l->attentuation_quadratic;
        
        lights_data[i].position = l->position;
        lights_data[i].direction = l->direction;
        lights_data[i].color = l->color;
        
        l->set_calculation_parameters(lights_data[i].light_data_settings);
        
        ++i;
    }
}

void Scene::load_skybox() {
    glGenTextures(1, &gl_renderer.skybox_texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, gl_renderer.program_skybox);
    
    GLint width, height, channel_count;
    stbi_set_flip_vertically_on_load(false);
    
    if (gl_renderer.skybox_texture != 0) {
        glDeleteTextures(1, &gl_renderer.skybox_texture);
    }
    
    for (int i = 0; i < 6; i++) {
        std::string temp_path = skybox_path;
        temp_path[temp_path.find_last_of('0')] = (char)(i + '0');
        GLubyte* data = stbi_load(temp_path.c_str(), &width, &height, &channel_count, 0);
        if (data) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                         0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else {
            std::cerr << "Failed to load skybox. Path name error." << std::endl;
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

inline void send_light_data(LightData* lights_data, GLuint program) {
    for (int i = 0; i < max_gl_light_count; i++) {
        std::string current_light_str = std::string("lights[" + std::to_string(i) + "]");
        std::string temp_string = current_light_str + ".type";
        GLuint current_lights_attribute_type = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".attentuation_constant";
        GLuint current_lights_attribute_constant = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".attentuation_linear";
        GLuint current_lights_attribute_linear = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".attentuation_quadratic";
        GLuint current_lights_attribute_quadratic = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".position";
        GLuint current_lights_attribute_position = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".direction";
        GLuint current_lights_attribute_direction = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".color";
        GLuint current_lights_attribute_color = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".light_data_settings[0]";
        GLuint current_lights_attribute_settings_0 = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".light_data_settings[1]";
        GLuint current_lights_attribute_settings_1 = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".light_data_settings[2]";
        GLuint current_lights_attribute_settings_2 = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".light_data_settings[3]";
        GLuint current_lights_attribute_settings_3 = glGetUniformLocation(program, temp_string.c_str());
        temp_string = current_light_str + ".light_data_settings[4]";
        GLuint current_lights_attribute_settings_4 = glGetUniformLocation(program, temp_string.c_str());
        
        glUniform1iv(current_lights_attribute_type, 1, (GLint*)&lights_data[i].type);
        glUniform1fv(current_lights_attribute_constant, 1, (GLfloat*)&lights_data[i].attentuation_constant);
        glUniform1fv(current_lights_attribute_linear, 1, (GLfloat*)&lights_data[i].attentuation_linear);
        glUniform1fv(current_lights_attribute_quadratic, 1, (GLfloat*)&lights_data[i].attentuation_quadratic);
        glUniform3fv(current_lights_attribute_position, 1, (GLfloat*)&lights_data[i].position);
        glUniform3fv(current_lights_attribute_direction, 1, (GLfloat*)&lights_data[i].direction);
        glUniform4fv(current_lights_attribute_color, 1, (GLfloat*)&lights_data[i].color);
        glUniform1iv(current_lights_attribute_settings_0, 1, (GLint*)&lights_data[i].light_data_settings[0]);
        glUniform1iv(current_lights_attribute_settings_1, 1, (GLint*)&lights_data[i].light_data_settings[1]);
        glUniform1iv(current_lights_attribute_settings_2, 1, (GLint*)&lights_data[i].light_data_settings[2]);
        glUniform1iv(current_lights_attribute_settings_3, 1, (GLint*)&lights_data[i].light_data_settings[3]);
        glUniform1iv(current_lights_attribute_settings_4, 1, (GLint*)&lights_data[i].light_data_settings[4]);
    }
    
    GLuint max_gl_light_count_uniform = glGetUniformLocation(program, "max_light_count");
    glUniform1iv(max_gl_light_count_uniform, 1, &max_gl_light_count);
}


void Scene::opengl_render() {
    if(gl_renderer.ssaa_factor > 1) {
        glEnable(GL_MULTISAMPLE_ARB);
    } else {
        glDisable(GL_MULTISAMPLE_ARB);
    }
    
    mat4 projection = active_camera->get_projection() * active_camera->get_transform();
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glClearColor(0.0, 0.0, 0.0, 1.0);
    
    if(use_skybox) {
        glDisable(GL_CULL_FACE);
        glDepthMask(GL_FALSE);
        glDisable(GL_DEPTH_TEST);
        
        GLuint program = gl_renderer.program_skybox;
        glUseProgram(program);
        GLuint position_attribute = glGetAttribLocation(program, "pos");
        GLuint projection_loc = glGetUniformLocation(program, "projection");
        glUniformMatrix4fv(projection_loc, 1, GL_TRUE, (GLfloat*)&projection);
        
        
        glBindVertexArray(gl_renderer.skybox_vertex_array);
        glBindBuffer(GL_ARRAY_BUFFER, gl_renderer.skybox_vertex_buffer);
        glEnableVertexAttribArray(position_attribute);
        glBindTexture(GL_TEXTURE_CUBE_MAP, gl_renderer.program_skybox);
        glVertexAttribPointer(position_attribute, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        
        glDepthMask(GL_TRUE);
        glEnable(GL_CULL_FACE);
    }
    
    glEnable(GL_DEPTH_TEST);
    
    LightData lights_data[max_gl_light_count] = {};
    load_light_data(lights_data);
    
    for(auto m : models) {
        GLuint program = 0;
        switch(m->material.shading_mode) {
            case Shading_Mode::Flat: {
                program = gl_renderer.program_flat;
                glShadeModel(GL_FLAT);
            } break;
            case Shading_Mode::Gouraud: {
                glShadeModel(GL_SMOOTH);
                program = gl_renderer.program_gouraud;
            } break;
            case Shading_Mode::Phong: {
                glShadeModel(GL_SMOOTH);
                program = gl_renderer.program_phong;
            } break;
            case Shading_Mode::Toon: {
                glShadeModel(GL_SMOOTH);
                program = gl_renderer.program_toon;
            } break;
            
            default: {
                assert(!"Invalid shading mode!");
            }
        }
        
        glUseProgram(program);
        
        GLuint position_attribute      = glGetAttribLocation(program,  "position");
        GLuint modelview_transform_loc = glGetUniformLocation(program, "modelview_transform");
        
        GLuint normal_attribute     = glGetAttribLocation(program,  "normal");
        GLuint normal_transform_loc = glGetUniformLocation(program, "normal_transform");
        
        GLuint index_attribute = glGetAttribLocation(program, "vertex_index");
        
        glUniform1f(glGetUniformLocation(program, "time"), (GLfloat)fmod(time*.0001f, 16*F_PI));
        glUniform1i(glGetUniformLocation(program, "material.vertex_animation_mode"), m->material.vertex_animation_mode);
        glUniform1i(glGetUniformLocation(program, "material.color_animation_mode"), m->material.color_animation_mode);
        glUniform1i(glGetUniformLocation(program, "material.uv_sampling_mode"), m->material.uv_sampling_mode);
        
        glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.index_buffer);
        
        glEnableVertexAttribArray(index_attribute);
        glVertexAttribPointer(index_attribute, 1, GL_INT, GL_FALSE, 0, 0);
        
        send_light_data(lights_data, program);
        
        GLuint eye_loc = glGetUniformLocation(program, "camera_eye");
        glUniform3fv(eye_loc, 1, (GLfloat *)&active_camera->eye);
        
        
        ModelMaterialStruct temp_material = m->material.getModelMaterialStruct();
        
        glUniform4fv(glGetUniformLocation(program, "material.base_emissive"), 1, (GLfloat*)&temp_material.base_emissive);
        glUniform4fv(glGetUniformLocation(program, "material.base_ambient"),  1, (GLfloat*)&temp_material.base_ambient);
        glUniform4fv(glGetUniformLocation(program, "material.base_diffuse"),  1, (GLfloat*)&temp_material.base_diffuse);
        glUniform4fv(glGetUniformLocation(program, "material.base_specular"), 1, (GLfloat*)&temp_material.base_specular);
        
        glUniform1fv(glGetUniformLocation(program, "material.shininess_coefficient"), 1, (GLfloat*)&temp_material.shininess_coefficient);
        glUniform1fv(glGetUniformLocation(program, "material.emissive_coefficient"), 1, (GLfloat*)&temp_material.emissive_coefficient);
        glUniform1fv(glGetUniformLocation(program, "material.ambient_coefficient"), 1, (GLfloat*)&temp_material.ambient_coefficient);
        glUniform1fv(glGetUniformLocation(program, "material.diffuse_coefficient"), 1, (GLfloat*)&temp_material.diffuse_coefficient);
        glUniform1fv(glGetUniformLocation(program, "material.specular_coefficient"), 1, (GLfloat*)&temp_material.specular_coefficient);
        
        glUniform1i(glGetUniformLocation(program,  "material.texture_mode"), (GLint)temp_material.texture_mode);
        glUniform1i(glGetUniformLocation(program,  "material.use_normal_map"),  (GLint)temp_material.use_normal_map);
        glUniform1i(glGetUniformLocation(program,  "material.use_environment_mapping"), (GLint)temp_material.use_environment_mapping);
        glUniform1fv(glGetUniformLocation(program, "material.reflectiveness"), 1, (GLfloat*)&temp_material.reflectiveness);
        
        glUniform1fv(glGetUniformLocation(program, "material.animate_over_x_coef"), 1, (GLfloat*)&temp_material.animate_over_x_coef);
        glUniform1fv(glGetUniformLocation(program, "material.animate_over_x_range"), 1, (GLfloat*)&temp_material.animate_over_x_range);
        glUniform4fv(glGetUniformLocation(program, "material.animate_over_x_color"), 1, (GLfloat*)&temp_material.animate_over_x_color);
        glUniform1fv(glGetUniformLocation(program, "material.animate_over_y_coef"), 1, (GLfloat*)&temp_material.animate_over_y_coef);
        glUniform1fv(glGetUniformLocation(program, "material.animate_over_y_range"), 1, (GLfloat*)&temp_material.animate_over_y_range);
        glUniform4fv(glGetUniformLocation(program, "material.animate_over_y_color"), 1, (GLfloat*)&temp_material.animate_over_y_color);
        glUniform1fv(glGetUniformLocation(program, "material.animate_over_z_coef"), 1, (GLfloat*)&temp_material.animate_over_z_coef);
        glUniform1fv(glGetUniformLocation(program, "material.animate_over_z_range"), 1, (GLfloat*)&temp_material.animate_over_z_range);
        glUniform4fv(glGetUniformLocation(program, "material.animate_over_z_color"), 1, (GLfloat*)&temp_material.animate_over_z_color);
        
        glUniform1f(glGetUniformLocation(program, "max_x"), m->boundary.max_x);
        glUniform1f(glGetUniformLocation(program, "min_x"), m->boundary.min_x);
        glUniform1f(glGetUniformLocation(program, "may_y"), m->boundary.max_y);
        glUniform1f(glGetUniformLocation(program, "min_y"), m->boundary.min_y);
        glUniform1f(glGetUniformLocation(program, "max_z"), m->boundary.max_z);
        glUniform1f(glGetUniformLocation(program, "min_z"), m->boundary.min_z);
        
        glBindVertexArray(m->gl_data.vertex_array);
        
        glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.vertex_buffer);
        
        mat4 object_transform = m->world_transform * m->model_transform;
        mat4 modelview = projection * object_transform;
        glUniformMatrix4fv(modelview_transform_loc, 1, GL_TRUE, (GLfloat *)&modelview);
        
        glEnableVertexAttribArray(position_attribute);
        glVertexAttribPointer(position_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), 0);
        
        glUniformMatrix3fv(normal_transform_loc, 1, GL_TRUE, (GLfloat *)&m->normal_transform);
        
        GLuint projection_loc = glGetUniformLocation(program, "projection");
        GLuint object_transform_loc = glGetUniformLocation(program, "object_transform");
        glUniformMatrix4fv(projection_loc, 1, GL_TRUE, (GLfloat*)&projection);
        glUniformMatrix4fv(object_transform_loc, 1, GL_TRUE, (GLfloat*)&object_transform);
        
        if(m->material.shading_mode != Shading_Mode::Flat) {
            glEnableVertexAttribArray(normal_attribute);
            glVertexAttribPointer(normal_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*)sizeof(vec3));
            
            if (m->material.shading_mode == Shading_Mode::Toon) {
                glUniform1f(glGetUniformLocation(program, "material.toon_constant"), temp_material.toon_constant);
                glUniform1f(glGetUniformLocation(program, "material.silhouette_threshold"), temp_material.silhouette_threshold);
                glUniform4fv(glGetUniformLocation(program, "material.silhouette_color"), 1, (GLfloat *)&temp_material.silhouette_color);
            }
        } else {
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.face_buffer);
            
            glEnableVertexAttribArray(normal_attribute);
            glVertexAttribPointer(normal_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(FaceData), (void*)sizeof(vec3));
        }
        
        if(m->uv_coords) {
            GLuint uv_attribute = glGetAttribLocation(program, "vuv_coord");
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.uv_buffer);
            glVertexAttribPointer(uv_attribute, 2, GL_FLOAT, GL_FALSE, 0, 0);
            glEnableVertexAttribArray(uv_attribute);
            
            if((m->gl_data.texture_map != 0)) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, m->gl_data.texture_map);
                glUniform1i(glGetUniformLocation(program, "texture_map"), 0);
            }
            
            if ((m->material.use_environment_mapping)) {
                
                glBindTexture(GL_TEXTURE_CUBE_MAP, gl_renderer.program_skybox);
            }
            
            if((m->gl_data.normal_map != 0) && m->uv_coords) {
                glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.face_buffer);
                
                GLuint tangent_attribute = glGetAttribLocation(program, "tangent");
                glVertexAttribPointer(tangent_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(FaceData), (void*)(sizeof(vec3)*2));
                glEnableVertexAttribArray(tangent_attribute);
                
                GLuint bitangent_attribute = glGetAttribLocation(program, "bitangent");
                glVertexAttribPointer(bitangent_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(FaceData), (void*)(sizeof(vec3)*3));
                glEnableVertexAttribArray(bitangent_attribute);
                
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, m->gl_data.normal_map);
                glUniform1i(glGetUniformLocation(program, "normal_map"), 1);
            }
        }
        
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m->triangle_count*3);
        
        if (m->settings & Model_Settings::DrawFaceNormals) {
            program = gl_renderer.program_normal;
            glUseProgram(program);
            
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.face_buffer);
            
            glUniform1f(glGetUniformLocation(program, "length"), .1f);
            
            position_attribute = glGetAttribLocation(program, "position");
            normal_attribute   = glGetAttribLocation(program, "normal");
            
            modelview_transform_loc = glGetUniformLocation(program, "modelview_transform");
            normal_transform_loc    = glGetUniformLocation(program, "normal_transform");
            GLuint projection_loc   = glGetUniformLocation(program, "projection");
            GLuint object_transform_loc = glGetUniformLocation(program, "object_transform");
            
            glUniformMatrix4fv(modelview_transform_loc, 1, GL_TRUE, (GLfloat *)&modelview);
            glUniformMatrix3fv(normal_transform_loc,    1, GL_TRUE, (GLfloat *)&m->normal_transform);
            glUniformMatrix4fv(projection_loc,          1, GL_TRUE, (GLfloat *)&projection);
            glUniformMatrix4fv(object_transform_loc,    1, GL_TRUE, (GLfloat *)&object_transform);
            
            vec4 color = vec4(.75f, .25f, 1.f, 1.f);
            
            GLuint color_loc = glGetUniformLocation(program, "color");
            glUniform4fv(color_loc, 1, (GLfloat *)&color);
            
            glEnableVertexAttribArray(position_attribute);
            glVertexAttribPointer(position_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(vec3)*3, 0);
            
            glEnableVertexAttribArray(normal_attribute);
            glVertexAttribPointer(normal_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(vec3)*3, (void*)sizeof(vec3));
            
            glDrawArrays(GL_LINES, 0, m->triangle_count*sizeof(FaceData));
        }
        
        if (m->settings & Model_Settings::DrawVertexNormals) {
            program = gl_renderer.program_normal;
            glUseProgram(program);
            
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.vertex_buffer);
            
            glUniform1f(glGetUniformLocation(program, "length"), .1f);
            
            position_attribute = glGetAttribLocation(program, "position");
            normal_attribute   = glGetAttribLocation(program, "normal");
            
            modelview_transform_loc = glGetUniformLocation(program, "modelview_transform");
            normal_transform_loc    = glGetUniformLocation(program, "normal_transform");
            GLuint projection_loc   = glGetUniformLocation(program, "projection");
            GLuint object_transform_loc = glGetUniformLocation(program, "object_transform");
            
            glUniformMatrix4fv(modelview_transform_loc, 1, GL_TRUE, (GLfloat *)&modelview);
            glUniformMatrix3fv(normal_transform_loc,    1, GL_TRUE, (GLfloat *)&m->normal_transform);
            glUniformMatrix4fv(projection_loc,          1, GL_TRUE, (GLfloat *)&projection);
            glUniformMatrix4fv(object_transform_loc,    1, GL_TRUE, (GLfloat *)&object_transform);
            
            vec4 color = vec4(.25f, .45f, .9f, 1.f);
            
            GLuint color_loc = glGetUniformLocation(program, "color");
            glUniform4fv(color_loc, 1, (GLfloat *)&color);
            
            glEnableVertexAttribArray(position_attribute);
            glVertexAttribPointer(position_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(vec3)*3, 0);
            
            glEnableVertexAttribArray(normal_attribute);
            glVertexAttribPointer(normal_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(vec3)*3, (void*)sizeof(vec3));
            
            glDrawArrays(GL_LINES, 0, m->triangle_count*sizeof(vec3));
        }
        
        if(m->settings & Model_Settings::DrawBoundingBox) {
            program = gl_renderer.program_ui;
            glUseProgram(program);
            
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.bounding_box_buffer);
            
            position_attribute      = glGetAttribLocation(program,  "position");
            modelview_transform_loc = glGetUniformLocation(program, "modelview_transform");
            
            glUniformMatrix4fv(modelview_transform_loc, 1, GL_TRUE, (GLfloat *)&modelview);
            
            vec4 color(1.0);
            color.r = is_active_model(m) ? 0.5f : 0.0f;
            
            GLuint color_loc = glGetUniformLocation(program, "color");
            glUniform4fv(color_loc, 1, (GLfloat *)&color);
            
            glEnableVertexAttribArray(position_attribute);
            glVertexAttribPointer(position_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), 0);
            glDrawArrays(GL_LINES, 0, MeshModel::bounding_box_corner_count*3);
        }
    }
    
    
    if(settings & Scene_Settings::DrawUiElements) {
        GLuint program = gl_renderer.program_ui;
        glUseProgram(program);
        
        GLuint position_attribute = glGetAttribLocation(program, "position");
        GLuint modelview_transform_loc = glGetUniformLocation(program, "modelview_transform");
        
#if 0
        if(settings & Scene_Settings::CameraMode) {
            float min_x = -1.0f, max_x = (float)(renderer->rdata.width-1)/(float)(renderer->rdata.width);
            float min_y = -1.0f, max_y = (float)(renderer->rdata.height-1)/(float)(renderer->rdata.height);
            
            vec4 color = vec4(.5f, .0f, .5f, 1.0f);
            if(settings & Scene_Settings::CameraRotatesAroundAt) {
                color = vec4(.5f, .5f, .0f, 1.0f);
            }
            // int w = renderer->rdata.width-1;
            // int h = renderer->rdata.height-1;
            for(int i = 0; i < renderer->ssaa_factor*2; ++i) {
                // renderer->plot_line(0, i, w, i, color);
            }
        }
        
        for(auto c : cameras) {
            if(is_active_camera(c)) continue;
            // renderer->draw_camera_ui(c);
        }
        
        for(auto l : lights) {
            // renderer->draw_light_ui(l);
        }
#endif
        
        if(active_model && (settings & Scene_Settings::ModelMode)) {
            auto m = active_model;
            
            GLuint index_attribute = glGetAttribLocation(program, "vertex_index");
            
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.index_buffer);
            
            glEnableVertexAttribArray(index_attribute);
            glVertexAttribIPointer(index_attribute, 1, GL_INT, 0, 0);
            
            glBindBuffer(GL_ARRAY_BUFFER, m->gl_data.vertex_buffer);
            
            glUniform1f(glGetUniformLocation(program, "time"), (GLfloat)fmod(time*.0001f, 16*F_PI));
            glUniform1i(glGetUniformLocation(program, "material.vertex_animation_mode"), m->material.vertex_animation_mode);
            glUniform1i(glGetUniformLocation(program, "material.color_animation_mode"), m->material.color_animation_mode);
            
            mat4 transformation = m->world_transform * m->model_transform;
            mat4 modelview = projection * transformation;
            
            glUniformMatrix4fv(modelview_transform_loc, 1, GL_TRUE, (GLfloat *)&modelview);
            
            vec4 color = vec4(.75f, 1.f, 1.f, .1f);
            GLuint color_loc = glGetUniformLocation(program, "color");
            glUniform4fv(color_loc, 1, (GLfloat *)&color);
            
            glEnableVertexAttribArray(position_attribute);
            glVertexAttribPointer(position_attribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), 0);
            
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
            glEnable(GL_BLEND);
            // glDisable(GL_DEPTH_TEST);
            glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m->triangle_count*3);
            glDisable(GL_BLEND);
            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
        }
    }
    
    glFlush();
    glutSwapBuffers();
}