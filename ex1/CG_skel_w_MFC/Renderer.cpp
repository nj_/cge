#include "StdAfx.h"
#include "Renderer.h"
#include <cassert>
#include "CG_skel_w_MFC.h"
#include "InitShader.h"
#include "GL\freeglut.h"

Renderer::Renderer(int width, int height) :
raster_flat(&rdata), raster_gouraud(&rdata), raster_phong(&rdata), raster_experimental(&rdata) {
    rdata.rgba_buffer = nullptr;
    rdata.z_buffer = nullptr;
    rdata.width = width;
    rdata.height = height;
    rdata.material = nullptr;
    init_opengl_rendering();
    allocate_buffers(width, height);
}

Renderer::Renderer() : Renderer(1920, 1080) {
}

Renderer::~Renderer(void) {
    free(rdata.rgba_buffer);
    free(rdata.z_buffer);
    if(subsampling_buffer) {
        free(subsampling_buffer);
    }
}

void Renderer::reallocate_buffers() {
    allocate_buffers(width, height);
}

void Renderer::allocate_buffers(int width, int height) {
    this->width = width;
    this->height = height;
    if(ssaa_factor > 1) {
        assert(ssaa_factor == 2 || ssaa_factor == 3 || ssaa_factor == 4);
        subsampling_buffer = (GLfloat *)realloc(subsampling_buffer, sizeof(__m128) * width * height);
        width  *= ssaa_factor;
        height *= ssaa_factor;
    }
    rdata.width = width;
    rdata.height = height;
    create_opengl_buffer(); //Do not remove this line.
    rdata.rgba_buffer = (GLfloat *)realloc(rdata.rgba_buffer, sizeof(__m128) * width * height);
    rdata.z_buffer = (GLfloat *)realloc(rdata.z_buffer, sizeof(GLfloat) * width * height);
}

void Renderer::set_camera_transform(const mat4& camera_transform) {
    rdata.camera_transform = camera_transform;
}

void Renderer::set_projection(const mat4& projection) {
    rdata.projection = projection;
}

mat4 Renderer::get_normalized_device_transform() {
    return rdata.projection * rdata.camera_transform * rdata.world_transform * rdata.model_transform;
}

void Renderer::clear_buffers() {
    for(int y = 0; y < rdata.height; ++y) {
        for(int x = 0; x < rdata.width; ++x) {
            _mm_store_ps(rdata.rgba_buffer + INDEX_RGBA(rdata.width, x, y, 0),
                         rdata.background_color.value);
            rdata.z_buffer[INDEX_VALUE(rdata.width, x, y)] = FLT_MAX;
        }
    }
}

void Renderer::draw_wireframe(const vec3 *vertices, const size_t count, const vec4 color) {
    mat4 transform = get_normalized_device_transform();
    
    for (int i = 0; i < count; ++i) {
        vec4 v0 = transform * vertices[i*3 + 0];
        vec4 v1 = transform * vertices[i*3 + 1];
        vec4 v2 = transform * vertices[i*3 + 2];
        
        draw_line(v0, v1, color);
        draw_line(v0, v2, color);
        draw_line(v1, v2, color);
    }
}

void Renderer::draw_selected_wireframe(const vec3 *vertices, const size_t count, const vec4 color) {
    mat4 transform = get_normalized_device_transform();
    
    for (int i = 0; i < count; ++i) {
        vec4 v0 = transform * vertices[i*3 + 0];
        vec4 v1 = transform * vertices[i*3 + 1];
        vec4 v2 = transform * vertices[i*3 + 2];
        
        // Backface culling {
        vec3 cross_normal = cross(v1-v0, v2-v0);
        if(cross_normal.z <= 0) continue;
        //}
        
        draw_line(v0, v1, color);
        draw_line(v0, v2, color);
        draw_line(v1, v2, color);
    }
}

void Renderer::draw_face_normals(const vec3 *normals, const size_t count, const vec4 color) {
    mat4 transform = rdata.projection * rdata.camera_transform;
    mat4 object_trasform = rdata.world_transform * rdata.model_transform;
    for (int i = 0; i < count; ++i) {
        vec3 c = vec3FromAffine(object_trasform  * normals[i*2 + 0]);
        vec3 n = normalize(rdata.normal_transform * normals[i*2 + 1])*.2f;
        n = vec3FromAffine(transform * (c + n));
        c = vec3FromAffine(transform * c);
        draw_line(c, n, color);
    }
}

void Renderer::draw_vertex_normals(const vec3 *vertices, const vec3 *normals,
                                   const size_t count, const vec4 color) {
    mat4 transform = rdata.projection * rdata.camera_transform;
    mat4 object_trasform = rdata.world_transform * rdata.model_transform;
    for (int i = 0; i < count*3; ++i) {
        vec3 v = vec3FromAffine(object_trasform * vertices[i]);
        vec3 n = normalize(rdata.normal_transform * normals[i])*.2f;
        
        n = vec3FromAffine(transform * (v + n));
        v = vec3FromAffine(transform * v);
        
        draw_line(v, n, color);
    }
}

void Renderer::draw_axes(vec4 color_x, vec4 color_y, vec4 color_z) {
    mat4 transform = rdata.projection * rdata.camera_transform;
    
    vec4 o = transform * vec3(0.0, 0.0, 0.0);
    vec4 x = transform * vec3(1.0, 0.0, 0.0);
    vec4 y = transform * vec3(0.0, 1.0, 0.0);
    vec4 z = transform * vec3(0.0, 0.0, 1.0);
    
    draw_line(o, x, color_x);
    draw_line(o, y, color_y);
    draw_line(o, z, color_z);
}

void Renderer::draw_camera_axes(vec3 eye, vec3 x_axis, vec3 y_axis, vec3 z_axis,
                                vec4 color_x, vec4 color_y, vec4 color_z) {
    mat4 transform = rdata.projection * rdata.camera_transform;
    
    vec3 o  = vec3FromAffine(transform * eye);
    vec3 x0 = vec3FromAffine(transform * (eye - x_axis*.125f));
    vec3 x1 = vec3FromAffine(transform * (eye + x_axis*.125f));
    vec3 y  = vec3FromAffine(transform * (eye + y_axis*.125f));
    vec3 z  = vec3FromAffine(transform * (eye + z_axis*.5f));
    
    draw_line(x0, x1, color_x);
    draw_line(o, y, color_y);
    draw_line(o, z, color_z);
}

void Renderer::draw_camera_ui(Camera *camera) {
    static const vec4 color_x = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    static const vec4 color_y = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    static const vec4 color_z = vec4(0.0f, 0.0f, 1.0f, 1.0f);
    
    mat4 transform = rdata.projection * rdata.camera_transform;
    vec3 o  = vec3FromAffine(transform * camera->eye);
    vec3 x0 = vec3FromAffine(transform * (camera->eye - camera->get_x_axis()*.125f));
    vec3 x1 = vec3FromAffine(transform * (camera->eye + camera->get_x_axis()*.125f));
    vec3 y  = vec3FromAffine(transform * (camera->eye + camera->get_y_axis()*.125f));
    vec3 z  = vec3FromAffine(transform * (camera->eye + camera->get_z_axis()*.5f));
    
    draw_line(x0, x1, color_x);
    draw_line(o, y, color_y);
    draw_line(o, z, color_z);
}

void Renderer::draw_light_ui(Light *light) {
    if(light->type == Light_Type::Ambient) return;
    mat4 transform = rdata.projection * rdata.camera_transform;
    vec4 color = light->color;
    if((light->settings & Light_Settings::Enabled) == 0) {
        color.a = .25f;
    }
    if(light->type == Light_Type::Point) {
        {
            vec3 p0 = vec3FromAffine(transform * (light->position + vec3(-.25f, 0, 0)));
            vec3 p1 = vec3FromAffine(transform * (light->position + vec3( .25f, 0, 0)));
            draw_line(p0, p1, color);
        }
        {
            vec3 p0 = vec3FromAffine(transform * (light->position + vec3(0, -.25f, 0)));
            vec3 p1 = vec3FromAffine(transform * (light->position + vec3(0,  .25f, 0)));
            draw_line(p0, p1, color);
        }
        {
            vec3 p0 = vec3FromAffine(transform * (light->position + vec3(0, 0, -.15f)));
            vec3 p1 = vec3FromAffine(transform * (light->position + vec3(0, 0,  .15f)));
            draw_line(p0, p1, color);
        }
        {
            vec3 p0 = vec3FromAffine(transform * (light->position + vec3(-.15f, -.15f, 0)));
            vec3 p1 = vec3FromAffine(transform * (light->position + vec3( .15f,  .15f, 0)));
            draw_line(p0, p1, color);
        }
        {
            vec3 p0 = vec3FromAffine(transform * (light->position + vec3(-.15f,  .15f, 0)));
            vec3 p1 = vec3FromAffine(transform * (light->position + vec3( .15f, -.15f, 0)));
            draw_line(p0, p1, color);
        }
    } else if(light->type == Light_Type::Parallel) {
        vec3 p = light->position;
        vec3 d = light->direction;
        {
            vec3 p0 = vec3FromAffine(transform * (p));
            vec3 p1 = vec3FromAffine(transform * (p + d));
            draw_line(p0, p1, color);
        }
        {
            vec3 p0 = vec3FromAffine(transform * (p));
            vec3 p1 = vec3FromAffine(transform * (d + p + vec3(0, .25f, 0)));
            draw_line(p0, p1, color);
        }
        {
            vec3 p0 = vec3FromAffine(transform * (p));
            vec3 p1 = vec3FromAffine(transform * (d + p + vec3(0,-.25f, 0)));
            draw_line(p0, p1, color);
        }
    }
}

void Renderer::draw_bounding_box(const vec3 *bounding_box, const vec4 color) {
    mat4 transform = rdata.projection * rdata.camera_transform * rdata.world_transform * rdata.model_transform;
    
    vec4 v000 = transform * bounding_box[0];
    vec4 v100 = transform * bounding_box[1];
    vec4 v010 = transform * bounding_box[2];
    vec4 v001 = transform * bounding_box[3];
    vec4 v110 = transform * bounding_box[4];
    vec4 v101 = transform * bounding_box[5];
    vec4 v011 = transform * bounding_box[6];
    vec4 v111 = transform * bounding_box[7];
    
    draw_line(v000, v100, color);
    draw_line(v000, v010, color);
    draw_line(v000, v001, color);
    
    draw_line(v100, v110, color);
    draw_line(v100, v101, color);
    
    draw_line(v010, v110, color);
    draw_line(v010, v011, color);
    
    draw_line(v001, v101, color);
    draw_line(v001, v011, color);
    
    draw_line(v111, v110, color);
    draw_line(v111, v101, color);
    draw_line(v111, v011, color);
}

inline int absolute_val(int n) {
    return n < 0 ? -n : n;
}

//The following plotting algorithm is based on https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm as reference.

inline void Renderer::fill_pixel_blend(int x, int y, const vec4 color) {
    __m128 pixel = _mm_load_ps(rdata.rgba_buffer + INDEX_RGBA(rdata.width, x, y, 0));
    __m128 pscalar = _mm_set1_ps(1.f-color.a);
    
    __m128 value = color.value;
    __m128 vscalar = _mm_set1_ps(color.a);
    
    pixel = _mm_mul_ps(pixel, pscalar);
    value = _mm_mul_ps(value, vscalar);
    
    pixel = _mm_add_ps(pixel, value);
    _mm_store_ps(rdata.rgba_buffer + INDEX_RGBA(rdata.width, x, y, 0), pixel);
    
    rdata.z_buffer[INDEX_VALUE(rdata.width, x, y)] = -3000.f;
}

inline void Renderer::fill_pixel(int x, int y, const vec4 color) {
    _mm_store_ps(rdata.rgba_buffer + INDEX_RGBA(rdata.width, x, y, 0), color.value);
    rdata.z_buffer[INDEX_VALUE(rdata.width, x, y)] = -3000.f;
}

inline
void Renderer::plot_line_x(int x, int y, int x2, int y2, const vec4 color) {
    int dx = x2 - x;
    int dy = y2 - y;
    int yi = 1;
    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }
    
    int d = (2 * dy) - dx;
    int de = 2 * dy;
    int dne = de - (2 * dx);
    
    for (; x <= x2; ++x) {
        fill_pixel_blend(x, y, color);
        if (d < 0) {
            d += de;
        }
        else {
            y += yi;
            d += dne;
        }
    }
}

inline
void Renderer::plot_line_y(int x, int y, int x2, int y2, const vec4 color) {
    int dx = x2 - x;
    int dy = y2 - y;
    int xi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }
    
    int d = (2 * dx) - dy;
    int de = 2 * dx;
    int dne = de - (2 * dy);
    
    for (; y <= y2; ++y) {
        fill_pixel_blend(x, y, color);
        if (d < 0) {
            d += de;
        }
        else {
            x += xi;
            d += dne;
        }
    }
}

inline void Renderer::plot_line(int x1, int y1, int x2, int y2, vec4 color) {
    int abs_dx = absolute_val((x2 - x1));
    int abs_dy = absolute_val((y2 - y1));
    
    if (abs_dy < abs_dx) {
        if (x1 <= x2) {
            plot_line_x(x1, y1, x2, y2, color);
        }
        else {
            plot_line_x(x2, y2, x1, y1, color);
        }
    }
    else {
        if (y1 <= y2) {
            plot_line_y(x1, y1, x2, y2, color);
        }
        else {
            plot_line_y(x2, y2, x1, y1, color);
        }
    }
}

inline void Renderer::draw_line(vec4 v1, vec4 v2, vec4 color) {
    { // clip the line to the view space:
        //The following clipping algorithm is based on https://en.wikipedia.org/wiki/Liang%E2%80%93Barsky_algorithm as reference.
        
        if(!z_in_bounds(v1) && !z_in_bounds(v2)) return;
        
        v1 /= v1.w;
        v2 /= v2.w;
        
        GLfloat x[2] = { v1.x , v2.x };
        GLfloat y[2] = { v1.y , v2.y };
        GLfloat z[2] = { v1.z , v2.z };
        
        GLfloat min = -1.0f,  max = 1.0f;
        
        GLfloat p[6] = {
            -(x[1] - x[0]) , -p[0],
            -(y[1] - y[0]) , -p[2],
            -(z[1] - z[0]) , -p[4],
        };
        
        GLfloat q[6] = {
            x[0] - min, max - x[0],
            y[0] - min, max - y[0],
            z[0] - min, max - z[0],
        };
        
        if((around_zero_epsilon(p[0]) && q[0] < 0.0f) ||
           (around_zero_epsilon(p[1]) && q[1] < 0.0f) ||
           (around_zero_epsilon(p[2]) && q[2] < 0.0f) ||
           (around_zero_epsilon(p[3]) && q[3] < 0.0f) ||
           (around_zero_epsilon(p[4]) && q[4] < 0.0f) ||
           (around_zero_epsilon(p[5]) && q[5] < 0.0f))
            return;
        
        GLfloat u[2] = { 0.0f, 1.0f };
        
        for (int i = 0; i < 6; i = i+2) {
            if (!around_zero_epsilon(p[i])) {
                const GLfloat r[2] = { q[i] / p[i] , q[i+1] / p[i+1] };
                if (p[i] < 0.0f) {
                    u[0] = maximum(u[0], r[0]);
                    u[1] = minimum(u[1], r[1]);
                } else {
                    u[0] = maximum(u[0], r[1]);
                    u[1] = minimum(u[1], r[0]);
                }
            }
        }
        
        if (u[0] > u[1]) {
            return;
        }
        
        v1.x = x[0] + (p[1] * u[0]);
        v1.y = y[0] + (p[3] * u[0]);
        
        v2.x = x[0] + (p[1] * u[1]);
        v2.y = y[0] + (p[3] * u[1]);
    }
    
    // TODO(NJ): clip width/height in a better way.
    // REPLY(OZ): we could round integers ourselves, or use cmath's round function, but testing shows there's no reason.
    //            let's decide this later.
    int x1 = SCREEN_COORD(v1.x, rdata.width);
    int y1 = SCREEN_COORD(v1.y, rdata.height);
    int x2 = SCREEN_COORD(v2.x, rdata.width);
    int y2 = SCREEN_COORD(v2.y, rdata.height);
    
    plot_line(x1, y1, x2, y2, color);
}


/////////////////////////////////////////////////////
//OpenGL stuff. Don't touch.

void Renderer::init_opengl_rendering()
{
    int a = glGetError();
    a = glGetError();
    glGenTextures(1, &gScreenTex);
    a = glGetError();
    glGenVertexArrays(1, &gScreenVtc);
    GLuint buffer;
    glBindVertexArray(gScreenVtc);
    glGenBuffers(1, &buffer);
    const GLfloat vtc[]={
        -1, -1,
        1, -1,
        -1, 1,
        -1, 1,
        1, -1,
        1, 1
    };
    const GLfloat tex[]={
        0,0,
        1,0,
        0,1,
        0,1,
        1,0,
        1,1};
    
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vtc)+sizeof(tex), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vtc), vtc);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vtc), sizeof(tex), tex);
    
    GLuint program = InitShader("vshader-software.glsl", "fshader-software.glsl");
    glUseProgram(program);
    GLint  vPosition = glGetAttribLocation(program, "vPosition");
    
    glEnableVertexAttribArray(vPosition);
    glVertexAttribPointer(vPosition, 2, GL_FLOAT, GL_FALSE, 0, 0);
    
    GLint  vTexCoord = glGetAttribLocation(program, "vTexCoord");
    glEnableVertexAttribArray(vTexCoord);
    glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *) sizeof(vtc));
    glProgramUniform1i(program, glGetUniformLocation(program, "texture"), 0);
    a = glGetError();
}

void Renderer::create_opengl_buffer()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gScreenTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, rdata.width, rdata.height, 0, GL_RGB, GL_FLOAT, NULL);
    glViewport(0, 0, rdata.width, rdata.height);
}

static inline void subsample_buffer2(GLfloat *dest, GLfloat *source, int width, int height) {
    __m128 scalar = _mm_set1_ps(4.0f);
    for(int y = 1; y < height; ++y) {
        for(int x = 0; x < width; ++x) {
            __m128 p0 = _mm_load_ps(source+INDEX_RGBA(width*2, (x*2+0), (y*2+0), 0));
            __m128 p1 = _mm_load_ps(source+INDEX_RGBA(width*2, (x*2+1), (y*2+0), 0));
            __m128 p2 = _mm_load_ps(source+INDEX_RGBA(width*2, (x*2+0), (y*2+1), 0));
            __m128 p3 = _mm_load_ps(source+INDEX_RGBA(width*2, (x*2+1), (y*2+1), 0));
            p0 = _mm_add_ps(p0, p1);
            p2 = _mm_add_ps(p2, p3);
            p0 = _mm_add_ps(p0, p2);
            p0 = _mm_div_ps(p0, scalar);
            
            _mm_store_ps(dest + INDEX_RGBA(width, x, y, 0), p0);
        }
    }
}

static inline void subsample_buffer3(GLfloat *dest, GLfloat *source, int width, int height) {
    __m128 scalar = _mm_set1_ps(3.0f);
    __m128 p, p0, p1, p2;
    for(int y = 0; y < height; ++y) {
        for(int x = 0; x < width; ++x) {
            p = _mm_set1_ps(0.0f);
            
            for(int i = 0; i < 3; ++i) {
                p0 = _mm_load_ps(source+INDEX_RGBA(width*3, (x*3+0), (y*3+i), 0));
                p1 = _mm_load_ps(source+INDEX_RGBA(width*3, (x*3+1), (y*3+i), 0));
                p2 = _mm_load_ps(source+INDEX_RGBA(width*3, (x*3+2), (y*3+i), 0));
                p0 = _mm_add_ps(p0, p1);
                p0 = _mm_add_ps(p0, p2);
                p0 = _mm_div_ps(p0, scalar);
                p = _mm_add_ps(p, p0);
            }
            
            p = _mm_div_ps(p, scalar);
            _mm_store_ps(dest + INDEX_RGBA(width, x, y, 0), p);
        }
    }
}

static inline void subsample_buffer4(GLfloat *dest, GLfloat *source, int width, int height) {
    __m128 scalar = _mm_set1_ps(4.0f);
    __m128 p, p0, p1, p2, p3;
    for(int y = 0; y < height; ++y) {
        for(int x = 0; x < width; ++x) {
            p = _mm_set1_ps(0.0f);
            
            for(int i = 0; i < 4; ++i) {
                p0 = _mm_load_ps(source+INDEX_RGBA(width*4, (x*4+0), (y*4+i), 0));
                p1 = _mm_load_ps(source+INDEX_RGBA(width*4, (x*4+1), (y*4+i), 0));
                p2 = _mm_load_ps(source+INDEX_RGBA(width*4, (x*4+2), (y*4+i), 0));
                p3 = _mm_load_ps(source+INDEX_RGBA(width*4, (x*4+3), (y*4+i), 0));
                p0 = _mm_add_ps(p0, p1);
                p2 = _mm_add_ps(p2, p3);
                p0 = _mm_add_ps(p0, p2);
                p0 = _mm_div_ps(p0, scalar);
                p = _mm_add_ps(p, p0);
            }
            
            p = _mm_div_ps(p, scalar);
            _mm_store_ps(dest + INDEX_RGBA(width, x, y, 0), p);
        }
    }
}

void Renderer::swap_buffers()
{
    int a = glGetError();
    glActiveTexture(GL_TEXTURE0);
    a = glGetError();
    glBindTexture(GL_TEXTURE_2D, gScreenTex);
    a = glGetError();
    if(ssaa_factor == 2) {
        subsample_buffer2(subsampling_buffer, rdata.rgba_buffer, width, height);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_FLOAT, subsampling_buffer);
    } else if(ssaa_factor == 3) {
        subsample_buffer3(subsampling_buffer, rdata.rgba_buffer, width, height);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_FLOAT, subsampling_buffer);
    } else if(ssaa_factor == 4) {
        subsample_buffer4(subsampling_buffer, rdata.rgba_buffer, width, height);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_FLOAT, subsampling_buffer);
    } else {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_FLOAT, rdata.rgba_buffer);
    }
    glGenerateMipmap(GL_TEXTURE_2D);
    a = glGetError();
    
    glBindVertexArray(gScreenVtc);
    a = glGetError();
    glDrawArrays(GL_TRIANGLES, 0, 6);
    a = glGetError();
    glutSwapBuffers();
    a = glGetError();
}