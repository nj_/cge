#pragma once
#include <queue>
#include "vec.h"
#include "mat.h"
#include "RasterizerData.h"

namespace Phong_Raster {
    struct Edge : public ::RasterizerEdge {
        vec3 min_n, max_n;
        vec4 min_color, max_color;
    };
    
    struct Triangle_Edges {
        Edge e0, e1, e2;
        vec3 center_of_triangle;
    };
    
    static auto compare_triangles_min_y = [](Triangle_Edges p1, Triangle_Edges p2) { return p1.e0.min_y > p2.e0.min_y; };
    
    class Rasterizer {
        public:
        RasterizerData *rdata;
        
        std::priority_queue<Triangle_Edges, std::vector<Triangle_Edges>,
        decltype(compare_triangles_min_y)> edge_queue;
        
        Rasterizer(RasterizerData *data);
        
        void load_triangles(const vec3 *vertices, const vec3 *normals, const int *indices, size_t count);
        void draw_triangles();
        
        GLfloat diffuse_summation(vec3 interpolated_normal);
        
        void push_triangle_edges            (vec3 v0, vec3 v1, vec3 v2, vec3 n0, vec3 n1, vec3 n2, vec4 c0, vec4 c1, vec4 c2, vec3 triangle_center);
        void push_flat_bottom_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 n0, vec3 n1, vec3 n2, vec4 c0, vec4 c1, vec4 c2);
        void push_flat_top_triangle_edges   (vec3 v0, vec3 v1, vec3 v2, vec3 n0, vec3 n1, vec3 n2, vec4 c0, vec4 c1, vec4 c2);
    };
}
