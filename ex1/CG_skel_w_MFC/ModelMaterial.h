#pragma once
#include "vec.h"

namespace Shading_Mode {
    enum ShadingMode {
        Flat,
        Gouraud,
        Phong,
		Toon,
        Experimental,
    };
}

namespace Animation_Mode {
    enum AnimationMode {
        None = 0,
        Animation1 = 1,
        Animation2 = 2,
    };
}

struct ModelMaterialStruct
{
	vec4 base_emissive;
	vec4 base_ambient;
	vec4 base_diffuse;
	vec4 base_specular;
	GLfloat shininess_coefficient;
    
	GLfloat emissive_coefficient;
	GLfloat ambient_coefficient;
	GLfloat diffuse_coefficient;
	GLfloat specular_coefficient;
	//TODO(OZ): Connect these to the UI.
	GLfloat toon_constant;
	GLfloat silhouette_threshold;
	vec4  silhouette_color;
    
	int texture_mode;
	bool use_normal_map;
	bool use_environment_mapping;
    
	GLfloat reflectiveness;
    
	/////// Color animation specific:
	bool animate_color_1_x;
	bool animate_color_1_y;
	bool animate_color_1_z;
    
	GLfloat animate_over_x_coef;
	GLfloat animate_over_x_range;
	vec4 animate_over_x_color;
	GLfloat animate_over_y_coef;
	GLfloat animate_over_y_range;
	vec4 animate_over_y_color;
	GLfloat animate_over_z_coef;
	GLfloat animate_over_z_range;
	vec4 animate_over_z_color;
    
	ModelMaterialStruct(vec4 base_emissive, vec4 base_ambient, vec4 base_diffuse,
						vec4 base_specular, GLfloat shininess_coefficient,
						GLfloat emissive_coefficient, GLfloat ambient_coefficient,
                        GLfloat diffuse_coefficient, GLfloat specular_coefficient,
                        GLint texture_mode, bool use_normal_map, bool use_environment_mapping,
						GLfloat reflectiveness, GLfloat toon_constant,
						GLfloat silhouette_threshold, vec4 silhouette_color,
						GLfloat animate_over_x_coef,
						GLfloat animate_over_x_range,
						vec4 animate_over_x_color,
						GLfloat animate_over_y_coef,
						GLfloat animate_over_y_range,
						vec4 animate_over_y_color,
						GLfloat animate_over_z_coef,
						GLfloat animate_over_z_range,
						vec4 animate_over_z_color);
};

class ModelMaterial
{
    public:
	vec4 base_emissive = vec4(1.f, 1.f, 1.f, 1.f);
	vec4 base_ambient  = vec4(1.f, 1.f, 1.f, 1.f);
	vec4 base_diffuse  = vec4(1.f, 1.f, 1.f, 1.f);
	vec4 base_specular = vec4(1.f, 1.f, 1.f, 1.f);
	GLfloat shininess_coefficient = 5.f; //minval = 0.f
    
    bool use_crazy_colors = false;
    GLfloat lerp_to_color_factor = .75f;
    int32_t shading_mode = Shading_Mode::Phong;
    int32_t vertex_animation_mode = 0;
    int32_t color_animation_mode = 0;
    
    int32_t uv_sampling_mode = 0;
    
	GLfloat emissive_coefficient = 0.f;
	GLfloat ambient_coefficient  = .1f;
	GLfloat diffuse_coefficient  = .5f;
	GLfloat specular_coefficient = .5f;
    
	GLfloat toon_constant = 5.f;
	GLfloat silhouette_threshold = 0.1f;
	vec4 silhouette_color = vec4(1.f, 1.f, 1.f, 1.f);
    
	int texture_mode = 0;
	bool use_normal_map = false;
	bool use_environment_mapping = false;
    
	GLfloat reflectiveness = .5f;
    
	/////// Color animation specific:
	GLfloat animate_over_x_coef = 1000;
	GLfloat animate_over_x_range = .1f;
	vec4 animate_over_x_color = vec4(1.f, 0.f, 0.f, 1.f);
	GLfloat animate_over_y_coef = 1000;
	GLfloat animate_over_y_range = .1f;
	vec4 animate_over_y_color = vec4(1.f, 0.f, 0.f, 1.f);
	GLfloat animate_over_z_coef = 1000;
	GLfloat animate_over_z_range = .1f;
	vec4 animate_over_z_color = vec4(1.f, 0.f, 0.f, 1.f);
	///////////////////////////////
    
	ModelMaterial() = default;
    
	ModelMaterialStruct getModelMaterialStruct();
    
	void change_emissive(const vec4& color);
	void change_color_ambient(const vec4& color);
	void change_color_diffuse(const vec4& color);
	void change_color_specular(const vec4& color);
	void change_color(const vec4& color);
	void lerp_to_color(const vec4& color, GLfloat t);
	void lerp_to_color(const vec4& color);
	void change_shininess(const GLfloat alpha);
    
	void turn_red();
	void turn_green();
	void turn_blue();
	void increment_shininess(GLfloat inc);
    
	void increment_red(GLfloat inc);
	void increment_green(GLfloat inc);
	void increment_blue(GLfloat inc);
};