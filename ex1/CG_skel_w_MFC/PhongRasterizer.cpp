#include "stdafx.h"
#include "PhongRasterizer.h"
#include "Renderer.h"

using namespace Phong_Raster;

Rasterizer::Rasterizer(RasterizerData *rdata) : rdata(rdata), edge_queue(compare_triangles_min_y) {}

inline void init_and_clip_edge(RasterizerData *rdata, Edge *e, vec3 v0, vec3 v1, vec3 n0, vec3 n1, vec4 c0, vec4 c1) {
    init_and_clip_edge(rdata, e, v0, v1);
    e->min_color = c0;
    e->max_color = c1;
    
    e->min_n = n0;
    e->max_n = n1;
}

inline void Rasterizer::push_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 n0, vec3 n1, vec3 n2, vec4 c0, vec4 c1, vec4 c2, vec3 triangle_center) {
    Triangle_Edges te;
    te.center_of_triangle = triangle_center;
    
    GLfloat slope1 = (v1.x - v0.x)*(v2.y - v0.y);
    GLfloat slope2 = (v2.x - v0.x)*(v1.y - v0.y);
    if(slope1 < slope2) {
        init_and_clip_edge(rdata, &te.e0, v0, v1, n0, n1, c0, c1);
        init_and_clip_edge(rdata, &te.e1, v0, v2, n0, n2, c0, c2);
    } else {
        init_and_clip_edge(rdata, &te.e0, v0, v2, n0, n2, c0, c2);
        init_and_clip_edge(rdata, &te.e1, v0, v1, n0, n1, c0, c1);
    }
    init_and_clip_edge(rdata, &te.e2, v1, v2, n1, n2, c1, c2);
    
    edge_queue.push(te);
}

void Rasterizer::load_triangles(const vec3 *vertices, const vec3 *normals, const int *indices, size_t count) {
    mat4 world_pos_transform = rdata->world_transform * rdata->model_transform;
    mat4 viewing_transform = rdata->projection * rdata->camera_transform;
    
    for (int i = 0; i < count; ++i) {
        vec4 tv0 = world_pos_transform * vertices[i*3 + 0];
        vec4 tv1 = world_pos_transform * vertices[i*3 + 1];
        vec4 tv2 = world_pos_transform * vertices[i*3 + 2];
        
        vec3 triangle_center = center_of_triangle(tv0, tv1, tv2);
        
        tv0 = viewing_transform * tv0;
        tv1 = viewing_transform * tv1;
        tv2 = viewing_transform * tv2;
        
        if(!z_in_bounds(tv0) && !z_in_bounds(tv1) && !z_in_bounds(tv2)) {
            continue;
        }
        vec3 v0 = vec3FromAffine(tv0);
        vec3 v1 = vec3FromAffine(tv1);
        vec3 v2 = vec3FromAffine(tv2);
        
        // Backface culling {
        vec3 n = cross(v1-v0, v2-v0);
        if(n.z <= 0) continue;
        // }
        
        vec3 n0 = normalize(rdata->normal_transform * normals[i*3 + 0]);
        vec3 n1 = normalize(rdata->normal_transform * normals[i*3 + 1]);
        vec3 n2 = normalize(rdata->normal_transform * normals[i*3 + 2]);
        vec4 c0 = vec4(), c1 = vec4(), c2 = vec4();
        
        if(rdata->material->use_crazy_colors && indices) {
            c0 = rdata->crazy_color_table[(indices[i*3 + 0]) % rdata->crazy_color_count];
            c1 = rdata->crazy_color_table[(indices[i*3 + 1]) % rdata->crazy_color_count];
            c2 = rdata->crazy_color_table[(indices[i*3 + 2]) % rdata->crazy_color_count];
        }
        
        vec3 n_v0, n_v1, n_v2, n_n0, n_n1, n_n2;
        vec4 n_c0, n_c1, n_c2;
        
        // organize triangle vertices by y value:
        if(v0.y < v2.y) {
            if(v0.y < v1.y) {
                n_v0 = v0; n_n0 = n0; n_c0 = c0;
                
                if(v1.y < v2.y) {
                    n_v1 = v1; n_n1 = n1; n_c1 = c1;
                    n_v2 = v2; n_n2 = n2; n_c2 = c2;
                } else { // v2.y < v1.y
                    n_v1 = v2; n_n1 = n2; n_c1 = c2;
                    n_v2 = v1; n_n2 = n1; n_c2 = c1;
                }
            } else { // v1.y < v0.y
                n_v0 = v1; n_n0 = n1; n_c0 = c1;
                n_v1 = v0; n_n1 = n0; n_c1 = c0;
                n_v2 = v2; n_n2 = n2; n_c2 = c2;
            }
        } else { // v2.y < v0.y
            if(v1.y < v2.y) {
                n_v0 = v1; n_n0 = n1; n_c0 = c1;
                n_v1 = v2; n_n1 = n2; n_c1 = c2;
                n_v2 = v0; n_n2 = n0; n_c2 = c0;
            } else { // v2.y < v1.y
                n_v0 = v2; n_n0 = n2; n_c0 = c2;
                
                if(v0.y < v1.y) {
                    n_v1 = v0; n_n1 = n0; n_c1 = c0;
                    n_v2 = v1; n_n2 = n1; n_c2 = c1;
                } else { // v1.y < v0.y
                    n_v1 = v1; n_n1 = n1; n_c1 = c1;
                    n_v2 = v0; n_n2 = n0; n_c2 = c0;
                }
            }
        }
        
        push_triangle_edges(n_v0, n_v1, n_v2, n_n0, n_n1, n_n2, n_c0, n_c1, n_c2, triangle_center);
    }
}

void Rasterizer::draw_triangles() {
    std::vector<Triangle_Edges> active_edges;
    
    ModelMaterial material = *rdata->material;
    
    for(int y = 0; y < rdata->height; ++y) {
        while(!edge_queue.empty()) {
            auto it = &edge_queue.top();
            if(it->e0.min_y > y) break;
            active_edges.push_back(*it);
            edge_queue.pop();
        }
        
        auto it = active_edges.begin();
        while(it != active_edges.end()) {
            // NOTE(NJ): Remove edges from the active set
            if(y > it->e2.max_y) {
                it = active_edges.erase(it);
                continue;
            } else if(y >= it->e2.min_y) {
                NEXT_TRIANGLE_EDGE(it);
            }
            
            auto e0 = &(*it).e0;
            auto e1 = &(*it).e1;
            
            if(e0->dy == 0 || e1->dy == 0) {
                ++it;
                continue;
            }
            
            int x0 = clamp(e0->x, 0, rdata->width-1);
            GLfloat t0 = (GLfloat)(y - e0->min_y)/(GLfloat)(e0->max_y - e0->min_y);
            GLfloat z0 = LERP(e0->min_z, t0, e0->max_z);
            vec4 c0 = LERP(e0->min_color, t0, e0->max_color);
            vec3 n0 = LERP(e0->min_n, t0, e0->max_n);
            
            int x1 = clamp(e1->x, 0, rdata->width-1);
            GLfloat t1 = (GLfloat)(y - e1->min_y)/(GLfloat)(e1->max_y - e1->min_y);
            GLfloat z1 = LERP(e1->min_z, t1, e1->max_z);
            vec4 c1 = LERP(e1->min_color, t1, e1->max_color);
            vec3 n1 = LERP(e1->min_n, t1, e1->max_n);
            
            for(int x = x0; x < x1; ++x) {
                GLfloat t = (GLfloat)(x - e0->x)/(GLfloat)(e1->x - e0->x);
                GLfloat z = LERP(z0, t, z1);
                if(z < -1.f || z > 1.f) continue;
                if(z >= rdata->z_buffer[INDEX_VALUE(rdata->width, x, y)]) continue;
                
                vec3 n = LERP(n0, t, n1);
                if (material.use_crazy_colors) {
                    material.change_color(LERP(c0, t, c1));
                }
                vec4 c = calculate_lighting(rdata->lights, &material, &n, &it->center_of_triangle, &rdata->active_camera->eye); //TODO(OZ): currently uses center of polygon, replace with actual interpolation.
                
                vec4 color = rdata->fog_calculation(rdata, z, c);
                
                fill_pixel(rdata, x, y, z, color);
            }
            if(((-1.f <= z1) && (z1 <= 1.f)) &&
               (z1 < rdata->z_buffer[INDEX_VALUE(rdata->width, x1, y)])) {
                
                if (material.use_crazy_colors) {
                    material.change_color(c1);
                }
                vec4 c = calculate_lighting(rdata->lights, &material, &n1, &it->center_of_triangle, &rdata->active_camera->eye); //TODO(OZ): currently uses center of polygon, replace with actual interpolation.
                
                vec4 color = rdata->fog_calculation(rdata, z1, c);
                
                fill_pixel(rdata, x1, y, z1, color);
            }
            
            ADVANCE_EDGE(e0);
            ADVANCE_EDGE(e1);
            ++it;
        }
    }
}
