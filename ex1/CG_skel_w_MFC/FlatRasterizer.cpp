#include "stdafx.h"
#include "GouraudRasterizer.h"
#include "Renderer.h"
#include <cassert>

using namespace Flat_Raster;

Rasterizer::Rasterizer(RasterizerData *rdata) :
rdata(rdata), edge_queue(compare_triangles_min_y) {}


inline void Rasterizer::push_triangle_edges(vec3 v0, vec3 v1, vec3 v2, vec3 normal, vec4 color) {
    Triangle_Edges te;
    
    te.color = color;
    
    GLfloat slope1 = (v1.x - v0.x)*(v2.y - v0.y);
    GLfloat slope2 = (v2.x - v0.x)*(v1.y - v0.y);
    if(slope1 < slope2) {
        init_and_clip_edge(rdata, &te.e0, v0, v1);
        init_and_clip_edge(rdata, &te.e1, v0, v2);
    } else {
        init_and_clip_edge(rdata, &te.e0, v0, v2);
        init_and_clip_edge(rdata, &te.e1, v0, v1);
    }
    init_and_clip_edge(rdata, &te.e2, v1, v2);
    
    edge_queue.push(te);
}

void Rasterizer::load_triangles(const vec3 *vertices, const vec3 *face_normals, size_t count) {
    mat4 world_pos_transform = rdata->world_transform * rdata->model_transform;
    mat4 viewing_transform = rdata->projection * rdata->camera_transform;
    
    ModelMaterial material = *rdata->material;
    
    for (int i = 0; i < count; ++i) {
        vec4 tv0 = world_pos_transform * vertices[i*3 + 0];
        vec4 tv1 = world_pos_transform * vertices[i*3 + 1];
        vec4 tv2 = world_pos_transform * vertices[i*3 + 2];
        
        vec3 triangle_center = center_of_triangle(tv0, tv1, tv2); //Note(OZ): Could be replaced with center of model, much less expensive, but in the case the model is large... Less exact.
        
        tv0 = viewing_transform * tv0;
        tv1 = viewing_transform * tv1;
        tv2 = viewing_transform * tv2;
        
        if(!z_in_bounds(tv0) && !z_in_bounds(tv1) && !z_in_bounds(tv2)) {
            continue;
        }
        
        vec3 v0 = vec3FromAffine(tv0);
        vec3 v1 = vec3FromAffine(tv1);
        vec3 v2 = vec3FromAffine(tv2);
        
        // Backface culling {
        vec3 cross_normal = cross(v1-v0, v2-v0);
        if(cross_normal.z <= 0) continue;
        //}
        
        vec3 n_v0, n_v1, n_v2;
        
        // organize triangle vertices by y value:
        if(v0.y < v2.y) {
            if(v0.y < v1.y) {
                n_v0 = v0;
                
                if(v1.y < v2.y) {
                    n_v1 = v1;
                    n_v2 = v2;
                } else { // v2.y < v1.y
                    n_v1 = v2;
                    n_v2 = v1;
                }
            } else { // v1.y < v0.y
                n_v0 = v1;
                n_v1 = v0;
                n_v2 = v2;
            }
        } else { // v2.y < v0.y
            if(v1.y < v2.y) {
                n_v0 = v1;
                n_v1 = v2;
                n_v2 = v0;
            } else { // v2.y < v1.y
                n_v0 = v2;
                
                if(v0.y < v1.y) {
                    n_v1 = v0;
                    n_v2 = v1;
                } else { // v1.y < v0.y
                    n_v1 = v1;
                    n_v2 = v0;
                }
            }
        }
        
        vec3 normal = normalize(rdata->normal_transform * face_normals[i * 2 + 1]);
        
        if (rdata->material->use_crazy_colors) {
            vec4 crazy_color = rdata->crazy_color_table[i % rdata->crazy_color_count];
            material.lerp_to_color(crazy_color);
        }
        
        vec4 color = calculate_lighting(rdata->lights, &material, &normal, &triangle_center, &rdata->active_camera->eye);
        
        push_triangle_edges(n_v0, n_v1, n_v2, normal, color);
    }
}

void Rasterizer::draw_triangles() {
    std::vector<Triangle_Edges> active_edges;
    
    for(int y = 0; y < rdata->height; ++y) {
        while(!edge_queue.empty()) {
            auto it = &edge_queue.top();
            if(it->e0.min_y > y) break;
            active_edges.push_back(*it);
            edge_queue.pop();
        }
        
        auto it = active_edges.begin();
        while(it != active_edges.end()) {
            // NOTE(NJ): Remove edges from the active set
            if(y > it->e2.max_y) {
                it = active_edges.erase(it);
                continue;
            } else if(y >= it->e2.min_y) {
                NEXT_TRIANGLE_EDGE(it);
            }
            
            auto e0 = &it->e0;
            auto e1 = &it->e1;
            
            if(e0->dy == 0 || e1->dy == 0) {
                ++it;
                continue;
            }
            
            int x0 = clamp(e0->x, 0, rdata->width-1);
            GLfloat t0 = (GLfloat)(y - e0->min_y)/(GLfloat)(e0->max_y - e0->min_y);
            GLfloat z0 = LERP(e0->min_z, t0, e0->max_z);
            
            int x1 = clamp(e1->x, 0, rdata->width-1);
            GLfloat t1 = (GLfloat)(y - e1->min_y)/(GLfloat)(e1->max_y - e1->min_y);
            GLfloat z1 = LERP(e1->min_z, t1, e1->max_z);
            
            for(int x = x0; x < x1; ++x) {
                GLfloat t = (GLfloat)(x - e0->x)/(GLfloat)(e1->x - e0->x);
                GLfloat z = LERP(z0, t, z1);
                if((-1.f > z) || (z > 1.f)) continue;
                if(z >= rdata->z_buffer[INDEX_VALUE(rdata->width, x, y)]) continue;
                
                vec4 color = rdata->fog_calculation(rdata, z, it->color);
                
                fill_pixel(rdata, x, y, z, color);
            }
            if(((-1.f <= z1) && (z1 <= 1.f)) &&
               (z1 < rdata->z_buffer[INDEX_VALUE(rdata->width, x1, y)])) {
                fill_pixel(rdata, x1, y, z1, rdata->fog_calculation(rdata, z1, it->color));
            }
            
            ADVANCE_EDGE(e0);
            ADVANCE_EDGE(e1);
            ++it;
        }
    }
}
