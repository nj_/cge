#pragma once

const char default_shader_header[] = R"END(
#version 150

#define PI 3.14159265358979323846264338327

struct LightData {
    int type;
    
    float attentuation_constant;
    float attentuation_linear;
    float attentuation_quadratic;
    
    vec3 position;
    vec3 direction;
    vec4 color;

    int light_data_settings[5];
    // 0 - Light on/off
    // 1 - Ambient on/off
    // 2 - Diffusive on/off
    // 3 - Specular on/off
    // 4 - Distance attentuation on/off
};

uniform LightData lights[16];

struct ModelMaterialStruct
{
	vec4 base_emissive;
	vec4 base_ambient;
	vec4 base_diffuse;
	vec4 base_specular;
	float shininess_coefficient;
	float emissive_coefficient;
	float ambient_coefficient;
	float diffuse_coefficient;
	float specular_coefficient;

    float toon_constant;
    float silhouette_threshold;
    vec4 silhouette_color;
    
	int texture_mode;
	bool use_normal_map;
    bool use_environment_mapping;

    float reflectiveness;

    /////// Color animation specific:
 int vertex_animation_mode;
int color_animation_mode;
int uv_sampling_mode;

    float animate_over_x_coef;
    float animate_over_x_range;
    vec4 animate_over_x_color;
    float animate_over_y_coef;
    float animate_over_y_range;
    vec4 animate_over_y_color;
    float animate_over_z_coef;
    float animate_over_z_range;
    vec4 animate_over_z_color;
};

uniform ModelMaterialStruct material;

uniform float time;

vec3 animate_vertex1(float time, vec3 pos) {
    vec3 offset;
    float t1 = pow(abs(cos(time*4.)*.9), 12.);
    float t2 = pow(abs(cos(time*4. + 2.)*.9), 12.);
    float x_sin = sin(t1 + t2 + pos.x);
    float y_sin = sin(t1 + t2 + pos.y);
    float z_sin = sin(t1 + t2 + pos.z);
    offset[0] = pow(x_sin, 3.)*6.;
    offset[1] = pow(y_sin, 3.)*6.;
    offset[2] = pow(z_sin, 3.)*6.;
    
    return offset;
}

vec3 animate_vertex2(float time, vec3 pos) {
    vec3 offset;
    float t1 = time*4.;
    offset[0] =  sin(t1 + pos.x);
    offset[1] =  cos(t1 + pos.x);
    offset[2] = -sin(t1 + pos.x);
    
    return offset*8.f;
}

vec3 animate_vertex3(float time, vec3 pos) {
    vec3 offset;
    float t1 = time*2.;
    offset[0] =  sin(t1 + pos.x*7.);
    offset[1] =  cos(t1 + pos.x*5.);
    offset[2] = -sin(t1 + pos.y*4.);
    
    return offset;
}

vec3 animate_vertex(int animation_index, float time, vec3 pos) {
    vec3 p;
    if(animation_index == 1) {
        p = pos + animate_vertex1(time, pos)*.015f;
    } else if(animation_index == 2) {
        p = pos + animate_vertex2(time, pos)*.015f;
    } else if(animation_index == 3) {
        p = pos + animate_vertex3(time, pos)*.015f;
    } else if(animation_index == 4) {
        p = pos + animate_vertex3(time, pos)*.015f + animate_vertex1(time, pos)*.015f;
    } else {
        p = pos;
    }
    
    return p;
}

uniform float max_x;
uniform float max_y;
uniform float max_z;
uniform float min_x;
uniform float min_y;
uniform float min_z;

vec4 LERP(vec4 v1, float t, vec4 v2) {
    return t * v1 + (1-t) * v2;
}

float LERP(float f1, float t, float f2) {
    return (1-t) * f1 + t * f2;
}

vec4 animate_color_fragment(vec3 pos, vec4 color_res, int animation_mode) {
    vec4 temp_color = color_res;
    
    if (animation_mode == 1 || animation_mode == 4) {
        float t1 = time*0.5;
        float time_var =  sin(t1);
        float centric_pos_x = (max_x - min_x) * time_var;
        float dist_x = abs(centric_pos_x - pos.x);
        if (dist_x < material.animate_over_x_range) {
            float t = abs(dist_x - material.animate_over_x_range) / material.animate_over_x_range;
            temp_color = temp_color + LERP(material.animate_over_x_color, t, vec4(0,0,0,1));
        }
    }
    
    if (animation_mode == 2 || animation_mode == 4) {
        float t1 = time*0.5;
        float time_var =  sin(t1);
        float centric_pos_y = (max_y - min_y) * time_var;
        float dist_y = abs(centric_pos_y - pos.y);
        if (dist_y < material.animate_over_y_range) {
            float t = abs(dist_y - material.animate_over_y_range) / material.animate_over_y_range;
            temp_color = temp_color + LERP(material.animate_over_y_color, t, vec4(0,0,0,1));
        }
    }
    
    if (animation_mode == 3 || animation_mode == 4) {
        float t1 = time*0.5;
        float time_var =  sin(t1);
        float centric_pos_z = (max_z - min_z) * time_var;
        float dist_z = abs(centric_pos_z - pos.z);
        if (dist_z < material.animate_over_z_range) {
            float t = abs(dist_z - material.animate_over_z_range) / material.animate_over_z_range;
            temp_color = temp_color + LERP(material.animate_over_z_color, t, vec4(0,0,0,1));
        }
    }
    
    if (animation_mode == 5) {
        float t1 = time*0.5;
        float r = sin(pos.y + t1)*sin(t1);
        float g = sin(pos.z + t1+.2f)*sin(t1+.2f);
        float b = sin(pos.x + t1+.5f)*sin(t1+.5f);
        
temp_color = LERP(vec4(r,g,b,1.f), min(.75, sin(t1+pos.y)*sin(t1+pos.z)), temp_color);
    }
    
    return temp_color;
}

vec2 canonical_cylinder_uv(vec3 pos) {
    pos = normalize(pos);
    
    vec2 uv;
    uv.x = atan(pos.x, pos.z)/(2*PI);
    uv.y = pos.y;
    
    return uv;
}

vec2 canonical_sphere_uv(vec3 pos) {
    pos = normalize(pos);
    
    vec2 uv;
    uv.x = .5f + atan(pos.x, pos.z)/(2*PI);
    uv.y = .5f - asin(pos.y)/(PI);
    
    return uv;
}

vec2 get_uv(vec3 pos, vec2 coords) {
    vec2 uv;
    if (material.uv_sampling_mode == 1) {
        uv = canonical_cylinder_uv(pos);
    } else if (material.uv_sampling_mode == 2) {
        uv = canonical_sphere_uv(pos);
    } else {
        uv = coords;
    }
    return uv;
}

/////////////////////////////////////////////////
////////////// -- Perlin Noise -- ///////////////
/////////////////////////////////////////////////

vec2 randomGradient(float ix, float iy) {
    float random = 2920.f * sin(ix * 21942.f + iy * 171324.f + 8912.f) * cos(ix * 23157.f * iy * 217832.f + 9758.f);
    return vec2(cos(random), sin(random));
}

float dotGridGradient(int ix, int iy, float x, float y) {
    vec2 gradient = randomGradient(ix, iy);
    
    float dx = x - ix;
    float dy = y - iy;

    return (dx*gradient.x + dy*gradient.y);
}

float perlin(float x, float y) {
    int x0 = int(x);
    int x1 = x0 + 1;
    int y0 = int(y);
    int y1 = y0 + 1;

    float sx = x - float(x0);
    float sy = y - float(y0);

    float n0, n1, ix0, ix1, value;

    n0 = dotGridGradient(x0, y0, x, y);
    n1 = dotGridGradient(x1, y0, x, y);
    ix0 = LERP(n1, sx, n0);

    n0 = dotGridGradient(x0, y1, x, y);
    n1 = dotGridGradient(x1, y1, x, y);
    ix1 = LERP(n1, sx, n0);

    value = LERP(ix1, sy, ix0);
    return value;
}

float perlin_wrapper(float a, float b) {
    return sin(PI * perlin(a, b));
}

float perlin3D(float x, float y, float z) {
    y += 1;
    z += 2;
    float xy = perlin_wrapper(x, y);
    float xz = perlin_wrapper(x, z);
    float yz = perlin_wrapper(y, z);
    float yx = perlin_wrapper(y, x);
    float zx = perlin_wrapper(z, x);
    float zy = perlin_wrapper(z, y);
    return (xy * xz * yz * yx * zx * zy);
}

/////////////////////////////////////////////////
//////////// -- Perlin Noise END-- //////////////
/////////////////////////////////////////////////


float f_hash( float n ) {
    return fract(sin(n)*43758.5453123);   
}

float f_noise( in vec2 x ){
    vec2 p = floor(x);
    vec2 f = fract(x);
    f = f * f * (3.0 - 2.0 * f);
    float n = p.x + p.y * 57.0;
    return mix(mix( f_hash(n + 0.0), f_hash(n + 1.0), f.x), mix(f_hash(n + 57.0), f_hash(n + 58.0), f.x), f.y);
}

float f_fbm(vec2 p){
    mat2 m = mat2( 0.6, 0.6, -0.6, 0.8);
    float f = 0.0;
    f += 0.5000 * f_noise(p); p *= m * 2.02;
    f += 0.2500 * f_noise(p); p *= m * 2.03;
    f += 0.1250 * f_noise(p); p *= m * 2.01;
    f += 0.0625 * f_noise(p);
    f /= 0.9375;
    return f;
}


/////////////////////////////////////////////////
////////////- Turbulence textures -//////////////
/////////////////////////////////////////////////

vec4 gravel_color(float t) {
    vec4 color_1 = vec4(1.f, 1.f, 1.f, 1.f);
    vec4 color_2 = vec4(0.1f, 0.1f, 0.1f, 1.f);
    return LERP(color_1, t, color_2);
}

vec4 gravel(vec3 point) {
    float len_x = 100 * abs((point.x - min_x) / (max_x - min_x));
    float len_y = 100 * abs((point.y - min_y) / (max_y - min_y));
    float len_z = 100 * abs((point.z - min_z) / (max_z - min_z));
    vec2 pos = canonical_sphere_uv(point);
    float t = len_x / 100 + perlin3D(len_x, len_y, len_z);
    return gravel_color(sin(t));
}

vec4 wood_color(float t) {
    vec4 color_1 = vec4(53,29,18, 255) / 255;
    vec4 color_2 = vec4(129, 97, 74, 255) / 255;
    return LERP(color_1, t, color_2);
}

float smooth_func(float t) {
	return 6 * pow(t, 5) - 15 * pow(t, 4) + 10 * pow(t, 3);
}

vec4 wood(vec3 point) {
    float t = f_fbm(point.xy / 10);
    float t1 = smooth_func(pow(sin(t * PI * 100),50));
    float t2 = f_fbm(point.xy);
    return wood_color(t1 + t2);
}

/////////////////////////////////////////////////
///////////-Turbulence textures END-/////////////
/////////////////////////////////////////////////

vec4 get_tex_color(sampler2D texture_map, vec2 uv, vec3 modelspace_pos) {
    if (material.texture_mode == 1) {
        return texture2D(texture_map, uv);
    }
    if (material.texture_mode == 2) {
        return wood(modelspace_pos);
    }
    else {
        return gravel(modelspace_pos);
    }
}


)END";

static char* readShaderSource(const char* shaderFile, GLint *buf_size, 
                              const char prepend[]=default_shader_header,
                              GLint prepend_size=sizeof(default_shader_header));
GLuint InitShader(const char* vShaderFile, const char* fShaderFile);