#pragma once
#include "vec.h"
#include "veci.h"
#include "mat.h"
#include <string>
#include <vector>
#include "ModelMaterial.h"

namespace Model_Settings {
    enum ModelSettings {
        Draw_Wireframe = 1UL << 4,
        
        DrawVertexNormals = 1UL << 5,
        DrawFaceNormals   = 1UL << 6,
        
        DrawBoundingBox = 1UL << 7,
    };
}

struct Boundary {
    GLfloat min_x, max_x;
    GLfloat min_y, max_y;
    GLfloat min_z, max_z;
    vec3 center;
};

struct GlData {
    GLuint vertex_array;
    
    GLuint vertex_buffer;
    GLuint face_buffer;
    
    GLuint index_buffer;
    GLuint uv_buffer;
    
    GLuint texture_map;
    GLuint normal_map;
    
    GLuint bounding_box_buffer;
};


struct FaceData {
    vec3 center;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;
};

struct VertexData {
    vec3 position;
    vec3 normal;
    vec3 pad_1;
    vec3 pad_2;
};

class MeshModel {
    public:
    // ---- Data ----
    GLsizei triangle_count  = 0;
    
    GLint *indices = nullptr;
    VertexData *vertices = nullptr;
    FaceData *faces = nullptr;
    vec2 *uv_coords = nullptr;
    
    std::string texture_map_path;
    std::string normal_map_path;
    
    std::string name;
    
    GlData gl_data;
    
    static const int bounding_box_corner_count = 24;
    vec3 bounding_box[bounding_box_corner_count];
    Boundary boundary;
    
    //add more attributes
    mat4 world_transform  = mat4(1.0);
    mat3 model_transform  = mat3(1.0);
    mat3 normal_transform = mat3(1.0);
    
    // ---- Settings and attributes ----
    uint32_t settings = 0;
    bool enabled = true;
    void iterate_shading_method();
    
    ModelMaterial material = ModelMaterial();
    
    // ---- Methods ----
    MeshModel() = default;
    MeshModel(const MeshModel &model);
    
    ~MeshModel() {
        if (vertices) delete[] vertices;
        if (faces) delete[] faces;
    }
    
    MeshModel(const VertexData *vertices, const FaceData *faces, const GLint *indices,
              const GLsizei triangle_count, const Boundary& boundary);
    
    inline void AddVertex(vec3i v, int index, std::vector<vec3> *vertices,
                          std::vector<vec3> *normals, std::vector<vec2> *textures);
    void SetBoundingBox(const Boundary& boundary);
    
    void CalculateFaceNormals();
    
    void PrintModelInCFormat();
    
    MeshModel(std::string fileName);
    void loadFile(std::string fileName);
    
    bool is_in_clip_space(mat4 normalized_device_transform);
    
    void load_texture_map();
    void load_normal_map();
};
