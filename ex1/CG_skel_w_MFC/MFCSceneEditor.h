#pragma once

#include "afxpropertygridctrl.h"
#include "MFCCustomProperties.h"
#include "Scene.h"

// MFCSceneEditor dialog

class MFCSceneEditor : public CDialog
{
	DECLARE_DYNAMIC(MFCSceneEditor)
        
        public:
	MFCSceneEditor(CWnd* pParent, Scene *scene);
	MFCSceneEditor(Scene *scene);
	virtual ~MFCSceneEditor();
    
    // Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SCENE_EDITOR };
#endif
    
    protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    
	DECLARE_MESSAGE_MAP()
        public:
    Scene *scene;
    
    void InitSceneProperties();
    void InitModelProperties();
    void InitCameraProperties();
    void InitLightProperties();
    MFCCustomProperties scene_properties;
    MFCCustomProperties model_properties;
    MFCCustomProperties camera_properties;
    MFCCustomProperties light_properties;
    CButton refresh_button;
    afx_msg void OnBnClickedButtonRefresh();
};
