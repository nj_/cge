#pragma once
#include "resource.h"

void display(void);
void reshape(int width, int height);
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void mouse_wheel(int button, int state, int x, int y);

void ContextMenu(int id);
void ContextMenuAdd(int id);
void InitMenu();

namespace Context_Menu_Action {
    enum Context_Menu_Action {
        ToggleSSAA,
        SceneEditor,
        ResetActiveCamera,
        
        ResetStepSize,
        
        PrintActiveModel,
        About,
        
        OpenModelFile,
        
        // Add submenu
        AddTetrahedron,
        AddCube,
        AddSphere,
        AddCylinder,
        AddDonut,
        AddMonkey,
        AddBanana,
    };
}