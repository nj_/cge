#pragma once
#include "CG_skel_w_MFC.h"
#include "vec.h"
#include "mat.h"
#include "GL/glew.h"

#include "RasterizerData.h"
#include "ExperimentalRasterizer.h"
#include "FlatRasterizer.h"
#include "GouraudRasterizer.h"
#include "PhongRasterizer.h"

class Renderer {
    public:
    RasterizerData rdata;
    
    int ssaa_factor = 2;
    GLfloat *subsampling_buffer = nullptr;
    
    int width, height;
    void allocate_buffers(int width, int height);
    void reallocate_buffers();
    
    Experimental_Raster::Rasterizer raster_experimental;
    
    Flat_Raster::Rasterizer    raster_flat;
    Gouraud_Raster::Rasterizer raster_gouraud;
    Phong_Raster::Rasterizer   raster_phong;
    
    //////////////////////////////
    // openGL stuff. Don't touch.
    
    GLuint gScreenTex;
    GLuint gScreenVtc;
    void create_opengl_buffer();
    void init_opengl_rendering();
    //////////////////////////////
    
    Renderer();
    Renderer(int width, int height);
    ~Renderer(void);
    void Init();
    
    void draw_wireframe(const vec3 *vertices, const size_t count, const vec4 color);
    void draw_selected_wireframe(const vec3 *vertices, const size_t count, const vec4 color);
    
    void draw_vertex_normals(const vec3 *vertices, const vec3 *normals,
                             const size_t count, const vec4 color = vec4(.25f, .45f, .9f, 1.f));
    
    void draw_face_normals(const vec3 *normals, size_t count, vec4 color);
    void draw_bounding_box(const vec3 *bounding_box, vec4 color);
    
    void draw_line(vec4 v1, vec4 v2, const vec4 color);
    void plot_line(int x1, int y1, int x2, int y2, const vec4 color);
    
    void draw_axes(vec4 color_x = vec4(1.0f, 0.0f, 0.0f, 1.0f),
                   vec4 color_y = vec4(0.0f, 1.0f, 0.0f, 1.0f),
                   vec4 color_z = vec4(0.0f, 0.0f, 1.0f, 1.0f));
    
    
    void draw_camera_ui(Camera *camera);
    void draw_light_ui(Light *light);
    
    void draw_camera_axes(vec3 eye, vec3 x_axis, vec3 y_axis, vec3 z_axis,
                          vec4 color_x = vec4(1.0f, 0.0f, 0.0f, 1.0f),
                          vec4 color_y = vec4(0.0f, 1.0f, 0.0f, 1.0f),
                          vec4 color_z = vec4(0.0f, 0.0f, 1.0f, 1.0f));
    
    void set_camera_transform(const mat4& camera_transform);
    void set_projection(const mat4& projection);
    mat4 get_normalized_device_transform();
    
    void clear_buffers();
    void swap_buffers();
    void clear_color_buffer();
    void clear_depth_buffer();
    void plot_line_x(int x, int y, int x2, int y2, const vec4 color);
    void plot_line_y(int x, int y, int x2, int y2, const vec4 color);
    void fill_pixel(int x, int y, const vec4 color);
    void fill_pixel_blend(int x, int y, const vec4 color);
};
