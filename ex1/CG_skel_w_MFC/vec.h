//////////////////////////////////////////////////////////////////////////////
//
//  --- vec.h ---
//
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
//  vec2.h - 2D vector
//
#pragma once
#include <iostream>
#include <cmath>
#include "GL/glew.h"
#define D_PI 3.14159265358979323846264338327
#define F_PI ((GLfloat)D_PI)

//----------------------------------------------------------------------------
//
//  General math functions
//

#define SWAP(a, b) do {auto c = b; b = a; a = c;} while(0)
#define LERP(a, t, b) (((a)*(1.f - t)) + ((b)*(t)))

static const GLfloat default_epsilon = 0.0000000001f;
inline bool around_zero_epsilon(GLfloat n, GLfloat epsilon = default_epsilon) {
    return ((-epsilon < n) && (n < epsilon));
}

inline GLfloat minimum(GLfloat x, GLfloat y) {
    return (x < y) ? x : y;
}

inline GLfloat maximum(GLfloat x, GLfloat y) {
    return (x > y) ? x : y;
}

inline GLfloat clamp(GLfloat n, GLfloat min, GLfloat max) {
    const GLfloat t = (n < min) ? min : n;
    return (t > max) ? max : t;
}

inline GLfloat clamp(GLfloat n) {
    const GLfloat t = (n < 0.f) ? 0.f : n;
    return (t > 1.f) ? 1.f : t;
}

inline GLfloat clamp_min(GLfloat n, GLfloat min = 0.f) {
    return (n < min) ? min : n;
}

inline GLfloat clamp_max(GLfloat n, GLfloat max = 1.f) {
    return (n > max) ? max : n;
}

inline int clamp(int n, int min = 0, int max = 255) {
    const int t = (n < min) ? min : n;
    return (t > max) ? max : t;
}

inline int clamp_min(int n, int min = 0) {
    return (n < min) ? min : n;
}

inline int clamp_max(int n, int max = 255) {
    return (n > max) ? max : n;
}

//----------------------------------------------------------------------------

struct vec2 {
    GLfloat x;
    GLfloat y;
    
    //
    //  --- Constructors and Destructors ---
    //
    
    vec2(GLfloat s = GLfloat(0.0)) :
    x(s), y(s) {}
    
    vec2(GLfloat x, GLfloat y) :
    x(x), y(y) {}
    
    vec2(const vec2& v) {
        x = v.x;
        y = v.y;
    }
    
    //
    //  --- Indexing Operator ---
    //
    
    GLfloat& operator [] (int i) {
        return *(&x + i);
    }
    const GLfloat operator [] (int i) const {
        return *(&x + i);
    }
    
    //
    //  --- (non-modifying) Arithematic Operators ---
    //
    
    // unary minus operator
    vec2 operator - () const {
        return vec2(-x, -y);
    }
    
    vec2 operator + (const vec2& v) const {
        return vec2(x + v.x, y + v.y);
    }
    
    vec2 operator - (const vec2& v) const {
        return vec2(x - v.x, y - v.y);
    }
    
    vec2 operator * (const GLfloat s) const {
        return vec2(s*x, s*y);
    }
    
    vec2 operator * (const vec2& v) const {
        return vec2(x*v.x, y*v.y);
    }
    
    friend vec2 operator * (const GLfloat s, const vec2& v) {
        return v * s;
    }
    
    vec2 operator / (const GLfloat s) const {
        GLfloat r = GLfloat(1.0) / s;
        
        return *this * r;
    }
    
    //
    //  --- (modifying) Arithematic Operators ---
    //
    
    vec2& operator += (const vec2& v) {
        x += v.x;
        y += v.y;
        
        return *this;
    }
    
    vec2& operator -= (const vec2& v) {
        x -= v.x;
        y -= v.y;
        
        return *this;
    }
    
    vec2& operator *= (const GLfloat s) {
        x *= s;
        y *= s;
        
        return *this;
    }
    
    vec2& operator *= (const vec2& v) {
        x *= v.x;
        y *= v.y;
        
        return *this;
    }
    
    vec2& operator /= (const GLfloat s) {
        GLfloat r = GLfloat(1.0) / s;
        *this *= r;
        
        return *this;
    }
    
    //
    //  --- Insertion and Extraction Operators ---
    //
    
    friend std::ostream& operator << (std::ostream& os, const vec2& v) {
        return os << "(" << v.x << ", " << v.y <<  ")";
    }
    
    friend std::istream& operator >> (std::istream& is, vec2& v) {
        return is >> v.x >> v.y ;
    }
    
    //
    //  --- Conversion Operators ---
    //
    
    operator const GLfloat* () const {
        return static_cast<const GLfloat*>(&x);
    }
    
    operator GLfloat* () {
        return static_cast<GLfloat*>(&x);
    }
};

//----------------------------------------------------------------------------
//
//  Non-class vec2 Methods
//

inline GLfloat dot(const vec2& u, const vec2& v) {
    return u.x*v.x + u.y*v.y;
}

inline GLfloat length(const vec2& v) {
    return std::sqrt((v.x*v.x) + (v.y*v.y));
}

inline vec2 normalize(const vec2& v) {
    return v / length(v);
}

//////////////////////////////////////////////////////////////////////////////
//
//  vec3.h - 3D vector
//
//////////////////////////////////////////////////////////////////////////////

struct vec3 {
    GLfloat x;
    GLfloat y;
    GLfloat z;
    
    //
    //  --- Constructors and Destructors ---
    //
    
    vec3(GLfloat s = GLfloat(0.0)) :
    x(s), y(s), z(s) {}
    
    vec3(GLfloat x, GLfloat y, GLfloat z) :
    x(x), y(y), z(z) {}
    
    vec3(const vec3& v) {
        x = v.x;
        y = v.y;
        z = v.z;
    }
    
    vec3(const vec2& v, const float f) : z(f) {
        x = v.x;
        y = v.y;
    }
    
    //
    //  --- Indexing Operator ---
    //
    
    
    GLfloat& operator [] (int i) {
        return *(&x + i);
    }
    const GLfloat operator [] (int i) const {
        return *(&x + i);
    }
    
    //
    //  --- (non-modifying) Arithematic Operators ---
    //
    
    // unary minus operator
    vec3 operator - () const {
        return vec3(-x, -y, -z);
    }
    
    vec3 operator + (const vec3& v) const {
        return vec3(x + v.x, y + v.y, z + v.z);
    }
    
    vec3 operator - (const vec3& v) const {
        return vec3(x - v.x, y - v.y, z - v.z);
    }
    
    vec3 operator * (const GLfloat s) const {
        return vec3(s*x, s*y, s*z);
    }
    
    vec3 operator * (const vec3& v) const {
        return vec3(x*v.x, y*v.y, z*v.z);
    }
    
    friend vec3 operator * (const GLfloat s, const vec3& v) {
        return v * s;
    }
    
    vec3 operator / (const GLfloat s) const {
        GLfloat r = GLfloat(1.0) / s;
        return *this * r;
    }
    
    //
    //  --- (modifying) Arithematic Operators ---
    //
    
    vec3& operator += (const vec3& v) {
        x += v.x;
        y += v.y;
        z += v.z;
        
        return *this;
    }
    
    vec3& operator -= (const vec3& v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
        
        return *this;
    }
    
    vec3& operator *= (const GLfloat s) {
        x *= s;
        y *= s;
        z *= s;
        
        return *this;
    }
    
    vec3& operator *= (const vec3& v) {
        x *= v.x;
        y *= v.y;
        z *= v.z;
        
        return *this;
    }
    
    vec3& operator /= (const GLfloat s) {
        GLfloat r = GLfloat(1.0) / s;
        *this *= r;
        
        return *this;
    }
    
    //
    //  --- Insertion and Extraction Operators ---
    //
    
    friend std::ostream& operator << (std::ostream& os, const vec3& v) {
        return os << "(" << v.x << ", " << v.y << ", " << v.z <<  ")";
    }
    
    friend std::istream& operator >> (std::istream& is, vec3& v) {
        return is >> v.x >> v.y >> v.z;
    }
    
    //
    //  --- Conversion Operators ---
    //
    
    operator const GLfloat* () const {
        return static_cast<const GLfloat*>(&x);
    }
    
    operator GLfloat* () {
        return static_cast<GLfloat*>(&x);
    }
};

//----------------------------------------------------------------------------
//
//  Non-class vec3 Methods
//

inline GLfloat dot(const vec3& u, const vec3& v) {
    return u.x*v.x + u.y*v.y + u.z*v.z ;
}

inline GLfloat length(const vec3& v) {
    return std::sqrt(dot(v,v));
}

inline vec3 normalize(const vec3& v) {
    return v / length(v);
}

inline vec3 cross(const vec3& a, const vec3& b) {
    return vec3(a.y*b.z - a.z*b.y,
                a.z*b.x - a.x*b.z,
                a.x*b.y - a.y*b.x);
}

inline GLfloat distance(const vec3& a, const vec3& b) {
    return length(a - b);
}

inline vec3 center_of_triangle(vec3& v0, vec3& v1, vec3& v2) {
    return vec3(v0.x + v1.x + v2.x,
                v0.y + v1.y + v2.y,
                v0.z + v1.z + v2.z) / 3.f;
}

//////////////////////////////////////////////////////////////////////////////
//
//  vec4 - 4D vector
//
//////////////////////////////////////////////////////////////////////////////

struct vec4 {
    union {
        __m128 value;
        struct {
            GLfloat x;
            GLfloat y;
            GLfloat z;
            GLfloat w;
        };
        struct {
            GLfloat r;
            GLfloat g;
            GLfloat b;
            GLfloat a;
        };
    };
    
    //
    //  --- Constructors and Destructors ---
    //
    
    vec4(GLfloat s = GLfloat(0.0)) {
        value = _mm_set_ps1(s);
    }
    
    vec4(GLfloat x, GLfloat y, GLfloat z, GLfloat w) :
    x(x), y(y), z(z), w(w) {}
    
    vec4(__m128 wide_value) {
        value = wide_value;
    }
    
    vec4(const vec4& v) {
        value = v.value;
    }
    
    vec4(const vec3& v, const float w = 1.0) : w(w) {
        x = v.x;
        y = v.y;
        z = v.z;
    }
    
    vec4(const vec2& v, const float z, const float w) : z(z), w(w) {
        x = v.x;
        y = v.y;
    }
    
    //
    //  --- Indexing Operator ---
    //
    
    GLfloat& operator [] (int i) { return *(&x + i); }
    const GLfloat operator [] (int i) const { return *(&x + i); }
    
    //
    //  --- (non-modifying) Arithematic Operators ---
    //
    
    // unary minus operator
    vec4 operator - () const {
        __m128 scalar = _mm_set_ps1(0);
        return vec4(_mm_sub_ps(scalar, value));
    }
    
    vec4 operator + (const vec4& v) const {
        return vec4(_mm_add_ps(value, v.value));
    }
    
    vec4 operator - (const vec4& v) const {
        return vec4(_mm_sub_ps(value, v.value));
    }
    
    vec4 operator * (const GLfloat s) const {
        __m128 scalar = _mm_set_ps1(s);
        return vec4(_mm_mul_ps(value, scalar));
    }
    
    vec4 operator * (const vec4& v) const {
        return vec4(_mm_mul_ps(value, v.value));
    }
    
    friend vec4 operator * (const GLfloat s, const vec4& v) {
        __m128 scalar = _mm_set_ps1(s);
        return vec4(_mm_mul_ps(v.value, scalar));
    }
    
    vec4 operator / (const GLfloat s) const {
        __m128 scalar = _mm_set_ps1(s);
        return vec4(_mm_div_ps(value, scalar));
    }
    
    //
    //  --- (modifying) Arithematic Operators ---
    //
    
    vec4& operator += (const vec4& v) {
        value = _mm_add_ps(value, v.value);
        return *this;
    }
    
    vec4& operator -= (const vec4& v) {
        value = _mm_sub_ps(value, v.value);
        return *this;
    }
    
    vec4& operator *= (const GLfloat s) {
        __m128 scalar = _mm_set_ps1(s);
        value = _mm_mul_ps(value, scalar);
        return *this;
    }
    
    vec4& operator *= (const vec4& v) {
        value = _mm_mul_ps(value, v.value);
        return *this;
    }
    
    vec4& operator /= (const GLfloat s) {
        __m128 scalar = _mm_set_ps1(s);
        value = _mm_div_ps(value, scalar);
        return *this;
    }
    
    //
    //  --- Insertion and Extraction Operators ---
    //
    
    friend std::ostream& operator << (std::ostream& os, const vec4& v) {
        return os << "(" << v.x << ", " << v.y
            << ", " << v.z << ", " << v.w << ")";
    }
    
    friend std::istream& operator >> (std::istream& is, vec4& v) {
        return is >> v.x >> v.y >> v.z >> v.w;
    }
    
    //
    //  --- Conversion Operators ---
    //
    
    operator const GLfloat* () const {
        return static_cast<const GLfloat*>(&x);
    }
    
    operator GLfloat* () {
        return static_cast<GLfloat*>(&x);
    }
};

//----------------------------------------------------------------------------
//
//  Non-class vec4 Methods
//

inline GLfloat square_root(GLfloat scalar) {
    __m128 in = _mm_set_ss(scalar);
    __m128 out = _mm_rsqrt_ss(in);
    GLfloat result = _mm_cvtss_f32(out);
    return result;
}

inline GLfloat dot(const vec4& u, const vec4& v) {
    GLfloat result;
    
    __m128 dot_product = _mm_dp_ps(u.value, v.value, 0xFF);
    _mm_store_ss(&result, dot_product);
    
    return result;
}

inline GLfloat length_squared(const vec4& v) {
    return dot(v,v);
}

inline GLfloat length(const vec4& v) {
    return square_root(length_squared(v));
}

inline vec4 normalize(const vec4& v) {
    return v / length(v);
}

inline vec3 vec3FromAffine(const vec4& v) {
    return vec3(v.x/v.w, v.y/v.w, v.z/v.w);
}

inline vec3 cross(const vec4& a, const vec4& b) {
    return vec3(a.y * b.z - a.z * b.y,
                a.z * b.x - a.x * b.z,
                a.x * b.y - a.y * b.x);
}

inline GLfloat distance_squared(const vec4& a, const vec4& b) {
    return length_squared(a - b);
}

inline GLfloat distance(const vec4& a, const vec4& b) {
    return length(a - b);
}

inline bool z_in_bounds(vec4 v) {
    return ((-v.w <= v.z) && (v.z <= v.w));
}

inline vec3 center_of_triangle(vec4& v0, vec4& v1, vec4& v2) {
    vec3 a_v0 = vec3FromAffine(v0);
    vec3 a_v1 = vec3FromAffine(v1);
    vec3 a_v2 = vec3FromAffine(v2);
    
    return vec3(a_v0.x + a_v1.x + a_v2.x,
                a_v0.y + a_v1.y + a_v2.y,
                a_v0.z + a_v1.z + a_v2.z) / 3.f;
}

//----------------------------------------------------------------------------

#include <vector>

/// 
/// Shuffle class
/// 

class Shuffle {
    public:
    size_t size, position;
    std::vector<int> shuffle_array;
    
    Shuffle(size_t size) : size(size), position(size-1) {
        for (int i = 0; i < size; i++) {
            shuffle_array.push_back(i);
        }
    }
    
    int get_num() {
        if (position == 1) {
            position = size;
            return shuffle_array[0];
        }
        int random_cell = rand() % position;
        position--;
        int return_val = shuffle_array[random_cell];
        shuffle_array[random_cell] = shuffle_array[position];
        shuffle_array[position] = return_val;
        return return_val;
    }
};


/// 
/// Generic shuffle container class
/// 

class Shuffle_Container {
    public:
    vec4 *container;
    size_t size;
    
    Shuffle_Container(vec4 *in_container, size_t size) : size(size) {
        container = (vec4 *)malloc(size * sizeof(vec4) * 2);
        memcpy(container, in_container, size*sizeof(vec4));
    }
    
    ~Shuffle_Container() {
        free(container);
    }
    
    vec4 operator[](size_t n) {
        return container[n];
    }
    
    void shuffle() {
        Shuffle shuffler(size);
        for (int i = 0; i < size; i++) {
            container[i + size] = container[shuffler.get_num()];
        }
        for (int i = 0; i < size; i++) {
            container[i] = container[i + size];
        }
    }
};

/// 
/// Generic interpolating color by step class
/// 


class Interpolating_Colors {
    public:
    vec4 *container;
    size_t size;
    GLfloat t = 0.f;
    GLfloat interval = 3.f;
    
    Interpolating_Colors(vec4* in_container, size_t size) : size(size) {
        container = (vec4 *)malloc(size * sizeof(vec4));
        memcpy(container, in_container, size*sizeof(vec4));
    }
    
    ~Interpolating_Colors() {
        free(container);
    }
    
    vec4 operator[](int n) {
        return LERP(container[n % size], t, container[(n + 1) % size]);
    }
    
    void move_container_by_one_step() {
        auto first_val = container[0];
        for (int i = 0; i < size - 1; i++)
            container[i] = container[i + 1];
        container[size - 1] = first_val;
    }
    
    void increment(GLfloat step) {
        t += step/interval;
        if (t >= 1) {
            t = 0.f;
            move_container_by_one_step();
        }
    }
};


#define VEC4_TO_RGB(v) RGB((short)(255.f*v.r), (short)(255.f*v.g), (short)(255.f*v.b))
#define RGB_TO_VEC4(rgb) vec4((float)GetRValue(rgb)/255.f, (float)GetGValue(rgb)/255.f, (float)GetBValue(rgb)/255.f, 1.f)
