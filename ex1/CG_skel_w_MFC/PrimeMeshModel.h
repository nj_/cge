#pragma once
#include "MeshModel.h"

namespace Primitives {
#if 0
#include "primitives/Tetrahedron.h"
#include "primitives/Cube.h"
#include "primitives/Sphere.h"
#include "primitives/Cylinder.h"
#include "primitives/Donut.h"
#include "primitives/Monkey.h"
#include "primitives/Banana.h"
    static const MeshModel Tetrahedron = MeshModel((vec3*)tetrahedron::vertices, (vec3*)tetrahedron::normals, (vec3*)tetrahedron::textures, tetrahedron::triangle_count, (vec3*)tetrahedron::bounding_box);
    static const MeshModel Cube        = MeshModel((vec3*)cube::       vertices, (vec3*)cube::       normals, (vec3*)cube::       textures, cube::       triangle_count, (vec3*)cube::       bounding_box);
    static const MeshModel Sphere      = MeshModel((vec3*)sphere::     vertices, (vec3*)sphere::     normals, (vec3*)sphere::     textures, sphere::     triangle_count, (vec3*)sphere::     bounding_box);
    static const MeshModel Cylinder    = MeshModel((vec3*)cylinder::   vertices, (vec3*)cylinder::   normals, (vec3*)cylinder::   textures, cylinder::   triangle_count, (vec3*)cylinder::   bounding_box);
    static const MeshModel Donut       = MeshModel((vec3*)donut::      vertices, (vec3*)donut::      normals, (vec3*)donut::      textures, donut::      triangle_count, (vec3*)donut::      bounding_box);
    static const MeshModel Monkey      = MeshModel((vec3*)monkey::     vertices, (vec3*)monkey::     normals, (vec3*)monkey::     textures, monkey::     triangle_count, (vec3*)monkey::     bounding_box);
    static const MeshModel Banana      = MeshModel((vec3*)banana::     vertices, (vec3*)banana::     normals, nullptr                     , banana::     triangle_count, (vec3*)banana::     bounding_box);
#else 
    static const MeshModel Tetrahedron = MeshModel("primitives/tetrahedron.obj");
    static const MeshModel Cube        = MeshModel("primitives/cube.obj");
    static const MeshModel Sphere      = MeshModel("primitives/sphere.obj");
    static const MeshModel Cylinder    = MeshModel("primitives/cylinder.obj");
    static const MeshModel Donut       = MeshModel("primitives/donut.obj");
    static const MeshModel Monkey      = MeshModel("primitives/monkey.obj");
    static const MeshModel Banana      = MeshModel("primitives/banana.obj");
#endif
};