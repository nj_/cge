// MFCSceneEditor.cpp : implementation file
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"
#include "MFCSceneEditor.h"

// MFCSceneEditor dialog

IMPLEMENT_DYNAMIC(MFCSceneEditor, CDialog)

MFCSceneEditor::MFCSceneEditor(Scene *scene) : MFCSceneEditor(nullptr, scene) {};

MFCSceneEditor::MFCSceneEditor(CWnd* pParent, Scene *scene)
: CDialog(IDD_SCENE_EDITOR, pParent), scene(scene), scene_properties(scene), model_properties(scene), camera_properties(scene), light_properties(scene)
{
    InitSceneProperties();
    InitModelProperties();
    InitCameraProperties();
    InitLightProperties();
}

MFCSceneEditor::~MFCSceneEditor()
{
}

void MFCSceneEditor::InitSceneProperties() {
    {
        COleVariant factor((long)scene->renderer->ssaa_factor);
        auto property = new MFCCustomProperty(_T("SSAA factor"), factor, NULL, new MFCPropertyData(MFCProperty_Type::Int, &scene->renderer->ssaa_factor));
        property->EnableSpinControl(TRUE, 1, 4);
        scene_properties.AddProperty(property);
    }
    {
        auto property = NEW_UI_COLOR_PROPERTY("Background", scene->renderer->rdata.background_color);
        property->EnableOtherButton(_T("Other..."));
        scene_properties.AddProperty(property);
    }
    
    {    
        auto property = new CMFCPropertyGridFileProperty(_T("Skybox Texture"), TRUE, scene->skybox_path.c_str(), _T("png"), 0, _T("*.*|*.*||"));
        property->SetData((DWORD_PTR)new MFCPropertyData(MFCProperty_Type::Path, scene, -1));
        scene_properties.AddProperty(property);
        
        COleVariant use_skybox((short)(scene->use_skybox), VT_BOOL);
        scene_properties.AddProperty(new MFCCustomProperty(_T("Draw Skybox"), use_skybox, NULL, new MFCPropertyData(MFCProperty_Type::Bool, &scene->use_skybox)));
    }
    
    {
        COleVariant enabled((short)((scene->settings & Scene_Settings::DrawUiElements) != 0), VT_BOOL);
        scene_properties.AddProperty(new MFCCustomProperty(_T("Draw HUD"), enabled, NULL, new MFCPropertyData(MFCProperty_Type::SceneSettings, &scene->settings, Scene_Settings::DrawUiElements)));
    }
    
    {
        // COleVariant enabled((short)((scene->settings & Scene_Settings::AnimateCrazyColors) != 0), VT_BOOL);
        // scene_properties.AddProperty(new MFCCustomProperty(_T("Animate Crazyness"), enabled, NULL, new MFCPropertyData(MFCProperty_Type::SceneSettings, &scene->settings, Scene_Settings::AnimateCrazyColors | Scene_Settings::AlwaysDraw)));
    }
    
    {
        // COleVariant enabled((short)((scene->settings & Scene_Settings::ChristmasLights) != 0), VT_BOOL);
        // scene_properties.AddProperty(new MFCCustomProperty(_T("Merry Christmas!!"), enabled, NULL, new MFCPropertyData(MFCProperty_Type::SceneSettings, &scene->settings, Scene_Settings::ChristmasLights | Scene_Settings::AlwaysDraw)));
    }
    
    {
        auto group = new MFCCustomProperty(_T("Camera Step Size"), 0, TRUE);
        group->AddSubItem(new MFCCustomProperty(_T("Translation"), scene->camera_delta.translation, NULL, new MFCPropertyData(MFCProperty_Type::Float, &scene->camera_delta.translation)));
        group->AddSubItem(new MFCCustomProperty(_T("Rotation"),    scene->camera_delta.rotation,    NULL, new MFCPropertyData(MFCProperty_Type::Angle, &scene->camera_delta.rotation)));
        group->AddSubItem(new MFCCustomProperty(_T("Scaling"),     scene->camera_delta.scaling,     NULL, new MFCPropertyData(MFCProperty_Type::Float, &scene->camera_delta.scaling)));
        group->AdjustButtonRect();
        group->AllowEdit();
        group->Enable();
        group->Show();
        group->Redraw();
        scene_properties.AddProperty(group);
    }
    {
        auto group = new MFCCustomProperty(_T("Model Step Size"), 0, TRUE);
        group->AddSubItem(new MFCCustomProperty(_T("Translation"), scene->model_delta.translation, NULL, new MFCPropertyData(MFCProperty_Type::Float, &scene->model_delta.translation)));
        group->AddSubItem(new MFCCustomProperty(_T("Rotation"),    scene->model_delta.rotation,    NULL, new MFCPropertyData(MFCProperty_Type::Angle, &scene->model_delta.rotation)));
        group->AddSubItem(new MFCCustomProperty(_T("Scaling"),     scene->model_delta.scaling,     NULL, new MFCPropertyData(MFCProperty_Type::Float, &scene->model_delta.scaling)));
        group->AdjustButtonRect();
        group->AllowEdit();
        group->Enable();
        group->Show();
        group->Redraw();
        scene_properties.AddProperty(group);
    }
}

void MFCSceneEditor::InitModelProperties() {
    int i = 0;
    char name[256];
    for(auto model : scene->models) {
        if(scene->active_model == model) {
            sprintf_s(name, "Model %d (active)", i+1);
        } else {
            sprintf_s(name, "Model %d", i+1);
        }
        
        auto model_group = new MFCCustomProperty(_T(name));
        {
            COleVariant enabled((short)(model->enabled), VT_BOOL);
            model_group->AddSubItem(new MFCCustomProperty(_T("Render"), enabled, NULL, new MFCPropertyData(MFCProperty_Type::Bool, &model->enabled)));
        }
        {
            auto group = new MFCCustomProperty(_T("Material"));
            auto material = &model->material;
            {
                char *shading_mode = "invalid";
                switch(model->material.shading_mode) {
                    case Shading_Mode::Flat:    { shading_mode = "Flat"; }    break;
                    case Shading_Mode::Gouraud: { shading_mode = "Gouraud"; } break;
                    case Shading_Mode::Phong:   { shading_mode = "Phong"; }   break;
                    case Shading_Mode::Toon:    { shading_mode = "Toon"; }    break;
                }
                auto property = new MFCCustomProperty(_T("Shading Mode"), shading_mode, NULL, new MFCPropertyData(MFCProperty_Type::ShadingMode, &model->material));
                property->AddOption("Flat");
                property->AddOption("Gouraud");
                property->AddOption("Phong");
                property->AddOption("Toon");
                group->AddSubItem(property);
            }
            {
                auto toon_group = new MFCCustomProperty(_T("Toon Settings"));
                auto property = new MFCCustomProperty(_T("Color Count"), model->material.toon_constant, NULL, new MFCPropertyData(MFCProperty_Type::Float, &model->material.toon_constant));
                toon_group->AddSubItem(property);
                property = new MFCCustomProperty(_T("Silhouette Width"), model->material.silhouette_threshold, NULL, new MFCPropertyData(MFCProperty_Type::Float, &model->material.silhouette_threshold));
                toon_group->AddSubItem(property);
                property = new MFCCustomProperty(_T("Silhouette Color"), model->material.silhouette_threshold, NULL, new MFCPropertyData(MFCProperty_Type::Color, &model->material.silhouette_color));
                {
                    auto property = new CMFCPropertyGridColorProperty(_T("Silhouette Color"), VEC4_TO_RGB(material->silhouette_color), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::Color, &material->silhouette_color));
                    property->EnableOtherButton(_T("Other..."));
                    toon_group->AddSubItem(property);
                }
                
                group->AddSubItem(toon_group);
            }
            
            {
                COleVariant factor((long)material->vertex_animation_mode);
                auto property = new MFCCustomProperty(_T("Vertex Animation"), factor, NULL, new MFCPropertyData(MFCProperty_Type::Int, &material->vertex_animation_mode));
                property->EnableSpinControl(TRUE, 0, 20);
                group->AddSubItem(property);
            }
            
            {
                COleVariant factor((long)material->color_animation_mode);
                auto property = new MFCCustomProperty(_T("Color Animation Mode"), factor, NULL, new MFCPropertyData(MFCProperty_Type::Int, &material->color_animation_mode));
                property->EnableSpinControl(TRUE, 0, 20);
                group->AddSubItem(property);
            }
            
            {
                COleVariant use_environment((short)(material->use_environment_mapping), VT_BOOL);
                group->AddSubItem(new MFCCustomProperty(_T("Use Environment Mapping"), use_environment, NULL, new MFCPropertyData(MFCProperty_Type::Bool, &material->use_environment_mapping)));
                group->AddSubItem(new MFCCustomProperty(_T("Reflectiveness"), material->reflectiveness, NULL, new MFCPropertyData(MFCProperty_Type::Float, &material->reflectiveness)));
                
                {                
                    COleVariant factor((long)material->uv_sampling_mode);
                    auto property = new MFCCustomProperty(_T("UV Sampling Mode"), factor, NULL, new MFCPropertyData(MFCProperty_Type::Int, &material->uv_sampling_mode));
                    property->EnableSpinControl(TRUE, 0, 20);
                    group->AddSubItem(property);
                }
                
                auto property = new CMFCPropertyGridFileProperty(_T("Texture File"), TRUE, model->texture_map_path.c_str(), _T("png"), 0, _T("*.*|*.*||"));
                property->SetData((DWORD_PTR)new MFCPropertyData(MFCProperty_Type::Path, model, 0));
                group->AddSubItem(property);
                
                {
                    COleVariant factor((long)material->texture_mode);
                    auto property = new MFCCustomProperty(_T("Texture Mode"), factor, NULL, new MFCPropertyData(MFCProperty_Type::Int, &material->texture_mode));
                    property->EnableSpinControl(TRUE, 0, 20);
                    group->AddSubItem(property);
                }
                
                property = new CMFCPropertyGridFileProperty(_T("Normal File"), TRUE, model->normal_map_path.c_str(), _T("png"), 0, _T("*.*|*.*||"));
                property->SetData((DWORD_PTR)new MFCPropertyData(MFCProperty_Type::Path, model, 1));
                COleVariant use_normal((short)(material->use_normal_map), VT_BOOL);
                group->AddSubItem(new MFCCustomProperty(_T("Use Normal"), use_normal, NULL, new MFCPropertyData(MFCProperty_Type::Bool, &material->use_normal_map)));
                group->AddSubItem(property);
            }
            
            group->AddSubItem(new MFCCustomProperty(_T("Shine coeff."), material->shininess_coefficient,
                                                    NULL, new MFCPropertyData(MFCProperty_Type::Float, &material->shininess_coefficient )));
            {
                auto color_group = new MFCCustomProperty(_T("Colors"));
                
                auto property = new CMFCPropertyGridColorProperty(_T("All Colors"), RGB(255, 255, 255), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::MaterialColor, material, 0));
                property->EnableOtherButton(_T("Other..."));
                color_group->AddSubItem(property);
                
                property = new CMFCPropertyGridColorProperty(_T("Emissive"), VEC4_TO_RGB(material->base_emissive), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::MaterialColor, material, 1));
                property->EnableOtherButton(_T("Other..."));
                color_group->AddSubItem(property);
                color_group->AddSubItem(new MFCCustomProperty(_T("Emissive coeff."), material->emissive_coefficient,
                                                              NULL, new MFCPropertyData(MFCProperty_Type::Float, &material->emissive_coefficient  )));
                
                property = new CMFCPropertyGridColorProperty(_T("Ambient"), VEC4_TO_RGB(material->base_ambient), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::MaterialColor, material, 2));
                property->EnableOtherButton(_T("Other..."));
                color_group->AddSubItem(property);
                color_group->AddSubItem(new MFCCustomProperty(_T("Ambient coeff."), material->ambient_coefficient,
                                                              NULL, new MFCPropertyData(MFCProperty_Type::Float, &material->ambient_coefficient  )));
                
                property = new CMFCPropertyGridColorProperty(_T("Diffuse"), VEC4_TO_RGB(material->base_diffuse), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::MaterialColor, material, 3));
                property->EnableOtherButton(_T("Other..."));
                color_group->AddSubItem(property);
                color_group->AddSubItem(new MFCCustomProperty(_T("Diffuse coeff."), material->diffuse_coefficient,
                                                              NULL, new MFCPropertyData(MFCProperty_Type::Float, &material->diffuse_coefficient  )));
                
                property = new CMFCPropertyGridColorProperty(_T("Specular"), VEC4_TO_RGB(material->base_specular), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::MaterialColor, material, 4));
                property->EnableOtherButton(_T("Other..."));
                color_group->AddSubItem(property);
                color_group->AddSubItem(new MFCCustomProperty(_T("Specular coeff."), material->specular_coefficient,
                                                              NULL, new MFCPropertyData(MFCProperty_Type::Float, &material->specular_coefficient  )));
                
                group->AddSubItem(color_group);
            }
            
            model_group->AddSubItem(group);
        }
        
        model_group->AdjustButtonRect();
        model_group->AllowEdit();
        model_group->Enable();
        model_group->Show();
        model_group->Redraw();
        model_properties.AddProperty(model_group);
        ++i;
    }
}

void MFCSceneEditor::InitCameraProperties() {
    int i = 0;
    char name[256];
    for(auto camera : scene->cameras) {
        if(scene->active_camera == camera) {
            sprintf_s(name, "Camera %d (active)", i+1);
        } else {
            sprintf_s(name, "Camera %d", i+1);
        }
        
        auto camera_group = new MFCCustomProperty(_T(name));
        camera_group->SetData((DWORD_PTR)camera);
        
        {
            char *projection_name = _T("Perspective");
            if(camera->is_orthographic) {
                projection_name = _T("Orthographic");
            }
            auto property = new MFCCustomProperty(_T("Projection Type"), projection_name,
                                                  NULL, new MFCPropertyData(MFCProperty_Type::ProjectionType, &camera->is_orthographic));
            property->AddOption("Orthographic");
            property->AddOption("Frustum");
            property->AddOption("Perspective");
            camera_group->AddSubItem(property);
        }
        {
            auto group = new MFCCustomProperty(_T("Frustum"), 0, TRUE);
            group->AddSubItem(new MFCCustomProperty(_T("left"),   camera->left,   NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->left  )));
            group->AddSubItem(new MFCCustomProperty(_T("right"),  camera->right,  NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->right )));
            group->AddSubItem(new MFCCustomProperty(_T("top"),    camera->top,    NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->top   )));
            group->AddSubItem(new MFCCustomProperty(_T("bottom"), camera->bottom, NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->bottom)));
            group->AddSubItem(new MFCCustomProperty(_T("near"),   camera->z_near, NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->z_near)));
            group->AddSubItem(new MFCCustomProperty(_T("far"),    camera->z_far,  NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->z_far )));
            camera_group->AddSubItem(group);
        }
        {
            auto group = new MFCCustomProperty(_T("Perspective"), 0, TRUE);
            float fovy = -std::atan((camera->top - camera->bottom)/(2.f*camera->z_near))*360.f/F_PI;
            float aspect = (camera->top - camera->bottom)/(camera->right - camera->left);
            group->AddSubItem(new MFCCustomProperty(_T("fovy"),   fovy,   NULL, NULL));
            group->AddSubItem(new MFCCustomProperty(_T("aspect"), aspect, NULL, NULL));
            group->AddSubItem(new MFCCustomProperty(_T("near"),   camera->z_near, NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->z_near)));
            group->AddSubItem(new MFCCustomProperty(_T("far"),    camera->z_far,  NULL, new MFCPropertyData(MFCProperty_Type::Float, &camera->z_far)));
            camera_group->AddSubItem(group);
        }
        
        camera_group->AdjustButtonRect();
        camera_group->AllowEdit();
        camera_group->Enable();
        camera_group->Show();
        camera_group->Redraw();
        
        camera_properties.AddProperty(camera_group);
        ++i;
    }
}

void MFCSceneEditor::InitLightProperties() {
    int i = 0;
    char name[256];
    for(auto light : scene->lights) {
        if(scene->active_light == light) {
            sprintf_s(name, "Light %d (active)", i+1);
        } else {
            sprintf_s(name, "Light %d", i+1);
        }
        
        auto light_group = new MFCCustomProperty(_T(name));
        
        {
            COleVariant enabled((short)((light->settings & Light_Settings::Enabled) != 0), VT_BOOL);
            light_group->AddSubItem(new MFCCustomProperty(_T("Enabled"), enabled, NULL, new MFCPropertyData(MFCProperty_Type::LightSettings, light, Light_Settings::Enabled)));
        }
        {
            char *light_type = "invalid";
            switch(light->type) {
                case Light_Type::Ambient:  { light_type = "Ambient"; }  break;
                case Light_Type::Point:    { light_type = "Point"; }    break;
                case Light_Type::Parallel: { light_type = "Parallel"; } break;
            }
            auto property = new MFCCustomProperty(_T("Light Type"), light_type, NULL, new MFCPropertyData(MFCProperty_Type::LightType, light));
            property->AddOption("Ambient");
            property->AddOption("Point");
            property->AddOption("Parallel");
            light_group->AddSubItem(property);
        }
        {
            auto group = new MFCCustomProperty(_T("Position"), 0, TRUE);
            group->AddSubItem(new MFCCustomProperty(_T("x"), light->position.x, NULL, new MFCPropertyData(MFCProperty_Type::Float, &light->position.x)));
            group->AddSubItem(new MFCCustomProperty(_T("y"), light->position.y, NULL, new MFCPropertyData(MFCProperty_Type::Float, &light->position.y)));
            group->AddSubItem(new MFCCustomProperty(_T("z"), light->position.z, NULL, new MFCPropertyData(MFCProperty_Type::Float, &light->position.z)));
            if(light->type != Light_Type::Point) {
                group->Show(FALSE, TRUE);
            }
            light_group->AddSubItem(group);
        }
        {
            auto group = new MFCCustomProperty(_T("Direction"), 0, TRUE);
            group->SetData((DWORD_PTR)new MFCPropertyData(MFCProperty_Type::Direction, &light->direction));
            group->AddSubItem(new MFCCustomProperty(_T("x"), light->direction.x, NULL, new MFCPropertyData(MFCProperty_Type::Float, &light->direction.x)));
            group->AddSubItem(new MFCCustomProperty(_T("y"), light->direction.y, NULL, new MFCPropertyData(MFCProperty_Type::Float, &light->direction.y)));
            group->AddSubItem(new MFCCustomProperty(_T("z"), light->direction.z, NULL, new MFCPropertyData(MFCProperty_Type::Float, &light->direction.z)));
            if(light->type != Light_Type::Parallel) {
                group->Show(FALSE, TRUE);
            }
            light_group->AddSubItem(group);
        }
        
        {
            COleVariant enabled((short)((light->settings & Light_Settings::Distance_Attenuation) != 0), VT_BOOL);
            auto property = new MFCCustomProperty(_T("Distance Attenuation"), enabled, NULL, new MFCPropertyData(MFCProperty_Type::LightSettings, light, Light_Settings::Distance_Attenuation));
            
            if(light->type != Light_Type::Point) {
                property->Show(FALSE, TRUE);
            }
            light_group->AddSubItem(property);
        }
        
        {
            CPalette *palette = new CPalette();
            auto color_property = new CMFCPropertyGridColorProperty(_T("Hue"), VEC4_TO_RGB(light->hue), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::LightHue, light));
            color_property->EnableOtherButton(_T("Other..."));
            light_group->AddSubItem(color_property);
        }
        {
            light_group->AddSubItem(new MFCCustomProperty(_T("Intensity"), light->intensity, NULL, new MFCPropertyData(MFCProperty_Type::LightIntensity, light)));
        }
        
        light_group->AdjustButtonRect();
        light_group->AllowEdit();
        light_group->Enable();
        light_group->Show();
        light_group->Redraw();
        light_properties.AddProperty(light_group);
        ++i;
    }
}

void MFCSceneEditor::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_MFCPROPERTYGRID3, model_properties);
    DDX_Control(pDX, IDC_MFCPROPERTYGRID4, camera_properties);
    DDX_Control(pDX, IDC_MFCPROPERTYGRID5, light_properties);
    DDX_Control(pDX, IDC_MFCPROPERTYGRID1, scene_properties);
}


BEGIN_MESSAGE_MAP(MFCSceneEditor, CDialog)
ON_BN_CLICKED(IDC_BUTTON_REFRESH, &MFCSceneEditor::OnBnClickedButtonRefresh)
ON_WM_SYSKEYDOWN()
ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// MFCSceneEditor message handlers


void MFCSceneEditor::OnBnClickedButtonRefresh()
{
    scene_properties.RemoveAll();
    InitSceneProperties();
    
    model_properties.RemoveAll();
    InitModelProperties();
    
    camera_properties.RemoveAll();
    InitCameraProperties();
    
    light_properties.RemoveAll();
    InitLightProperties();
}

