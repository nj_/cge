#pragma once
#include "Light.h"
#include "ModelMaterial.h"
#include "Camera.h"

#define INDEX_VALUE(stride, x, y) (x+y*stride)
#define INDEX_RGB(stride, x, y, c) (x+y*stride)*3+c
#define INDEX_RGBA(stride, x, y, c) (x+y*stride)*4+c
#define SCREEN_COORD(p, dim) (int)(((1.0f + p) * (float)((dim - 1))/2))

struct RasterizerData {
    GLfloat *rgba_buffer; // 4*width*height
    GLfloat *z_buffer; // width*height
    
    mat4 projection;
    mat4 camera_transform;
    mat4 world_transform;
    mat3 model_transform;
    mat3 normal_transform;
    ModelMaterial* material;
    Camera* active_camera;
    
    vec4 (*fog_calculation)(RasterizerData*, GLfloat, vec4&);
    vec4 background_color = vec4(0);
    
    int width, height;
    
    std::vector<Light*>* lights;
    
    
    int crazy_color_index = 0;
    static const int crazy_color_count = 20;
    vec4 crazy_color_table[20] = {
        RGB_TO_VEC4(RGB(84, 19, 136)),
        RGB_TO_VEC4(RGB(217, 3, 104)),
        RGB_TO_VEC4(RGB(241, 233, 218)),
        RGB_TO_VEC4(RGB(46, 41, 78)),
        RGB_TO_VEC4(RGB(255, 212, 0)),
        RGB_TO_VEC4(RGB(56, 119, 128)),
        RGB_TO_VEC4(RGB(178, 171, 242)),
        RGB_TO_VEC4(RGB(63, 124, 172)),
        RGB_TO_VEC4(RGB(26, 147, 111)),
        RGB_TO_VEC4(RGB(78, 56, 34)),
        RGB_TO_VEC4(RGB(84, 19, 136)),
        RGB_TO_VEC4(RGB(217, 3, 104)),
        RGB_TO_VEC4(RGB(241, 233, 218)),
        RGB_TO_VEC4(RGB(46, 41, 78)),
        RGB_TO_VEC4(RGB(255, 212, 0)),
        RGB_TO_VEC4(RGB(56, 119, 128)),
        RGB_TO_VEC4(RGB(178, 171, 242)),
        RGB_TO_VEC4(RGB(63, 124, 172)),
        RGB_TO_VEC4(RGB(26, 147, 111)),
        RGB_TO_VEC4(RGB(78, 56, 34)),
    };
};

static inline void fill_pixel(RasterizerData *rdata, int x, int y, GLfloat z, vec4 color) {
    _mm_store_ps(rdata->rgba_buffer + INDEX_RGBA(rdata->width, x, y, 0), color.value);
    rdata->z_buffer[INDEX_VALUE(rdata->width, x, y)] = z;
}

struct RasterizerEdge {
    int x, dx;
    int min_y, max_y, dy;
    GLfloat min_z, max_z;
    int sign, sum;
};

static inline void init_and_clip_edge(RasterizerData *rdata, RasterizerEdge *e, vec3 v0, vec3 v1) {
    e->sign = (v0.x < v1.x) - (v0.x >= v1.x);
    
    e->x  = SCREEN_COORD(v0.x, rdata->width);
    e->dx = (SCREEN_COORD(v1.x, rdata->width) - e->x) * e->sign;
    e->min_y = SCREEN_COORD(v0.y, rdata->height);
    e->max_y = SCREEN_COORD(v1.y, rdata->height);
    e->dy = e->max_y - e->min_y;
    
    e->min_z = v0.z;
    e->max_z = v1.z;
    e->sum = 0;
    
    if(e->dy > 0) {
        if(e->max_y >= rdata->height) {
            e->max_y = rdata->height-1;
        }
        if(e->min_y < 0) {
            if(e->max_y < 0) return;
            
            e->sum += e->dx*(-e->min_y);
            while(e->sum > e->dy) {
                e->x   += e->sign;
                e->sum -= e->dy;
            }
            
            e->min_y = 0;
        }
    }
}

#define ADVANCE_EDGE(e) \
do { \
e->sum += e->dx; \
while(e->sum > e->dy) { \
e->x += e->sign; \
e->sum -= e->dy; \
} \
} while(0)

#define NEXT_TRIANGLE_EDGE(t) \
do {  \
if(t->e0.max_y < t->e1.max_y) { \
if(t->e1.x < t->e2.x) { \
t->e0 = t->e1; \
t->e1 = t->e2; \
} else { \
t->e0 = t->e2; \
} \
} else { \
if(t->e0.x < t->e2.x) { \
t->e1 = t->e2; \
} else { \
t->e1 = t->e0; \
t->e0 = t->e2; \
} \
} \
t->e2.min_y = INT_MAX; \
} while(0)