#include "StdAfx.h"
#include "Camera.h"

Camera::Camera(int width, int height) : width(width), height(height) {}

void Camera::translate(vec3 delta) {
    vec3 vx = delta.x * get_x_axis();
    vec3 vy = delta.y * get_y_axis();
    vec3 vz = delta.z * get_z_axis();
    
    vec3 dv = vx + vy + vz;
    at  += dv;
    eye += dv;
}

void Camera::zoom(float factor) {
    left  *= factor;
    right *= factor;
    
    top    *= factor;
    bottom *= factor;
}

void Camera::global_translate(vec3 delta) {
    at  += delta;
    eye += delta;
}

void Camera::rotate(GLfloat theta, vec3 axis) {
    vec3 vx = axis.x * get_x_axis();
    vec3 vy = axis.y * get_y_axis();
    vec3 vz = axis.z * get_z_axis();
    
    vec3 d_axis = vx + vy + vz;
    
    vec4 v_at = (mat4::Rotate(theta, d_axis))*(at - eye);
    at = eye + vec3FromAffine(v_at);
    
    up = vec3FromAffine((mat4::Rotate(theta, d_axis)) * up);
}

void Camera::rotate_around(GLfloat theta, vec3 axis) {
    vec3 vx = axis.x * get_x_axis();
    vec3 vy = axis.y * get_y_axis();
    vec3 vz = axis.z * get_z_axis();
    
    vec3 d_axis = vx + vy + vz;
    
    vec4 v_eye = (mat4::Rotate(theta, d_axis))*(at - eye);
    eye = at - vec3FromAffine(v_eye);
    
    up = vec3FromAffine((mat4::Rotate(theta, d_axis)) * up);
}

void Camera::global_rotate(GLfloat theta, vec3 axis) {
    vec4 v_at = (mat4::Rotate(theta, axis)) * (at - eye);
    at = eye + vec3FromAffine(v_at);
    
    up = vec3FromAffine((mat4::Rotate(theta, axis)) * up);
}

void Camera::look_at(const vec3& eye, const vec3& at, const vec3& up) {
    this->eye = eye;
    this->at  = at;
    this->up  = up;
}

void Camera::look_at(const vec3& at) {
    vec3 delta = this->at - this->eye;
    this->eye = at - delta;
    this->at = at;
}

mat4 Camera::get_transform() {
    vec4 n = -normalize(eye-at);
    vec4 u = -normalize(cross(up, n));
    vec4 v = -normalize(cross(n, u));
    
    // NOTE(NJ): This is important. Aperantly we cast vec3's to vec4 but always set
    // the w coordinate to 1, so here we get a weird translation of everything by
    // 1 unit to each direction.
    n.w = u.w = v.w = 0.0f;
    
    vec4 t = vec4(0.0f, 0.0f, 0.0f, 1.0f);
    mat4 c = mat4(u, v, n, t);
    return c * mat4::Translate(-eye);
}

mat4 Camera::ortho() {
    float right = this->right * (float)width / pixels_per_unit;
    float left  = this->left  * (float)width / pixels_per_unit;
    float top    = this->top    * (float)height / pixels_per_unit;
    float bottom = this->bottom * (float)height / pixels_per_unit;
    
    return mat4(2.0f/(right-left), 0, 0,  -(right + left)/(right - left),
                0, 2.0f/(top-bottom), 0,  -(top + bottom)/(top - bottom),
                0, 0, -2.0f/(z_far-z_near), -(z_far + z_near)/(z_far - z_near),
                0, 0, 0, 1);
}


mat4 Camera::frustum() {
    float right = this->right * (float)width / pixels_per_unit;
    float left  = this->left  * (float)width / pixels_per_unit;
    float top    = this->top    * (float)height / pixels_per_unit;
    float bottom = this->bottom * (float)height / pixels_per_unit;
    
    // NOTE(NJ): We are multiplying here by -1 because for some reason we get a negative
    // w coordinate after applying this transformation, but to check if we are inside
    // the normalized device we need an inequality that gets flipped in this scenario.
    // Multiplying by -1 fixes this problem by flipping the inequality back, and as
    // we are dividing later by w it does not affect the final point.
    // Maybe there's a better solution, but this works for now.
    return -1*mat4((2.0f*z_near)/(right-left), 0, 0, -z_near*(right+left)/(right-left), 
                   0, (2.0f*z_near)/(top-bottom), 0, -z_near*(top+bottom)/(top-bottom), 
                   0, 0, -(z_far+z_near)/(z_far-z_near), (2.0f*z_near*z_far)/(z_near-z_far), 
                   0, 0, -1.0f, 0);
}

mat4 Camera::get_projection() {
    if(is_orthographic) {
        return ortho();
    } else {
        return frustum();
    }
}

void Camera::set_projection(float left, float right, float bottom, float top, float z_near, float z_far) {
    this->left   = left;
    this->right  = right;
    this->bottom = bottom;
    this->top    = top;
    this->z_near = z_near;
    this->z_far  = z_far;
}

void Camera::set_projection(float fovy, float aspect, float z_near, float z_far) {
    // TODO(NJ): Check if it works or if conversion to radians is needed...
    top    = z_near * std::tan((fovy)/2);
    bottom = -top;
    right  = top * aspect;
    left   = -right;
    z_near = z_near;
    z_far  = z_far;
}

void Camera::reset_projection() {
    left   = -1.0f; right =  1.0f;
    bottom = -1.0f; top   =  1.0f;
    z_near = -8.0f; z_far = -12.0f;
}

void Camera::update_aspect_ratio(int width, int height) {
    this->width  = width;
    this->height = height;
}

vec3 Camera::get_x_axis() {
    return cross(get_z_axis(), get_y_axis());
}

vec3 Camera::get_y_axis() {
    return normalize(up);
}

vec3 Camera::get_z_axis() {
    return normalize(at - eye);
}

