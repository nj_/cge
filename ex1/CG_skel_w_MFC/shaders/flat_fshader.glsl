flat in vec4 fragment_color;

// Lighting inputs
flat in vec4 i_ambient;
flat in vec4 i_diffuse;
flat in vec4 i_specular;

in  vec2 uv_coord;
uniform sampler2D texture_map;

/////////////////////////////////////////////////
////////////// --Color Animation-- //////////////
/////////////////////////////////////////////////

varying vec3 modelspace_pos;

/////////////////////////////////////////////////
//////////////-Color Animation END-//////////////
/////////////////////////////////////////////////


void main() { 
    vec4 temp_color;
    vec2 uv = get_uv(modelspace_pos, uv_coord);
    
    if (material.texture_mode != 0) {
        vec4 base_color = get_tex_color(texture_map, uv, modelspace_pos);
        
        vec4 ambient  = i_ambient  * material.ambient_coefficient;
        vec4 diffuse  = i_diffuse  * material.diffuse_coefficient;
        vec4 specular = i_specular * material.specular_coefficient;
        
        temp_color = (ambient + diffuse + specular) * base_color + material.base_emissive * material.emissive_coefficient;
    } else {
        vec4 ambient  = i_ambient  * material.ambient_coefficient  * material.base_ambient;
        vec4 diffuse  = i_diffuse  * material.diffuse_coefficient  * material.base_diffuse;
        vec4 specular = i_specular * material.specular_coefficient * material.base_specular;
        
        temp_color = (ambient + diffuse + specular) + material.base_emissive * material.emissive_coefficient;
    }

    gl_FragColor = animate_color_fragment(modelspace_pos.xyz, temp_color, material.color_animation_mode);
}
