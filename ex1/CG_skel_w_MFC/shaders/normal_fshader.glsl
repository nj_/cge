uniform vec4 color;
flat in int should_draw;

void main() { 
    if(should_draw == 1) {
        gl_FragColor = color;
    } else {
        gl_FragColor = vec4(0, 0, 0, 0);
    }
}
