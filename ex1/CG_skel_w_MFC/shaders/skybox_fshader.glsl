in vec3 tex_coords;

uniform samplerCube skybox;

void main()
{
	gl_FragColor = texture(skybox, tex_coords);
}