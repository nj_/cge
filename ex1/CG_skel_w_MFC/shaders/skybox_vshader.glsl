in vec3 pos;

out vec3 tex_coords;

uniform mat4 projection;

void main()
{
	tex_coords = pos;
	gl_Position = projection * vec4(pos, 1.f);
}