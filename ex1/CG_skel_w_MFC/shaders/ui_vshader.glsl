in  vec3 position;
in  int vertex_index;
uniform mat4 modelview_transform;

void main() {
    vec3 updated_position = animate_vertex(material.vertex_animation_mode, time, position);
    gl_Position = modelview_transform * vec4(updated_position, 1);
}
