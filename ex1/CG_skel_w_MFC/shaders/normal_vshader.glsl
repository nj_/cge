in  vec3 position;
in  vec3 normal;

uniform mat4 modelview_transform;
uniform mat3 normal_transform;
uniform mat4 projection;
uniform mat4 object_transform;

// oops;

uniform float length;
flat out int should_draw;

void main() {
    should_draw = 1;
    
    if((gl_VertexID % 4) == 1) {
        gl_Position = modelview_transform * vec4(normal, 1.f);
    } else if((gl_VertexID % 4) == 0) {
        vec4 p = object_transform * vec4(position, 1.f);
        vec3 c = vec3(p[0]/p[3], p[1]/p[3], p[2]/p[3]);
        vec3 n = normalize(normal_transform * normal)*length;
        gl_Position = projection * vec4(c + n, 1.f);
    } else {
        gl_Position = vec4(0, 0, 0, 0);
        should_draw = 0;
    }
}
