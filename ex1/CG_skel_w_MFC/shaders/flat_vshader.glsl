in  vec3 position;
in  vec3 normal;

uniform mat4 modelview_transform;
uniform mat3 normal_transform;

in  int vertex_index;
vec3 updated_position;

uniform mat4 object_transform;
uniform mat4 projection;

// Lighting outputs
flat out vec4 i_ambient;
flat out vec4 i_diffuse;
flat out vec4 i_specular;

in  vec2 vuv_coord;
out vec2 uv_coord;

/////////////////////////////////////////////////
////////////// --Color Animation-- //////////////
/////////////////////////////////////////////////

varying vec3 modelspace_pos;

void animate_color_vertex(vec3 animate_colors_1_pos) {
    modelspace_pos = animate_colors_1_pos.xyz;
}

/////////////////////////////////////////////////
//////////////-Color Animation END-//////////////
/////////////////////////////////////////////////


/////////////////////////////////////////////////
////////////// --Light interface-- //////////////
/////////////////////////////////////////////////

#define LIGHT_ON 0
#define AMBIENT_ON 1
#define DIFFUSIVE_ON 2
#define SPECULAR_ON 3
#define DISTANCE_ON 4

#define TYPE_POINT 1
#define TYPE_PARALLEL 2
#define TYPE_AMBIENT 3

uniform int max_light_count;
uniform vec3 camera_eye;

vec3 light_direction;
vec3 v_normal;


float distance_attentuation(int index) {
    float distance = distance(updated_position, lights[index].position);
    float denominator = distance * (distance * lights[index].attentuation_quadratic + lights[index].attentuation_linear) + lights[index].attentuation_constant;
    if (denominator == 0.f) {
        return 1.f;
    }
    return 1 / (denominator);
}


vec4 calculate_specular(int index) {
    vec3 viewing_angle = normalize(camera_eye - updated_position);
    vec3 reflection_angle = 2 * dot(light_direction, v_normal) * (v_normal) - light_direction;
    return lights[index].color * max(0, pow(clamp(dot(reflection_angle, viewing_angle),0.0,1.0), material.shininess_coefficient));
}

vec4 calculate_diffuse(int index) {
    return max(0, dot(light_direction, v_normal)) * lights[index].color;
}

vec4 calculate_ambient(int index) {
    return lights[index].color;
}


void calculate_lighting(int index) {
    
    vec4 ambient, diffuse, specular;
    float distance_term;
    
    if (lights[index].light_data_settings[AMBIENT_ON] == 1) {
        ambient = calculate_ambient(index);
    } else {
        ambient = vec4(0);
    }
    
    if (lights[index].type == TYPE_AMBIENT) {
        i_ambient += ambient;
        return;
    }
    
    if (lights[index].light_data_settings[DIFFUSIVE_ON] == 1) {
        diffuse = calculate_diffuse(index);
    } else {
        diffuse = vec4(0);
    }
    
    if (lights[index].light_data_settings[SPECULAR_ON] == 1) {
        specular = calculate_specular(index);
    } else {
        specular = vec4(0);
    }
    
    if (lights[index].light_data_settings[DISTANCE_ON] == 1) {
        distance_term = distance_attentuation(index);
    }
    else {
        distance_term = 1;
    } 
    
    i_ambient += distance_term * ambient;
    i_diffuse += distance_term * diffuse;
    i_specular += distance_term * specular;
}


void lighting_summation() {
    i_ambient = vec4(0);
    i_diffuse = vec4(0);
    i_specular = vec4(0);
    
    for (int i = 0; i < max_light_count && lights[i].type != 0; i++) {
        if ( lights[i].light_data_settings[LIGHT_ON] == 1 ) {
            if (lights[i].type == TYPE_POINT) {
                light_direction = normalize(lights[i].position - updated_position);
            }
            else {
                light_direction = normalize(lights[i].direction);
            }
            calculate_lighting(i);
        }
    }
    
    i_ambient = clamp(i_ambient, 0, 1);
    i_diffuse = clamp(i_diffuse, 0, 1);
    i_specular = clamp(i_specular, 0, 1);
}

/////////////////////////////////////////////////
//////////////-Light Interface END-//////////////
/////////////////////////////////////////////////

/////////////////////////////////////////////////
////////////// -- Main sequence -- //////////////
/////////////////////////////////////////////////

void main() {
    animate_color_vertex(position);
    updated_position = animate_vertex(material.vertex_animation_mode, time, position);
    vec4 temp_pos = object_transform * vec4(updated_position, 1);
    
    updated_position = temp_pos.xyz / temp_pos.w;
    
    v_normal = normalize(normal_transform * normal);
    lighting_summation();
    gl_Position = projection * temp_pos;
    uv_coord = vuv_coord;
}
