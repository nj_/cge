in  vec3 position;
in  vec3 normal;

uniform mat4 modelview_transform;
uniform mat3 normal_transform;

in  int vertex_index;
vec3 updated_position;

varying vec3 fragment_normal;

in  vec2 vuv_coord;
out vec2 uv_coord;

/////////////////////////////////////////////////
////////////// --Phong Specifics-- //////////////
/////////////////////////////////////////////////

varying vec3 f_position;
uniform mat4 object_transform;
uniform mat4 projection;

/////////////////////////////////////////////////
////////////// --Color Animation-- //////////////
/////////////////////////////////////////////////

varying vec3 modelspace_pos;

void animate_color_vertex() {
    modelspace_pos = position.xyz;
}

/////////////////////////////////////////////////
//////////////-Color Animation END-//////////////
/////////////////////////////////////////////////

/////////////////////////////////////////////////
////////////// -- Main sequence -- //////////////
/////////////////////////////////////////////////

in vec3 tangent;
in vec3 bitangent;

out mat3 TBN;

void main() {
    animate_color_vertex();
    vec3 T = normalize(normal_transform * tangent);
    vec3 B = normalize(normal_transform * bitangent);
    vec3 N = normalize(normal_transform * normal);
    TBN = mat3(T, B, N);
    
    updated_position = animate_vertex(material.vertex_animation_mode, time, position);
    
    fragment_normal = normalize(normal_transform * normal);
    vec4 temp_pos = object_transform * vec4(updated_position, 1.f);
    gl_Position = projection * temp_pos;
    f_position = temp_pos.xyz / temp_pos.w;
    uv_coord = vuv_coord;
}
