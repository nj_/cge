#include "StdAfx.h"
#include "MeshModel.h"
#include "vec.h"
#include "veci.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;

MeshModel::MeshModel(string fileName) : MeshModel() {
    loadFile(fileName);
    CalculateFaceNormals();
}

MeshModel::MeshModel(const MeshModel &m) : 
MeshModel(m.vertices, m.faces, m.indices, m.triangle_count, m.boundary) {
    world_transform = m.world_transform;
    model_transform = m.model_transform;
}


MeshModel::MeshModel(const VertexData *vertices, const FaceData *faces, const GLint *indices,
                     const GLsizei triangle_count, const Boundary& boundary) : boundary(boundary) {
    this->triangle_count = triangle_count;
    GLsizei vertex_count = triangle_count*3;
    if(vertices) {
        this->vertices = new VertexData[vertex_count];
        memcpy(this->vertices, vertices, vertex_count*sizeof(VertexData));
    }
    if(faces) {
        this->faces = new FaceData[vertex_count];
        memcpy(this->faces, faces, vertex_count*sizeof(FaceData));
    }
    if(indices) {
        this->indices  = new GLint[vertex_count];
        memcpy(this->indices, indices, vertex_count*sizeof(GLint));
    }
    SetBoundingBox(boundary);
    CalculateFaceNormals();
}

struct FaceIndecies {
    vector<vec3i> v;
    
    FaceIndecies(GLsizei count = 3) {
        v.resize(count, 0);
    }
    
    FaceIndecies(std::istream & aStream) {
        char c;
        for(int i = 0; aStream.peek() != EOF; i++) {
            vec3i vertex;
            aStream >> std::ws >> vertex.v >> std::ws;
            
            if (aStream.peek() == '/'){
                aStream >> c >> std::ws;
                
                if (aStream.peek() == '/') {
                    aStream >> c >> std::ws >> vertex.n;
                } else {
                    aStream >> vertex.t;
                    if (aStream.peek() == '/') {
                        aStream >> c >> vertex.n;
                    }
                }
            }
            
            v.push_back(vertex);
        }
    }
    
    GLsizei size() const{
        return (GLsizei)v.size();
    }
};

vec3 vec3fFromStream(std::istream & aStream) {
    GLfloat x, y, z;
    aStream >> x >> std::ws >> y >> std::ws >> z;
    return vec3(x, y, z);
}

vec2 vec2fFromStream(std::istream & aStream) {
    GLfloat x, y;
    aStream >> x >> std::ws >> y;
    return vec2(x, y);
}

void MeshModel::loadFile(string fileName) {
    ifstream ifile(fileName.c_str());
    vector<FaceIndecies> faces;
    vector<vec3> vertices;
    vector<vec3> normals;
    vector<vec2> textures;
    
    boundary.min_x = FLT_MAX; boundary.max_x = -FLT_MAX;
    boundary.min_y = FLT_MAX; boundary.max_y = -FLT_MAX;
    boundary.min_z = FLT_MAX; boundary.max_z = -FLT_MAX;
    
    triangle_count = 0;
    
    // while not end of file
    while (!ifile.eof()) {
        // get line
        string curLine;
        getline(ifile, curLine);
        
        // read type of the line
        istringstream issLine(curLine);
        string lineType;
        
        issLine >> std::ws >> lineType;
        
        // based on the type parse data
        if (lineType == "v") {
            vec3 v = vec3fFromStream(issLine);
            boundary.min_x = minimum(boundary.min_x, v.x); boundary.max_x = maximum(boundary.max_x, v.x);
            boundary.min_y = minimum(boundary.min_y, v.y); boundary.max_y = maximum(boundary.max_y, v.y);
            boundary.min_z = minimum(boundary.min_z, v.z); boundary.max_z = maximum(boundary.max_z, v.z);
            vertices.push_back(v);
        } else if (lineType == "f") {
            FaceIndecies face(issLine);
            if(face.size() <= 2) {
                cout << "Illeagal vertex count in face: \"" << curLine << "\"" << endl;
                cout  << "(vertex count is " << face.size() << " but must be at least 3)" << endl;
            } else {
                faces.push_back(face);
                triangle_count += face.size() - 2;
            }
        } else if (lineType == "vn") {
            normals.push_back(vec3fFromStream(issLine));
        } else if (lineType == "vt") {
            textures.push_back(vec2fFromStream(issLine));
        } else if (lineType == "o") {
            issLine >> name;
        } else if (lineType == "#" || lineType == "") {
            // comment / empty line
        } else {
            cout << "Found unknown line Type \"" << lineType << "\"" << endl;
        }
    }
    
    SetBoundingBox(boundary);
    
    this->indices  = new GLint[triangle_count * 3];
    this->vertices = new VertexData[triangle_count * 3];
    if(textures.size() > 0) {
        this->uv_coords = new vec2[triangle_count * 3];
    }
    
    // iterate through all stored faces and create triangles
    int vertex_count = 0;
    for (auto it : faces) {
        for(int i = 1; i < it.size() - 1; ++i) {
            this->indices[vertex_count] = it.v[0].v;
            AddVertex(it.v[0],   vertex_count++, &vertices, &normals, &textures);
            this->indices[vertex_count] = it.v[i].v;
            AddVertex(it.v[i],   vertex_count++, &vertices, &normals, &textures);
            this->indices[vertex_count] = it.v[i+1].v;
            AddVertex(it.v[i+1], vertex_count++, &vertices, &normals, &textures);
        }
    }
}

inline void MeshModel::AddVertex(vec3i v, int index,
                                 vector<vec3> *vertices,
                                 vector<vec3> *normals,
                                 vector<vec2> *textures) {
    assert(this->vertices && 1 <= v.v && v.v <= vertices->size());
    this->vertices[index].position = (*vertices)[v.v - 1];
    
    bool has_normal = (1 <= v.n && v.n <= normals->size());
    this->vertices[index].normal = (has_normal ? (*normals)[v.n - 1] : vec3());
    
    if(this->uv_coords) {
        bool has_texture = (1 <= v.t && v.t <= textures->size());
        this->uv_coords[index] = (has_texture ? (*textures)[v.t - 1] : vec2());
    }
}

void MeshModel::CalculateFaceNormals() {
    if(!faces) {
        faces = new FaceData[triangle_count*3];
    }
    
    for(int i = 0; i < triangle_count; ++i) {
        FaceData face = {};
        vec3 v0 = vertices[i*3 + 0].position;
        vec3 v1 = vertices[i*3 + 1].position;
        vec3 v2 = vertices[i*3 + 2].position;
        
        vec3 normal = cross(v1-v0, v2-v0);
        vec3 center = (v0 + v1 + v2)/3.f;
        
        face.normal = normal;
        face.center = center;
        
        if(uv_coords) {
            vec2 uv0 = uv_coords[i*3 + 0];
            vec2 uv1 = uv_coords[i*3 + 1];
            vec2 uv2 = uv_coords[i*3 + 2];
            
            vec3 delta_pos1 = v1-v0;
            vec3 delta_pos2 = v2-v0;
            
            vec2 delta_uv1 = uv1-uv0;
            vec2 delta_uv2 = uv2-uv0;
            
            GLfloat r = 1.f / (delta_uv1.x * delta_uv2.y - delta_uv2.x * delta_uv1.y);
            
            face.tangent   = (delta_pos1 * delta_uv2.y - delta_pos2 * delta_uv1.y) * r;
            face.bitangent = (delta_pos2 * delta_uv1.x - delta_pos1 * delta_uv2.x) * r;
        }
        
        faces[i*3 + 0] = face;
        faces[i*3 + 1] = face;
        faces[i*3 + 2] = face;
    }
}

void MeshModel::SetBoundingBox(const Boundary& boundary) {
    vec3 *b = bounding_box;
    b[0] = vec3(boundary.min_x, boundary.min_y, boundary.min_z);
    b[1] = vec3(boundary.min_x, boundary.min_y, boundary.max_z);
    b[2] = vec3(boundary.max_x, boundary.min_y, boundary.min_z);
    b[3] = vec3(boundary.max_x, boundary.min_y, boundary.max_z);
    b[4] = vec3(boundary.min_x, boundary.max_y, boundary.min_z);
    b[5] = vec3(boundary.min_x, boundary.max_y, boundary.max_z);
    b[6] = vec3(boundary.max_x, boundary.max_y, boundary.min_z);
    b[7] = vec3(boundary.max_x, boundary.max_y, boundary.max_z);
    
    b += 8;
    b[0] = vec3(boundary.min_x, boundary.min_y, boundary.min_z);
    b[1] = vec3(boundary.min_x, boundary.max_y, boundary.min_z);
    b[2] = vec3(boundary.max_x, boundary.min_y, boundary.min_z);
    b[3] = vec3(boundary.max_x, boundary.max_y, boundary.min_z);
    b[4] = vec3(boundary.min_x, boundary.min_y, boundary.max_z);
    b[5] = vec3(boundary.min_x, boundary.max_y, boundary.max_z);
    b[6] = vec3(boundary.max_x, boundary.min_y, boundary.max_z);
    b[7] = vec3(boundary.max_x, boundary.max_y, boundary.max_z);
    
    b += 8;
    b[0] = vec3(boundary.min_x, boundary.min_y, boundary.min_z);
    b[1] = vec3(boundary.max_x, boundary.min_y, boundary.min_z);
    b[2] = vec3(boundary.min_x, boundary.max_y, boundary.min_z);
    b[3] = vec3(boundary.max_x, boundary.max_y, boundary.min_z);
    b[4] = vec3(boundary.min_x, boundary.min_y, boundary.max_z);
    b[5] = vec3(boundary.max_x, boundary.min_y, boundary.max_z);
    b[6] = vec3(boundary.min_x, boundary.max_y, boundary.max_z);
    b[7] = vec3(boundary.max_x, boundary.max_y, boundary.max_z);
    
    this->boundary.center = vec3((boundary.max_x + boundary.min_x)/2.f,
                                 (boundary.max_y + boundary.min_y)/2.f,
                                 (boundary.max_z + boundary.min_z)/2.f);
}

bool MeshModel::is_in_clip_space(mat4 normalized_device_transform) {
    GLfloat min_x = (std::numeric_limits<GLfloat>::max)(), max_x = -(std::numeric_limits<GLfloat>::max)();
    GLfloat min_y = (std::numeric_limits<GLfloat>::max)(), max_y = -(std::numeric_limits<GLfloat>::max)();
    GLfloat min_z = (std::numeric_limits<GLfloat>::max)(), max_z = -(std::numeric_limits<GLfloat>::max)();
    
    for(int i = 0; i < bounding_box_corner_count; ++i) {
        vec4 v = (normalized_device_transform * bounding_box[i]);
        if(!z_in_bounds(v)) continue;
        v /= v.w;
        if((-1.f <= v.z && v.z <= 1.f) &&
           (-1.f <= v.x && v.x <= 1.f) &&
           (-1.f <= v.y && v.y <= 1.f)) {
            return true;
        }
        min_x = minimum(min_x, v.x); max_x = maximum(max_x, v.x);
        min_y = minimum(min_y, v.y); max_y = maximum(max_y, v.y);
        min_z = minimum(min_z, v.z); max_z = maximum(max_z, v.z);
    }
    if((min_x <= -1.0f && max_x >= 1.0f) ||
       (min_y <= -1.0f && max_y >= 1.0f) ||
       (min_z <= -1.0f && max_z >= 1.0f)) {
        return true;
    }
    
    return false;
}

void MeshModel::PrintModelInCFormat() {
    cout.precision(6);
    cout << fixed;
    cout << endl << endl;
    cout << "static const GLsizei triangle_count = " << triangle_count << ";" << endl;
    cout << "static const vec3 bounding_box[] = {" << endl;
    for (int i = 0; i < bounding_box_corner_count; ++i) {
        vec3 v = bounding_box[i];
        cout << "{" << v.x << "f," << v.y << "f," << v.z << "f" << "}, ";
        
        if(i % 2 == 1) {
            cout << endl;
        }
    }
    cout << "};" << endl;
    
    cout << "static const vec3 vertices[] = {" << endl;
    for (int i = 0; i < triangle_count * 3; ++i) {
        vec3 v = vertices[i].position;
        cout << "{" << v.x << "f," << v.y << "f," << v.z << "f" << "}, ";
        
        if(i % 3 == 2) {
            cout << endl;
        }
    }
    cout << "};" << endl;
    
    cout << "static const vec3 normals[] = {" << endl;
    for (int i = 0; i < triangle_count * 3; ++i) {
        vec3 v = vertices[i].normal;
        cout << "{" << v.x << "f," << v.y << "f," << v.z << "f" << "}, ";
        
        if(i % 3 == 2) {
            cout << endl;
        }
    }
    cout << "};" << endl;
    
    if(uv_coords) {    
        cout << "static const vec3 textures[] = {" << endl;
        for (int i = 0; i < triangle_count * 3; ++i) {
            vec2 v = uv_coords[i];
            cout << "{" << v.x << "f," << v.y << "f" << "}, ";
            
            if(i % 3 == 2) {
                cout << endl;
            }
        }
        cout << "};" << endl;
    }
}

void MeshModel::iterate_shading_method() {
    if (material.shading_mode == Shading_Mode::Flat) {
        material.shading_mode = Shading_Mode::Gouraud;
    } else if (material.shading_mode == Shading_Mode::Gouraud) {
        material.shading_mode = Shading_Mode::Phong;
    } else {
        material.shading_mode = Shading_Mode::Flat;
    }
}

inline void load_gl_image(const char *file_name, GLuint *handle, GLuint texture_index) {
    GLint width, height, channel_count;
    stbi_set_flip_vertically_on_load(true);
    
    if(*handle != 0) {
        glDeleteTextures(1, handle);
    }
    
    GLubyte *data = stbi_load(file_name, &width, &height, &channel_count, 0);
    if(!data) return;
    
    glActiveTexture(texture_index);
    glGenTextures(1, handle);
    glBindTexture(GL_TEXTURE_2D, *handle);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    stbi_image_free(data);
}

void MeshModel::load_texture_map() {
    load_gl_image(texture_map_path.c_str(), &gl_data.texture_map, GL_TEXTURE0);
    material.texture_mode = 1;
}

void MeshModel::load_normal_map() {
    load_gl_image(normal_map_path.c_str(), &gl_data.normal_map, GL_TEXTURE1);
    material.use_normal_map = true;
}
