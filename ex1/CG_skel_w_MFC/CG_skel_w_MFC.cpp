// CG_skel_w_MFC.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CG_skel_w_MFC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// The one and only application object

#include "GL/glew.h"
#include "GL/freeglut.h"
#include "GL/freeglut_ext.h"
#include "MFCSceneEditor.h"
#include "vec.h"
#include "mat.h"
#include "InitShader.h"
#include "Scene.h"
#include "Renderer.h"
#include "PrimeMeshModel.h"
#include <string>

#define BUFFER_OFFSET(offset) ((GLvoid*) (offset))

Scene *scene;
Renderer *renderer;
char window_title[256];

int last_x,last_y;
bool lb_down,rb_down,mb_down;

static bool should_draw = true;

//Default model path settings:
static const char *default_model_path = "primitives/sphere.obj";

// Debug FPS statistic counters:
static LARGE_INTEGER debug_last_counter, debug_frame_start, debug_frame_end, debug_counter_frequency, debug_dt;
static int64_t crazy_elapsed = 0, initial_crazy_index = 0;

Interpolating_Colors *crazy_colors;
Shuffle_Container *crazy_shuffle;

//----------------------------------------------------------------------------
// Callbacks


void display(void) {
    // 1 = always draw, give fps counter
    // 0 = draw only when needed
    if(should_draw || (scene->settings & Scene_Settings::AlwaysDraw)) {
        QueryPerformanceCounter(&debug_frame_start);
        
        scene->draw();
        
        QueryPerformanceCounter(&debug_frame_end);
        int64_t time_elapsed = debug_frame_end.QuadPart - debug_last_counter.QuadPart;
        crazy_elapsed += time_elapsed/100;
        scene->time += .001*(GLfloat)time_elapsed;
        
        int32_t time_per_frame = (int32_t)((1000*time_elapsed)/debug_counter_frequency.QuadPart);
        int32_t frames_per_second = (int32_t)(debug_counter_frequency.QuadPart/time_elapsed);
        // printf("time: %f, ms/frame: %d, FPS: %d\n", scene->time, time_per_frame, frames_per_second);
        
        debug_last_counter = debug_frame_end;
        
        should_draw = false;
    }
}

void reshape(int width, int height) {
    scene->update_aspect_ratio(width, height);
    should_draw = true;
}


void handle_input_for_model(unsigned char key, int x, int y) {
    if(!scene->active_model) return;
    
    switch(key) {
        case 'a': { scene->translate_active_model(vec3(-1.0f,  0.0f,  0.0f)); } break;
        case 'd': { scene->translate_active_model(vec3( 1.0f,  0.0f,  0.0f)); } break;
        case 'S': { scene->translate_active_model(vec3( 0.0f, -1.0f,  0.0f)); } break;
        case 'W': { scene->translate_active_model(vec3( 0.0f,  1.0f,  0.0f)); } break;
        case 's': { scene->translate_active_model(vec3( 0.0f,  0.0f, -1.0f)); } break;
        case 'w': { scene->translate_active_model(vec3( 0.0f,  0.0f,  1.0f)); } break;
        
        case 'z': { scene->rotate_active_model_z( 1.0f); } break;
        case 'Z': { scene->rotate_active_model_z(-1.0f); } break;
        case 'x': { scene->rotate_active_model_x( 1.0f); } break;
        case 'X': { scene->rotate_active_model_x(-1.0f); } break;
        case 'y': { scene->rotate_active_model_y( 1.0f); } break;
        case 'Y': { scene->rotate_active_model_y(-1.0f); } break;
        
        case 'Q': { scene->scale_active_model_up();   } break;
        case 'q': { scene->scale_active_model_down(); } break;
        
        case 'k': { scene->unload_active_model(); } break;
        
        case '5': {
            scene->active_model->settings ^= Model_Settings::Draw_Wireframe;
        } break;
        
        case '6': {
            scene->active_model->material.shading_mode = Shading_Mode::Experimental;
        } break;
        
        case '7': {
            scene->active_model->material.shading_mode = Shading_Mode::Flat;
        } break;
        
        case '8': {
            scene->active_model->material.shading_mode = Shading_Mode::Gouraud;
        } break;
        
        case '9': {
            scene->active_model->material.shading_mode = Shading_Mode::Phong;
        } break;
        
        case 'r': {
            scene->reset_active_model_mtransform();
            scene->reset_active_model_wtransform();
        } break;
        
        case '\t': { scene->iterate_models(); } break;
    }
    
    should_draw = true;
}

void handle_input_for_camera(unsigned char key, int x, int y) {
    switch(key) {
        case 'a': { scene->translate_active_camera(vec3(-1.0f,  0.0f,  0.0f)); } break;
        case 'd': { scene->translate_active_camera(vec3( 1.0f,  0.0f,  0.0f)); } break;
        case 'S': { scene->translate_active_camera(vec3( 0.0f, -1.0f,  0.0f)); } break;
        case 'W': { scene->translate_active_camera(vec3( 0.0f,  1.0f,  0.0f)); } break;
        case 's': { scene->translate_active_camera(vec3( 0.0f,  0.0f, -1.0f)); } break;
        case 'w': { scene->translate_active_camera(vec3( 0.0f,  0.0f,  1.0f)); } break;
        
        case 'z': { scene->rotate_active_camera_z( 1.0f); } break;
        case 'Z': { scene->rotate_active_camera_z(-1.0f); } break;
        case 'x': { scene->rotate_active_camera_x( 1.0f); } break;
        case 'X': { scene->rotate_active_camera_x(-1.0f); } break;
        case 'y': { scene->rotate_active_camera_y( 1.0f); } break;
        case 'Y': { scene->rotate_active_camera_y(-1.0f); } break;
        
        case 'k': { scene->unload_active_camera(); } break;
        case 'n': { scene->add_camera(); } break;
        
        case 'r': { scene->reset_active_camera(); } break;
        
        case 'o': {
            scene->settings ^= Scene_Settings::CameraMovesInWorldSpace;
        } break;
        
        case '\t': { scene->iterate_cameras(); } break;
    }
    
    should_draw = true;
}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 033: {
            exit(EXIT_SUCCESS);
        } break;
        
        case 'c': {
            scene->settings ^= Scene_Settings::CameraMode;
            scene->settings ^= Scene_Settings::ModelMode;
            should_draw = true;
        } break;
        
        case 'C': {
            scene->settings ^= Scene_Settings::CameraRotatesAroundAt;
            scene->settings |= Scene_Settings::CameraMode;
            scene->settings &= (~Scene_Settings::ModelMode);
            should_draw = true;
        } break;
        
        case 'b': { //Display/undisplay bounding box
            if(scene->active_model) {
                scene->active_model->settings ^=  Model_Settings::DrawBoundingBox;
                should_draw = true;
            }
        } break;
        
        case 'M': { //Display/undisplay bounding box
            scene->settings ^= Scene_Settings::MousePassivelyMoveable;
        } break;
        
        case '1': { //Display/undisplay bounding box
            if(scene->active_model) {
                scene->active_model->settings ^=  Model_Settings::DrawVertexNormals;
                should_draw = true;
            }
        } break;
        
        case '2': { //Display/undisplay bounding box
            if(scene->active_model) {
                scene->active_model->settings ^=  Model_Settings::DrawFaceNormals;
                should_draw = true;
            }
        } break;
        
        case 'f': { //Display/undisplay bounding box
            scene->settings ^= Scene_Settings::ViewFrame;
            scene->settings ^= Scene_Settings::WorldFrame;
        } break;
        
        case 'j': {
            scene->look_at_active_model();
            should_draw = true;
        } break;
        
        case 'l': { //Iterate over shadings methods on the active model
            scene->active_model->iterate_shading_method();
            should_draw = true;
        } break;
        
        case 'L': { //Add a light source at camera placement
            scene->add_light();
            should_draw = true;
        } break;
        //TODO(OZ): Add a 'light control mode', making full use of the interface implemented in 'Scene.h'.
        case 'h': { //Removes the active light source (we do not allow 0 light sources). Active light is now it.
            scene->unload_active_light();
            should_draw = true;
        } break;
        
        case 'H': { //iterates over to the next light source
            scene->iterate_active_light();
        } break;
        
        case 'p': {
            scene->active_camera->is_orthographic = !scene->active_camera->is_orthographic;
            scene->update_aspect_ratio(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
            should_draw = true;
        } break;
        
        case ',': {
            scene->model_scaling_switch();
        } break;
        
        case '+': {
            scene->modify_model_translation_delta(+0.1f);
            scene->modify_model_scaling_delta(+0.1f);
            scene->modify_model_rotation_delta(+0.1f);
        } break;
        
        case '-': {
            scene->modify_model_translation_delta(-0.1f);
            scene->modify_model_scaling_delta(-0.1f);
            scene->modify_model_rotation_delta(-0.1f);
        } break;
        
        case '=': {
            scene->reset_model_delta();
        } break;
        
        case ')': {
            scene->modify_camera_translation_delta(+0.1f);
            scene->modify_camera_scaling_delta(+0.1f);
            scene->modify_camera_rotation_delta(+0.1f);
        } break;
        
        case '(': {
            scene->modify_camera_translation_delta(-0.1f);
            scene->modify_camera_scaling_delta(-0.1f);
            scene->modify_camera_rotation_delta(-0.1f);
        } break;
        
        case '0': {
            scene->reset_camera_delta();
        } break;
        
        default: {
            if(scene->settings & Scene_Settings::CameraMode) {
                handle_input_for_camera(key, x, y);
            } else {
                handle_input_for_model(key, x, y);
            }
        } break;
    }
}

void mouse(int button, int state, int x, int y)
{
    //button = {GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON}
    //state = {GLUT_DOWN,GLUT_UP}
    
    //set down flags
    switch(button) {
        case GLUT_LEFT_BUTTON: {
            lb_down = (state==GLUT_UP)?0:1;
        } break;
        case GLUT_RIGHT_BUTTON: {
            rb_down = (state==GLUT_UP)?0:1;
        } break;
        case GLUT_MIDDLE_BUTTON: {
            mb_down = (state==GLUT_UP)?0:1;
        } break;
    }
    
    // add your code
}

void mouse_wheel(int button, int dir, int x, int y) {
    //set down flags
    if(dir > 0) { // Scroll up
        scene->zoom_in_active_camera();
    } else { // Scroll down
        scene->zoom_out_active_camera();
    }
    
    should_draw = true;
}

void motion(int x, int y) {
    // calc difference in mouse movement
    int dx = x - last_x;
    int dy = y - last_y;
    // update last x,y
    last_x = x;
    last_y = y;
    if (scene->settings & Scene_Settings::CameraMode) {
        if (scene->settings & Scene_Settings::MouseMoveable) {
            GLfloat x_theta = scene->normalize_mouse_movement_x(dy); //It's confusing, but dy signals the mouse moving up/down, so we should rotate around X
            GLfloat y_theta = scene->normalize_mouse_movement_y(dx); //Same here.
            
            x_theta *= 10.0f;
            y_theta *= 10.0f;
            
            scene->rotate_active_camera_x(x_theta);
            scene->rotate_active_camera_y(y_theta);
            
            should_draw = true;
        }
    }
    else {
        GLfloat x_theta = -scene->normalize_mouse_movement_x(dy);
        GLfloat y_theta = -scene->normalize_mouse_movement_y(dx);
        
        x_theta *= 40.0f;
        y_theta *= 40.0f;
        
        scene->rotate_active_model_x(x_theta);
        scene->rotate_active_model_y(y_theta);
        
        should_draw = true;
    }
}

void passive_motion(int x, int y) {
    // calc difference in mouse movement
    int dx = x - last_x;
    int dy = y - last_y;
    // update last x,y
    last_x = x;
    last_y = y;
    
    if ((scene->settings & Scene_Settings::MousePassivelyMoveable) &&
        (scene->settings & Scene_Settings::CameraMode)) {
        //It's confusing, but dy signals the mouse moving up/down, so we should rotate around X
        GLfloat x_theta = scene->normalize_mouse_movement_x(dy);
        GLfloat y_theta = scene->normalize_mouse_movement_y(dx); //Same here.
        
        x_theta *= 2.0f;
        y_theta *= 2.0f;
        
        scene->rotate_active_camera_x(x_theta);
        scene->rotate_active_camera_y(y_theta);
        
        should_draw = true;
    }
}

void ContextMenuAdd(int id) {
    using namespace Context_Menu_Action;
    switch (id) {
        case AddTetrahedron: {
            scene->add_model(new MeshModel(Primitives::Tetrahedron));
        } break;
        case AddCube: {
            scene->add_model(new MeshModel(Primitives::Cube));
        } break;
        case AddSphere: {
            scene->add_model(new MeshModel(Primitives::Sphere));
        } break;
        case AddCylinder: {
            scene->add_model(new MeshModel(Primitives::Cylinder));
        } break;
        case AddDonut: {
            scene->add_model(new MeshModel(Primitives::Donut));
        } break;
        case AddMonkey: {
            scene->add_model(new MeshModel(Primitives::Monkey));
        } break;
        case AddBanana: {
            scene->add_model(new MeshModel(Primitives::Banana));
        } break;
    }
    
    should_draw = true;
}

void ContextMenu(int id) {
    using namespace Context_Menu_Action;
    switch (id) {
        case OpenModelFile: {
            CFileDialog dlg(TRUE,_T(".obj"),NULL,NULL,_T("*.obj|*.*"));
            
            if(dlg.DoModal()==IDOK) {
                std::string s((LPCTSTR)dlg.GetPathName());
                scene->load_obj_model((LPCTSTR)dlg.GetPathName());
                should_draw = true;
            }
        } break;
        
        case ResetActiveCamera: {
            scene->active_camera->reset_projection();
            scene->reset_camera_delta();
            should_draw = true;
        } break;
        
        case ResetStepSize: {
            scene->reset_model_delta();
        } break;
        
        case ToggleSSAA: {
            if(renderer->ssaa_factor == 1) {
                renderer->ssaa_factor = 2;
            } else {
                renderer->ssaa_factor = 1;
            }
            scene->renderer->reallocate_buffers();
            should_draw = true;
        } break;
        
        case SceneEditor: {
            auto dialog = new MFCSceneEditor(scene);
            dialog->Create(IDD_SCENE_EDITOR, 0);
            dialog->ShowWindow(SW_SHOW);
            should_draw = true;
        } break;
        
        case PrintActiveModel: {
            scene->print_active_model();
        } break;
        case About: {
            AfxMessageBox(_T("Project by Noam Jacobi and Ori Zarhin.\n"
                             "Keybindings:\n"
                             "-- translation:\n"
                             "     w a s d W S: in, left, out, right, up, down\n"
                             "-- rotation:\n"
                             "     x y z: rotate around x/y/z axis (counter clockwise)\n"
                             "     X Y Z: rotate around x/y/z axis (clockwise)\n"
                             "-- scaling:\n"
                             "     q Q: scale down/up\n"
                             "-- show/hide stuff:\n"
                             "     d: draw bounding box\n"
                             "     1: draw vertex normals\n"
                             "     2: draw face normals\n"
                             "---- other:\n"
                             "     c: switch between model and camera modes\n"
                             "     C: switch between rotating the camera around whatever it looks at or around it's position\n"
                             "     tab: iterate through models/cameras\n"
                             "     k: delete the active model/camera\n"
                             "     n: add a new camera\n"
                             "     M: allow passive mouse control over the camera\n"
                             "     ,: change scaling to operate over each axis\n"
                             "     j: look at the active model\n"
                             "     +: increment step size\n"
                             "     -: decrement step size\n"
                             ));
        } break;
    }
}

void initMenu() {
    using namespace Context_Menu_Action;
    
    int submenu_add = glutCreateMenu(ContextMenuAdd);
    glutAddMenuEntry("Cube",        AddCube);
    glutAddMenuEntry("Tetrahedron", AddTetrahedron);
    glutAddMenuEntry("Sphere",      AddSphere);
    glutAddMenuEntry("Cylinder",    AddCylinder);
    glutAddMenuEntry("Donut",       AddDonut);
    glutAddMenuEntry("Monkey",      AddMonkey);
    glutAddMenuEntry("Banana",      AddBanana);
    
    glutCreateMenu(ContextMenu);
    glutAddMenuEntry("Open OBJ", OpenModelFile);
    glutAddSubMenu("Add Primitive", submenu_add);
    
    glutAddMenuEntry("Scene Editor",  SceneEditor);
    glutAddMenuEntry("Reset Active Camera", ResetActiveCamera);
    
    glutAddMenuEntry("Reset Step Size", ResetStepSize);
    
    glutAddMenuEntry("Toggle SSAA", ToggleSSAA);
    
    // glutAddMenuEntry("Demo",  DrawDemo);
    glutAddMenuEntry("About", About);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}
//----------------------------------------------------------------------------



int my_main( int argc, char **argv )
{
    //----------------------------------------------------------------------------
    // Initialize window
    glutInit( &argc, argv );
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_MULTISAMPLE);
    glutInitWindowSize(1280, 720);
    glutInitContextVersion(3, 2);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutCreateWindow("Awesome Software Render Enigne!");
    glewExperimental = GL_TRUE;
    glewInit();
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        /* Problem: glewInit failed, something is seriously wrong. */
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        /*        ...*/
    }
    fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
    
    
    renderer = new Renderer();
    scene = new Scene(renderer);
    //----------------------------------------------------------------------------
    // Initialize Callbacks
    
    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutMouseFunc( mouse );
    glutMouseWheelFunc( mouse_wheel );
    glutMotionFunc ( motion );
    glutPassiveMotionFunc( passive_motion ); //ADDED FOR PASSIVE MOUSE MOVEMENT
    glutReshapeFunc( reshape );
    initMenu();
    glutIdleFunc( display );
    
    QueryPerformanceFrequency(&debug_counter_frequency);
    scene->load_obj_model(default_model_path);
    scene->active_model->texture_map_path = "textures/Tiles_Terracotta_005_basecolor.jpg";
    scene->active_model->load_texture_map();
    scene->active_model->normal_map_path = "textures/Tiles_Terracotta_005_normal.jpg";
    scene->active_model->load_normal_map();
    
    crazy_colors = new Interpolating_Colors(renderer->rdata.crazy_color_table, renderer->rdata.crazy_color_count);
    crazy_shuffle = new Shuffle_Container(renderer->rdata.crazy_color_table, renderer->rdata.crazy_color_count);
    
    glutMainLoop();
    return 0;
}

CWinApp theApp;

using namespace std;

int main( int argc, char **argv )
{
    int nRetCode = 0;
    
    // initialize MFC and print and error on failure
    if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
    {
        // TODO: change error code to suit your needs
        _tprintf(_T("Fatal Error: MFC initialization failed\n"));
        nRetCode = 1;
    }
    else
    {
        my_main(argc, argv );
    }
    
    return nRetCode;
}
