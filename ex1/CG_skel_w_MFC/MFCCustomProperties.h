#pragma once
#include "afxpropertygridctrl.h"
#include "Scene.h"

namespace MFCProperty_Type {
    enum MFCPropertyType {
        Float,
        SceneSettings,
        Int,
        Angle,
        Bool,
        Color,
        MaterialColor,
        Path,
        Direction,
        ProjectionType,
        ShadingMode,
        LightType,
        LightSettings,
        LightHue,
        LightEnabled,
        LightIntensity,
    };
}

#define NEW_UI_COLOR_PROPERTY(name, property) new CMFCPropertyGridColorProperty(_T(name), VEC4_TO_RGB(property), NULL, NULL, (DWORD_PTR)new MFCPropertyData(MFCProperty_Type::Color, &(property)))

class MFCPropertyData {
    public:
    MFCPropertyData(MFCProperty_Type::MFCPropertyType type, void *data, uint32_t enum_value = 0);
    uint32_t type;
    uint32_t enum_value;
    void *data;
};

class MFCCustomProperty : public CMFCPropertyGridProperty
{
    public:
    // Group constructor
	MFCCustomProperty(const CString& strGroupName, DWORD_PTR dwData = 0, BOOL bIsValueList = FALSE);
    
	// Simple property
	MFCCustomProperty(const CString& strName, const COleVariant& varValue, LPCTSTR lpszDescr = NULL, MFCPropertyData *data = NULL,
                      LPCTSTR lpszEditMask = NULL, LPCTSTR lpszEditTemplate = NULL, LPCTSTR lpszValidChars = NULL);
    
    virtual CString FormatProperty();
};

class MFCCustomProperties : public CMFCPropertyGridCtrl
{
    public:
    MFCCustomProperties(Scene *scene);
    Scene *scene;
    void Init();
    virtual void OnPropertyChanged(CMFCPropertyGridProperty* pProp) const;
    void Reset();
};
