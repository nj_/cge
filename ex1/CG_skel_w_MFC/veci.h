#pragma once

struct vec3i {
    union {
        struct {
            int x;
            int y;
            int z;
        };
        struct {
            int v;
            int n;
            int t;
        };
    };
    
    vec3i(int s = 0) :
    x(s), y(s), z(s) {};
    
    
    vec3i(int x, int y, int z) :
    x(x), y(y), z(z) {};
    
    int& operator [] (int i) {
        return *(&x + i);
    }
    
    const int& operator [] (int i) const {
        return *(&x + i);
    }
};