#pragma once
#include "vec.h"


//----------------------------------------------------------------------------
//
//  mat2 - 2D square matrix
//

class mat2 {
    
    vec2  _m[2];
    
    public:
    //
    //  --- Constructors and Destructors ---
    //
    
    // Create a diagional matrix
    mat2(const GLfloat d = GLfloat(1.0)) {
        _m[0].x = d;
        _m[1].y = d;   
    }
    
    mat2(const vec2& a, const vec2& b) {
        _m[0] = a;
        _m[1] = b; 
    }
    
    /*FIXED*/
    mat2(GLfloat m00, GLfloat m01, GLfloat m10, GLfloat m11) {
        _m[0] = vec2(m00, m01);
        _m[1] = vec2(m10, m11);
    }
    
    mat2(const mat2& m) {
        if (*this != m) {
            _m[0] = m._m[0];
            _m[1] = m._m[1];
        } 
    }
    
    //
    //  --- Indexing Operator ---
    //
    
    vec2& operator [] (int i) {
        return _m[i]; 
    }
    
    const vec2& operator [] (int i) const {
        return _m[i]; 
    }
    
    //
    //  --- (non-modifying) Arithmatic Operators ---
    //
    
    mat2 operator + (const mat2& m) const {
        return mat2(_m[0]+m[0], _m[1]+m[1]); 
    }
    
    
    mat2 operator - (const mat2& m) const {
        return mat2(_m[0]-m[0], _m[1]-m[1]); 
    }
    
    mat2 operator * (const GLfloat s) const  {
        return mat2(s*_m[0], s*_m[1]); 
    }
    
    mat2 operator / (const GLfloat s) const {
        
        GLfloat r = GLfloat(1.0) / s;
        return *this * r;
    }
    
    friend mat2 operator * (const GLfloat s, const mat2& m) {
        return m * s; 
    }
    
    mat2 operator * (const mat2& m) const {
        vec2 tempcol1 = vec2(m[0][0], m[1][0]);
        vec2 tempcol2 = vec2(m[0][1], m[1][1]);
        mat2 result(vec2(dot(_m[0], tempcol1), dot(_m[0], tempcol2)),
                    vec2(dot(_m[1], tempcol1), dot(_m[1], tempcol2)));
        
        return result;
    }
    
    //
    //  --- (modifying) Arithmetic Operators ---
    //
    
    mat2& operator += (const mat2& m) {
        _m[0] += m[0];
        _m[1] += m[1];  
        return *this;
    }
    
    mat2& operator -= (const mat2& m) {
        _m[0] -= m[0];
        _m[1] -= m[1];
        return *this;
    }
    
    mat2& operator *= (const GLfloat s) {
        _m[0] *= s;  _m[1] *= s;   
        return *this;
    }
    
    mat2& operator *= (const mat2& m) {
        mat2 result = *this * result;
        return *this = result;
    }
    
    mat2& operator /= (const GLfloat s) {
        GLfloat r = GLfloat(1.0) / s;
        return *this *= r;
    }
    
    //
    //  --- Matrix / Vector operators ---
    //
    
    vec2 operator * (const vec2& v) const {  // m * v
        return vec2(dot(_m[0], v), dot(_m[1], v));
    }
    
    //
    //  --- Insertion and Extraction Operators ---
    //
    
    friend std::ostream& operator << (std::ostream& os, const mat2& m) {
        return os << std::endl << m[0] << std::endl << m[1] << std::endl; 
    }
    
    friend std::istream& operator >> (std::istream& is, mat2& m) {
        return is >> m._m[0] >> m._m[1]; 
    }
    
    //
    //  --- Conversion Operators ---
    //
    
    operator const GLfloat* () const {
        return static_cast<const GLfloat*>(&_m[0].x); 
    }
    
    operator GLfloat* () {
        return static_cast<GLfloat*>(&_m[0].x); 
    }
    
    static inline GLfloat Det(GLfloat m00, GLfloat m01,
                              GLfloat m10, GLfloat m11) {
        return m00 * m11 - m01 * m10;
    }
};

//
//  --- Non-class mat2 Methods ---
//

inline mat2 matrixCompMult(const mat2& A, const mat2& B) {
    return mat2(A[0][0]*B[0][0], A[0][1]*B[0][1],
                A[1][0]*B[1][0], A[1][1]*B[1][1]);
}

inline mat2 transpose(const mat2& A) {
    return mat2(A[0][0], A[1][0],
                A[0][1], A[1][1]);
}

//----------------------------------------------------------------------------
//
//  mat3 - 3D square matrix 
//

class mat3 {
    
    vec3  _m[3];
    
    public:
    //
    //  --- Constructors and Destructors ---
    //
    
    // Create a diagonal matrix
    mat3(const GLfloat d = GLfloat(0.0)) {
        _m[0].x = d;
        _m[1].y = d;
        _m[2].z = d;   
    }
    
    mat3(const vec3& a, const vec3& b, const vec3& c) {
        _m[0] = a;
        _m[1] = b;
        _m[2] = c;  
    }
    
    mat3(GLfloat m00, GLfloat m01, GLfloat m02,
         GLfloat m10, GLfloat m11, GLfloat m12,
         GLfloat m20, GLfloat m21, GLfloat m22) {
        _m[0] = vec3(m00, m01, m02);
        _m[1] = vec3(m10, m11, m12);
        _m[2] = vec3(m20, m21, m22);
    }
    
    mat3(const mat3& m) {
        if (*this != m) {
            _m[0] = m._m[0];
            _m[1] = m._m[1];
            _m[2] = m._m[2];
        } 
    }
    
    //
    //  --- Indexing Operator ---
    //
    vec3& operator [] (int i) {
        return _m[i]; 
    }
    const vec3& operator [] (int i) const {
        return _m[i]; 
    }
    
    //
    //  --- (non-modifying) Arithmatic Operators ---
    //
    
    mat3 operator + (const mat3& m) const {
        return mat3(_m[0]+m[0], _m[1]+m[1], _m[2]+m[2]); 
    }
    
    mat3 operator - (const mat3& m) const {
        return mat3(_m[0]-m[0], _m[1]-m[1], _m[2]-m[2]); 
    }
    
    mat3 operator * (const GLfloat s) const  {
        return mat3(s*_m[0], s*_m[1], s*_m[2]); 
    }
    
    mat3 operator / (const GLfloat s) const {
        GLfloat r = GLfloat(1.0) / s;
        return *this * r;
    }
    
    friend mat3 operator * (const GLfloat s, const mat3& m) {
        return m * s; 
    }
    
    mat3 operator * (const mat3& m) const {
        vec3 tempcol1(m._m[0][0], m._m[1][0], m._m[2][0]);
        vec3 tempcol2(m._m[0][1], m._m[1][1], m._m[2][1]);
        vec3 tempcol3(m._m[0][2], m._m[1][2], m._m[2][2]);
        
        mat3 a(dot(_m[0], tempcol1) , dot(_m[0], tempcol2) , dot(_m[0], tempcol3),
               dot(_m[1], tempcol1) , dot(_m[1], tempcol2) , dot(_m[1], tempcol3),
               dot(_m[2], tempcol1) , dot(_m[2], tempcol2) , dot(_m[2], tempcol3));
        /* //This part was replaced with the above code for readability, the part below works fine if a replacement is needed or the cost
             of initializing 3 vec3 and using the dot() method is too high//
        mat3  a(0.0);

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                for (int k = 0; k < 3; ++k) {
                    a[i][j] += _m[i][k] * m[k][j];
                }
            }
        }*/
        
        return a;
    }
    
    //
    //  --- (modifying) Arithmetic Operators ---
    //
    
    mat3& operator += (const mat3& m) {
        _m[0] += m[0];
        _m[1] += m[1];
        _m[2] += m[2]; 
        return *this;
    }
    
    mat3& operator -= (const mat3& m) {
        _m[0] -= m[0];
        _m[1] -= m[1];
        _m[2] -= m[2]; 
        return *this;
    }
    
    mat3& operator *= (const GLfloat s) {
        _m[0] *= s;
        _m[1] *= s;
        _m[2] *= s; 
        return *this;
    }
    
    mat3& operator *= (const mat3& m) {
        //This can be replaced with a call for
        //return *this = (*this)*m;
        mat3  a(0.0);
        
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                for (int k = 0; k < 3; ++k) {
                    a[i][j] += _m[i][k] * m[k][j];
                }
            }
        }
        
        return *this = a;
    }
    
    mat3& operator /= (const GLfloat s) {
        GLfloat r = GLfloat(1.0) / s;
        return *this *= r;
    }
    
    //
    //  --- Matrix / Vector operators ---
    //
    
    vec3 operator * (const vec3& v) const {  // m * v
        return vec3(dot(_m[0], v),
                    dot(_m[1], v),
                    dot(_m[2], v));
        /*return vec3(_m[0][0]*v[0] + _m[0][1] * v[1] + _m[0][2] * v[2],
                _m[1][0] * v[0] + _m[1][1] * v[1] + _m[1][2] * v[2],  //FIXED//
                _m[2][0] * v[0] + _m[2][1] * v[1] + _m[2][2] * v[2]);*/
    }
    
    //
    //  --- Insertion and Extraction Operators ---
    //
    
    friend std::ostream& operator << (std::ostream& os, const mat3& m) {
        return os << std::endl 
            << m[0] << std::endl
            << m[1] << std::endl
            << m[2] << std::endl;
    }
    
    friend std::istream& operator >> (std::istream& is, mat3& m) {
        return is >> m._m[0] >> m._m[1] >> m._m[2]; 
    }
    
    //
    //  --- Conversion Operators ---
    //
    
    operator const GLfloat* () const {
        return static_cast<const GLfloat*>(&_m[0].x); 
    }
    
    operator GLfloat* () {
        return static_cast<GLfloat*>(&_m[0].x); 
    }
    
    
    // Scale matrix constructors {
    
    static mat3 Scale(const GLfloat factor){
        mat3 c((GLfloat) 1.0); 
        c[0][0] = factor;
        c[1][1] = factor;
        c[2][2] = factor;
        
        return c;
    }
    
    static mat3 Scale(const GLfloat x, const GLfloat y, const GLfloat z){
        mat3 c((GLfloat) 1.0); 
        c[0][0] = x;
        c[1][1] = y;
        c[2][2] = z;
        
        return c;
    }
    
    static mat3 Scale(const vec3& v) {
        return Scale(v.x, v.y, v.z);
    }
    // } -- Scale matrix constructors
    
    // Rotation matrix constructors {
    // Rotation matrix constructors - mat4 {
    static mat3 Rotate(const GLfloat theta, vec3 axis) {
        GLfloat angle = (F_PI / (GLfloat)180.0) * theta;
        axis = normalize(axis);
        
        mat3 ux(0, -axis.z, axis.y,
                axis.z, 0, -axis.x,
                -axis.y, axis.x, 0);
        
        mat3 uxu(axis.x * axis.x, axis.x * axis.y, axis.x * axis.z,
                 axis.y * axis.x, axis.y * axis.y, axis.y * axis.z,
                 axis.z * axis.x, axis.z * axis.y, axis.z * axis.z);
        
        mat3 r = mat3((GLfloat)cos(angle)) + ux*(GLfloat)sin(angle) + uxu*(GLfloat)(1-cos(angle));
        
        return r;
    }
    
    static mat3 RotateX(const GLfloat theta) {
        return Rotate(theta, vec3(1, 0, 0));
    }
    
    static mat3 RotateY(const GLfloat theta) {
        return Rotate(theta, vec3(0, 1, 0));
    }
    
    static mat3 RotateZ(const GLfloat theta) {
        return Rotate(theta, vec3(0, 0, 1));
    }
    // } -- Rotation matrix constructors
    
    
    static inline GLfloat Det(GLfloat m00, GLfloat m01, GLfloat m02,
                              GLfloat m10, GLfloat m11, GLfloat m12,
                              GLfloat m20, GLfloat m21, GLfloat m22) {
        return (m00 * mat2::Det(m11, m12, m21, m22) - 
                m01 * mat2::Det(m10, m12, m20, m22) + 
                m02 * mat2::Det(m10, m11, m20, m21));
    }
    
    static inline mat3 Inverse(const mat3& m) {
        GLfloat determinant = Det(m[0][0], m[0][1], m[0][2], 
                                  m[1][0], m[1][1], m[1][2],
                                  m[2][0], m[2][1], m[2][2]);
        
        mat3 inverse;
        inverse[0][0] = mat2::Det(m[1][1], m[1][2], m[2][1], m[2][2]) / determinant;
        inverse[0][1] = mat2::Det(m[1][0], m[1][2], m[2][0], m[2][2]) / determinant;
        inverse[0][2] = mat2::Det(m[1][0], m[1][1], m[2][0], m[2][1]) / determinant;
        inverse[1][0] = mat2::Det(m[0][1], m[0][2], m[2][1], m[2][2]) / determinant;
        inverse[1][1] = mat2::Det(m[0][0], m[0][2], m[2][0], m[2][2]) / determinant;
        inverse[1][2] = mat2::Det(m[0][0], m[0][1], m[2][0], m[2][1]) / determinant;
        inverse[2][0] = mat2::Det(m[0][1], m[0][2], m[1][1], m[1][2]) / determinant;
        inverse[2][1] = mat2::Det(m[0][0], m[0][2], m[1][0], m[1][2]) / determinant;
        inverse[2][2] = mat2::Det(m[0][0], m[0][1], m[1][0], m[1][1]) / determinant;
        
        return inverse;
    }
    
};




//
//  --- Non-class mat3 Methods ---
//

inline mat3 matrixCompMult(const mat3& A, const mat3& B) {
    return mat3(A[0][0]*B[0][0], A[0][1]*B[0][1], A[0][2]*B[0][2],
                A[1][0]*B[1][0], A[1][1]*B[1][1], A[1][2]*B[1][2],
                A[2][0]*B[2][0], A[2][1]*B[2][1], A[2][2]*B[2][2]);
}

inline mat3 transpose(const mat3& A) {
    return mat3(A[0][0], A[1][0], A[2][0],
                A[0][1], A[1][1], A[2][1],
                A[0][2], A[1][2], A[2][2]); 
}


//----------------------------------------------------------------------------
//
//  Performing rotation by shear - mat3
//
/*  
    As we know, Rotation by -theta is the inverse of rotation by theta. Due to its properties,
    over any 'theta', this transformation's inverse is the theta's minus' rotation
    matrix's transpose, allowing us to make the inverse action just a bit more precise.
*/


inline vec3 ShearRotateZ(vec3 v, GLfloat theta) {
    GLfloat min_tan_half;  //alpha
    GLfloat sin;           //beta
    bool perform_inverse = theta < 0.0f;      
    if (perform_inverse) {
        theta = -theta;
        min_tan_half = -std::tan(theta / 2);  //alpha
        sin = (GLfloat)std::sin(theta);;      //beta
    }
    else {
        min_tan_half = -std::tan(theta / 2);  //alpha
        sin = (GLfloat)std::sin(theta);       //beta
    }
    mat3 mat_a(1.0f, min_tan_half, 0.0f,
               0.0f, 1.0f        , 0.0f,
               0.0f, 0.0f        , 1.0f);
    mat3 mat_b(1.0f, 1.0f        , 0.0f,
               sin , 1.0f        , 0.0f,
               0.0f, 0.0f        , 1.0f);
    if (perform_inverse) {
        mat_a = transpose(mat_a);
        mat_b = transpose(mat_b);
    }
    
    return (mat_a*(mat_b*(mat_a*v)));
}

inline vec3 ShearRotateX(vec3 v, GLfloat theta) {
    GLfloat min_tan_half;  //alpha
    GLfloat sin;           //beta
    bool perform_inverse = theta < 0.0f;
    if (perform_inverse) {
        theta = -theta;
        min_tan_half = -std::tan(theta / 2);  //alpha
        sin = (GLfloat)std::sin(theta);;      //beta
    }
    else {
        min_tan_half = -std::tan(theta / 2);  //alpha
        sin = (GLfloat)std::sin(theta);       //beta
    }
    mat3 mat_a(1.0f, 0.0f        , 0.0f,
               0.0f, 1.0f        , min_tan_half,
               0.0f, 0.0f        , 1.0f);
    mat3 mat_b(1.0f, 1.0f        , 0.0f,
               1.0f, 1.0f        , 0.0f,
               0.0f, sin         , 1.0f);
    if (perform_inverse) {
        mat_a = transpose(mat_a);
        mat_b = transpose(mat_b);
    }
    
    return (mat_a*(mat_b*(mat_a*v)));    
}

inline vec3 ShearRotateY(vec3 v, GLfloat theta) {
    GLfloat min_tan_half;  //alpha
    GLfloat sin;           //beta
    bool perform_inverse = theta < 0.0f;
    if (perform_inverse) {
        theta = -theta;
        min_tan_half = -std::tan(theta / 2);  //alpha
        sin = (GLfloat)std::sin(theta);;      //beta
    }
    else {
        min_tan_half = -std::tan(theta / 2);  //alpha
        sin = (GLfloat)std::sin(theta);       //beta
    }
    mat3 mat_a(1.0f, 0.0f, min_tan_half,
               0.0f, 1.0f, 0.0f,
               0.0f, 0.0f, 1.0f);
    mat3 mat_b(1.0f, 1.0f, 0.0f,
               0.0f, 1.0f, 0.0f,
               sin, 0.0f, 1.0f);
    if (perform_inverse) {
        mat_a = transpose(mat_a);
        mat_b = transpose(mat_b);
    }
    
    return (mat_a*(mat_b*(mat_a*v)));
}

// vec3 GeneralRotateByShear(vec3, GLfloat theta, vec3 direction) {
// TODO
// }

//----------------------------------------------------------------------------
//
//  mat4.h - 4D square matrix
//

class mat4 {
    vec4  _m[4];
    
    public:
    //
    //  --- Constructors and Destructors ---
    //
    
    // Create a diagional matrix
    mat4(const GLfloat d = GLfloat(1.0))   {
        _m[0].x = d;
        _m[1].y = d;
        _m[2].z = d;
        _m[3].w = d; 
    }
    
    mat4(const vec4& a, const vec4& b, const vec4& c, const vec4& d) {
        _m[0] = a;
        _m[1] = b;
        _m[2] = c;
        _m[3] = d; 
    }
    
    mat4(GLfloat m00, GLfloat m01, GLfloat m02, GLfloat m03,
         GLfloat m10, GLfloat m11, GLfloat m12, GLfloat m13,
         GLfloat m20, GLfloat m21, GLfloat m22, GLfloat m23,
         GLfloat m30, GLfloat m31, GLfloat m32, GLfloat m33) {
        _m[0] = vec4(m00, m01, m02, m03);
        _m[1] = vec4(m10, m11, m12, m13);
        _m[2] = vec4(m20, m21, m22, m23);
        _m[3] = vec4(m30, m31, m32, m33);
    }
    
    mat4(const mat4& m) {
        if (*this != m) {
            _m[0] = m._m[0];
            _m[1] = m._m[1];
            _m[2] = m._m[2];
            _m[3] = m._m[3];
        } 
    }
    
    mat4(const mat3& m) {
        _m[0] = vec4(m[0][0], m[0][1], m[0][2], (GLfloat) 0.0);
        _m[1] = vec4(m[1][0], m[1][1], m[1][2], (GLfloat) 0.0);
        _m[2] = vec4(m[2][0], m[2][1], m[2][2], (GLfloat) 0.0);
        _m[3] = vec4((GLfloat) 0.0, (GLfloat) 0.0, (GLfloat) 0.0, (GLfloat) 1.0);
    }
    
    //
    //  --- Inverse Constructor ---
    //
    
    static inline GLfloat Det(GLfloat m00, GLfloat m01, GLfloat m02, GLfloat m03,
                              GLfloat m10, GLfloat m11, GLfloat m12, GLfloat m13,
                              GLfloat m20, GLfloat m21, GLfloat m22, GLfloat m23,
                              GLfloat m30, GLfloat m31, GLfloat m32, GLfloat m33) {
        return (m00 * mat3::Det(m11, m12, m13, m21, m22, m23, m31, m32, m33) -
                m01 * mat3::Det(m10, m12, m13, m20, m22, m23, m30, m32, m33) +
                m02 * mat3::Det(m10, m11, m13, m20, m21, m23, m30, m31, m33) -
                m03 * mat3::Det(m10, m11, m12, m20, m21, m22, m30, m31, m32));
    }
    
    static inline mat4 Inverse(const mat4& m) {
        GLfloat determinant = Det(m[0][0], m[0][1], m[0][2], m[0][3],
                                  m[1][0], m[1][1], m[1][2], m[1][3],
                                  m[2][0], m[2][1], m[2][2], m[2][3],
                                  m[3][0], m[3][1], m[3][2], m[3][3]);
        mat4 inverse;
        inverse[0][0] = mat3::Det(m[1][1], m[1][2], m[1][3], m[2][1], m[2][2], m[2][3], m[3][1], m[3][2], m[3][3]) / determinant;
        inverse[0][1] = mat3::Det(m[1][0], m[1][2], m[1][3], m[2][0], m[2][2], m[2][3], m[3][0], m[3][2], m[3][3]) / determinant;
        inverse[0][2] = mat3::Det(m[1][0], m[1][1], m[1][3], m[2][0], m[2][1], m[2][3], m[3][0], m[3][1], m[3][3]) / determinant;
        inverse[0][3] = mat3::Det(m[1][0], m[1][1], m[1][2], m[2][0], m[2][1], m[2][2], m[3][0], m[3][1], m[3][2]) / determinant;
        inverse[1][0] = mat3::Det(m[0][1], m[0][2], m[0][3], m[2][1], m[2][2], m[2][3], m[3][1], m[3][2], m[3][3]) / determinant;
        inverse[1][1] = mat3::Det(m[0][0], m[0][2], m[0][3], m[2][0], m[2][2], m[2][3], m[3][0], m[3][2], m[3][3]) / determinant;
        inverse[1][2] = mat3::Det(m[0][0], m[0][1], m[0][3], m[2][0], m[2][1], m[2][3], m[3][0], m[3][1], m[3][3]) / determinant;
        inverse[1][3] = mat3::Det(m[0][0], m[0][1], m[0][2], m[2][0], m[2][1], m[2][2], m[3][0], m[3][1], m[3][2]) / determinant;
        inverse[2][0] = mat3::Det(m[0][1], m[0][2], m[0][3], m[1][1], m[1][2], m[1][3], m[3][1], m[3][2], m[3][3]) / determinant;
        inverse[2][1] = mat3::Det(m[0][0], m[0][2], m[0][3], m[1][0], m[1][2], m[1][3], m[3][0], m[3][2], m[3][3]) / determinant;
        inverse[2][2] = mat3::Det(m[0][0], m[0][1], m[0][3], m[1][0], m[1][1], m[1][3], m[3][0], m[3][1], m[3][3]) / determinant;
        inverse[2][3] = mat3::Det(m[0][0], m[0][1], m[0][2], m[1][0], m[1][1], m[1][2], m[3][0], m[3][1], m[3][2]) / determinant;
        inverse[3][0] = mat3::Det(m[0][1], m[0][2], m[0][3], m[1][1], m[1][2], m[1][3], m[2][1], m[2][2], m[2][3]) / determinant;
        inverse[3][1] = mat3::Det(m[0][0], m[0][2], m[0][3], m[1][0], m[1][2], m[1][3], m[2][0], m[2][2], m[2][3]) / determinant;
        inverse[3][2] = mat3::Det(m[0][0], m[0][1], m[0][3], m[1][0], m[1][1], m[1][3], m[2][0], m[2][1], m[2][3]) / determinant;
        inverse[3][3] = mat3::Det(m[0][0], m[0][1], m[0][2], m[1][0], m[1][1], m[1][2], m[2][0], m[2][1], m[2][2]) / determinant;
        
        return inverse;
    }
    
    //
    //  --- Indexing Operator ---
    //
    vec4& operator [] (int i) {
        return _m[i]; 
    }
    const vec4& operator [] (int i) const {
        return _m[i]; 
    }
    
    //
    //  --- (non-modifying) Arithematic Operators ---
    //
    
    mat4 operator + (const mat4& m) const {
        return mat4(_m[0]+m[0], _m[1]+m[1], _m[2]+m[2], _m[3]+m[3]); 
    }
    
    mat4 operator - (const mat4& m) const {
        return mat4(_m[0]-m[0], _m[1]-m[1], _m[2]-m[2], _m[3]-m[3]); 
    }
    
    mat4 operator * (const GLfloat s) const  {
        return mat4(s*_m[0], s*_m[1], s*_m[2], s*_m[3]); 
    }
    
    mat4 operator / (const GLfloat s) const {
        GLfloat r = GLfloat(1.0) / s;
        return *this * r;
    }
    
    friend mat4 operator * (const GLfloat s, const mat4& m) {
        return m * s; 
    }
    
    mat4 operator * (const mat4& m) const {
        
        vec4 tempcol1(m[0][0], m[1][0], m[2][0], m[3][0]);
        vec4 tempcol2(m[0][1], m[1][1], m[2][1], m[3][1]);
        vec4 tempcol3(m[0][2], m[1][2], m[2][2], m[3][2]);
        vec4 tempcol4(m[0][3], m[1][3], m[2][3], m[3][3]);
        
        mat4 a(dot(_m[0], tempcol1), dot(_m[0], tempcol2), dot(_m[0], tempcol3), dot(_m[0], tempcol4),
               dot(_m[1], tempcol1), dot(_m[1], tempcol2), dot(_m[1], tempcol3), dot(_m[1], tempcol4),
               dot(_m[2], tempcol1), dot(_m[2], tempcol2), dot(_m[2], tempcol3), dot(_m[2], tempcol4),
               dot(_m[3], tempcol1), dot(_m[3], tempcol2), dot(_m[3], tempcol3), dot(_m[3], tempcol4));
        
        /* //This part was replaced with the above code for readability, the part below works fine if a replacement is needed or the cost
             of initializing 3 vec3 and using the dot() method is too high//
        mat4  a(0.0);
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                for (int k = 0; k < 4; ++k) {
                    a[i][j] += _m[i][k] * m[k][j];
                }
            }
        }*/
        
        return a;
    }
    
    //
    //  --- (modifying) Arithematic Operators ---
    //
    
    mat4& operator += (const mat4& m) {
        _m[0] += m[0];
        _m[1] += m[1];
        _m[2] += m[2];
        _m[3] += m[3];
        return *this;
    }
    
    mat4& operator -= (const mat4& m) {
        _m[0] -= m[0];
        _m[1] -= m[1];
        _m[2] -= m[2];
        _m[3] -= m[3];
        return *this;
    }
    
    mat4& operator *= (const GLfloat s) {
        _m[0] *= s;
        _m[1] *= s;
        _m[2] *= s;
        _m[3] *= s;
        return *this;
    }
    
    mat4& operator *= (const mat4& m) {
        //This can be replaced with a call for
        //return *this = (*this)*m;
        mat4  a(0.0);
        
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                for (int k = 0; k < 4; ++k) {
                    a[i][j] += _m[i][k] * m[k][j];
                }
            }
        }
        
        return *this = a;
    }
    
    mat4& operator /= (const GLfloat s) {
        GLfloat r = GLfloat(1.0) / s;
        return *this *= r;
    }
    
    //
    //  --- Matrix / Vector operators ---
    //
    
    vec4 operator * (const vec4& v) const {  // m * v
        return vec4(dot(_m[0], v), dot(_m[1], v), dot(_m[2], v), dot(_m[3], v));
    }
    
    vec4 operator * (const vec3& v) const { //We added this one. Autofills the 4th element to be the unit 1.0.
        return (*this) * vec4(v[0], v[1], v[2], GLfloat(1.0));
    }
    
    //
    //  --- Insertion and Extraction Operators ---
    //
    
    friend std::ostream& operator << (std::ostream& os, const mat4& m) {
        return os << std::endl 
            << m[0] << std::endl
            << m[1] << std::endl
            << m[2] << std::endl
            << m[3] << std::endl;
    }
    
    friend std::istream& operator >> (std::istream& is, mat4& m) {
        return is >> m._m[0] >> m._m[1] >> m._m[2] >> m._m[3]; 
    }
    
    //
    //  --- Conversion Operators ---
    //
    
    operator const GLfloat* () const {
        return static_cast<const GLfloat*>(&_m[0].x); 
    }
    
    operator GLfloat* () {
        return static_cast<GLfloat*>(&_m[0].x); 
    }
    
    // Rotation matrix constructors - mat4 {
    static mat4 Rotate(const GLfloat theta, const vec3 axis) {
        return mat4(mat3::Rotate(theta, axis));
    }
    
    static mat4 RotateX(const GLfloat theta) {
        return Rotate(theta, vec3(1, 0, 0));
    }
    
    static mat4 RotateY(const GLfloat theta) {
        return Rotate(theta, vec3(0, 1, 0));
    }
    
    static mat4 RotateZ(const GLfloat theta) {
        return Rotate(theta, vec3(0, 0, 1));
    }
    // } -- Rotation matrix constructors - mat4 
    
    //  Translation matrix constructors {
    static mat4 Translate(const GLfloat x, const GLfloat y, const GLfloat z) {
        mat4 c((GLfloat)1.0);
        
        c[0][3] = x;
        c[1][3] = y;
        c[2][3] = z;
        
        return c;
    }
    
    static mat4 Translate(const vec3& v) {
        return Translate(v.x, v.y, v.z);
    }
    
    static mat4 Translate(const vec4& v) {
        return Translate(v.x, v.y, v.z);
    }
    // } -- Translation matrix constructors
    
    // Scale matrix constructors {
    static mat4 Scale(const GLfloat factor){
        mat4 c((GLfloat) 1.0); 
        c[0][0] = factor;
        c[1][1] = factor;
        c[2][2] = factor;
        
        return c;
    }
    
    static mat4 Scale(const GLfloat x, const GLfloat y, const GLfloat z){
        mat4 c((GLfloat) 1.0); 
        c[0][0] = x;
        c[1][1] = y;
        c[2][2] = z;
        
        return c;
    }
    
    static mat4 Scale(const vec3& v) {
        return Scale(v.x, v.y, v.z);
    }
    // } -- Scale matrix constructors
};

//
//  --- Non-class mat4 Methods ---
//

inline mat4 matrixCompMult(const mat4& A, const mat4& B) {
    return mat4(A[0][0]*B[0][0], A[0][1]*B[0][1], A[0][2]*B[0][2], A[0][3]*B[0][3],
                A[1][0]*B[1][0], A[1][1]*B[1][1], A[1][2]*B[1][2], A[1][3]*B[1][3],
                A[2][0]*B[2][0], A[2][1]*B[2][1], A[2][2]*B[2][2], A[2][3]*B[2][3],
                A[3][0]*B[3][0], A[3][1]*B[3][1], A[3][2]*B[3][2], A[3][3]*B[3][3]);
}

inline mat4 transpose(const mat4& A) {
    return mat4(A[0][0], A[1][0], A[2][0], A[3][0],
                A[0][1], A[1][1], A[2][1], A[3][1],
                A[0][2], A[1][2], A[2][2], A[3][2],
                A[0][3], A[1][3], A[2][3], A[3][3]);
}

//////////////////////////////////////////////////////////////////////////////
//
//  Helpful Matrix Methods
//
//////////////////////////////////////////////////////////////////////////////

#define Error(str) do { std::cerr << "[" __FILE__ ":" << __LINE__ << "] " \
<< str << std::endl; } while(0)

inline vec4 mvmult(const mat4& a, const vec4& b) {
    Error("replace with vector matrix multiplcation operator");
    
    vec4 c;
    int i, j;
    for(i=0; i<4; i++) {
        c[i] =0.0;
        for(j=0;j<4;j++) c[i]+=a[i][j]*b[j];
    }
    return c;
}

//----------------------------------------------------------------------------
